<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\Model\Employee;

use App\Providers\Model\RoleAuthentication;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;


class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;


    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function login (Request $request) {
        $email = $request->get('email');
//        $token = Str::random(60);
        $User = Employee::query()->where('email', '=', $email)
            ->first();
        if (!$User)
        {
            return response()->json([
                'status' => "error",
                'message' => "Unauthorized"
            ],401);
        }
        $credentials = $request->only('email', 'password');

        if ($token = $this->guard()->attempt($credentials))
        {
            $User->bearer_token = $token;
            $User->save();
            return $this->respondWithToken($token, $User);
        }

        return response()->json([
            'status' => "error",
            'message' => "incorrect username or password"
        ],400);

    }

    public function logout(Request $request)
    {
        if ($token = $request->header('Authorization'))
        {
            $result = explode(" ",$token);
        } else {
            return response()->json([
                'status' => 'error',
                'message' => "Token not found"
            ], 401);
        }
//        $result = explode(" ",$token);
        $user = null;
        if ($result)
        {
            $user = Employee::where('bearer_token', '=', $result[1])->first();
            if ($user)
            {
                $user->bearer_token = null;
                $user->save();
                return response()->json([
                    'status' => 'success',
                    'message' => 'OK'
                ], 200);
            } else {
                return response()->json([
                    'status' => 'error',
                    'message' => 'Unauthorized'
                ], 401);
            }
        }

        return response()->json([
            'status' => 'error',
            'message' => "Unauthorized"
        ], 401);
    }

    protected function respondWithToken($token, $user)
    {
        //generate token

        $roles = RoleAuthentication::where('role_authentications.RoleSkey', '=', $user->RoleCodeSkey)
            ->join('role_screens', 'role_authentications.ScreenSkey', '=', 'role_screens.ScreenNo')
//            ->where('role_authentications.RoleSkey', '=', $user->RoleCodeSkey)->get();
            ->select('role_authentications.PermissionType', 'role_screens.ScreenName', 'role_screens.ScreenStatus')->get();


        return response()->json([
            'id' => $user->id,
            'email' => $user->email,
            'full_name_eng' => $user->PrefixEng.' ' .$user->NameEng. ' ' .$user->SurNameEng,
            'full_name_thai' => $user->PrefixThai.' ' .$user->NameThai. ' ' .$user->SurNameThai,
            'access_token' => $token,
            'role' => $user->RoleCodeSkey,
            'role_permission' => $roles,
            'token_type' => 'bearer',
            'expires_in' => $this->guard()->factory()->getTTL() * 60,
        ], 200);
    }
    protected function guard()
    {
        return Auth::guard('api');
    }

}
