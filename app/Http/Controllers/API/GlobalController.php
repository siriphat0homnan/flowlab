<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Providers\Model\Approve;
use App\Providers\Model\Employee;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class GlobalController extends Controller
{
    //
    static public function CheckHeader(Request $request)
    {
        //check header from admin
        $token = $request->header('authorization');
        $user = null;
        try {
            $result = explode(" ",$token);
            $user = null;
            if ($result)
            {
                $user = Employee::where('bearer_token', '=', $result[1])
//                    ->where('status', '=', "Active")
                    ->first();
            }
            if (!$user)
            {
                return response()->json([
                    "status" => "error",
                    "message" => "Unauthorized"
                ], 401);
            }
        } catch (\Exception $exception)
        {
            return response()->json([
                "status" => "error",
                "message" => "Unauthorized"
            ], 401);
        }
        return response()->json([
            "status" => "ok",
            "id" => $user->id,
            "message" => "Authorized"
        ], 200);
    }

    static public function SearchBetween($first, $second, $table, $roles){
        $now = $first;
        $prev = $second;

        $roles->whereBetween($table, [$now, $prev]);

        return $roles;
    }


    static public function CreateApprove($id, $type, $master, $check_header){
        $no = DB::table('approves')->orderBy('ApproveKey', 'desc')->select('ApproveKey')->get();
        if(count($no) == 0){
            $last = 'A00000001';
        }else{
            $last = $no[0]->ApproveKey;
            $last++;
        }

        // create approve
        $approve = new Approve();
        $approve->ApproveKey = $last;
        $approve->Master = $master;
        $approve->CRType = $type;
        $approve->CRStatus = 'Pending';
        $approve->ApproveUserCreate = $check_header['id'];
        $approve->ApproveDateCreate = now();
        $approve->save();

        return $last;
    }
}
