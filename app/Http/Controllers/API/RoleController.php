<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Controllers\API\GlobalController;
use App\Http\Middleware\Authenticate;
use App\Providers\Model\Approve;
use App\Providers\Model\CrRole;
use App\Providers\Model\CRRoleAuthentications;
use App\Providers\Model\Employee;
use App\Providers\Model\Role;
use App\Providers\Model\RoleAuthentication;
use App\Providers\Model\RoleScreen;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use function GuzzleHttp\Psr7\str;

class RoleController extends Controller
{
    // get role
    public function GetRole(Request $request){

        $allow_header = GlobalController::CheckHeader($request);
        $check_header = json_decode($allow_header->content(), true);
        if ($check_header['status'] == 'error')
        {
            return $allow_header;
        }
        try {

            $id = $check_header['id'];
            $emp_state = Employee::where('id', '=', $id)->select('StateRoles')->first();
            $arr = explode(",",$emp_state['StateRoles']);
            $roles = DB::table('roles')
                ->join('employees', 'employees.id', '=', 'roles.UserCreate')
                ->select('roles.RoleCode', 'roles.RoleName', 'roles.RoleStatus', 'roles.DateCreate', 'roles.UserCreate', 'roles.LastUpdateDate', 'roles.LastUpdateUser', 'employees.email')->get();

            if (isset($roles[0]))
            {
                for ($i = 0; $i < count($roles); $i++){
                    $emp_to_create = DB::table('employees')->where('id', '=', $roles[$i]->UserCreate)->get();
                    $emp_to_change = DB::table('employees')->where('id', '=', $roles[$i]->LastUpdateUser)->get();
                    $all_data[$i] = [];

                    for($j = 0; $j < count($arr); $j++){
                        if($arr[$j] == 'RoleCode'){
                            $all_data[$i] = array_merge($all_data[$i], array('RoleCode' => $roles[$i]->RoleCode,));
                        }
                        if($arr[$j] == 'RoleName'){
                            $all_data[$i] = array_merge($all_data[$i], array('RoleName' => $roles[$i]->RoleName,));
                        }
                        if($arr[$j] == 'RoleStatus'){
                            $all_data[$i] = array_merge($all_data[$i], array('RoleStatus' => $roles[$i]->RoleStatus,));
                        }
                        if($arr[$j] == 'DateCreate'){
                            $all_data[$i] = array_merge($all_data[$i], array('DateCreate' => $roles[$i]->DateCreate,));
                        }
                        if($arr[$j] == 'UserCreate'){
                            $all_data[$i] = array_merge($all_data[$i], array('UserCreate' => $emp_to_create[0]->email,));
                        }
                        if($arr[$j] == 'LastUpdateDate'){
                            $all_data[$i] = array_merge($all_data[$i], array('LastUpdateDate' => $roles[$i]->LastUpdateDate,));
                        }
                        if($arr[$j] == 'LastUpdateUser'){
                            if(isset($roles[$i]->LastUpdateUser)){
                                $all_data[$i] = array_merge($all_data[$i], array('LastUpdateUser' => $emp_to_change[0]->email,));
                            }else{
                                $all_data[$i] = array_merge($all_data[$i], array('LastUpdateUser' => $roles[$i]->LastUpdateUser,));
                            }
                        }
                    }
                }
            }
            if($all_data[0] == []){
                $all_data = $roles;
            }
            $array = json_decode(json_encode($all_data), true);
            return $array;
        } catch (\Exception $exception)
        {
            return $this->ErrorDataBase();
        }
    }

    // search role
    public function SearchRole(Request $request){
        $allow_header = GlobalController::CheckHeader($request);
        $check_header = json_decode($allow_header->content(), true);
        if ($check_header['status'] == 'error')
        {
            return $allow_header;
        }
        $id = $check_header['id'];
        $emp_state = Employee::where('id', '=', $id)->select('StateRoles')->first();
        $arr = explode(",", $emp_state['StateRoles']);
        try {

            $roles = DB::table('roles')->join('employees', 'employees.id', '=', 'roles.UserCreate')
                ->select('roles.id', 'roles.RoleCode', 'roles.RoleName', 'roles.RoleStatus', 'roles.DateCreate', 'roles.UserCreate', 'roles.LastUpdateDate', 'roles.LastUpdateUser', 'employees.email');

            $check = count($request->get('role_code'));
            if ($check != 0){
                $first = $request->get("role_code");
                if($check == 1){

                    $second = $request->get("role_code_to");
                    $first = $first[0];
                    if($second){
                        $roles = GlobalController::SearchBetween($first, $second, 'roles.RoleCode', $roles);
                    }
                    else{

                        if(isset($first)){
                            $roles = $roles->Where('roles.RoleCode','LIKE','%'.$first.'%');
                        }
                    }
                }else{
                    $roles = $roles->whereIn('roles.RoleCode', $first);
                }
            }

            if (!empty($request->get('role_status'))){
                $roles->Where('roles.RoleStatus','LIKE','%'.$request->get("role_status").'%');
            }
            if (!empty($request->get('role_name'))){
                $roles->Where('roles.RoleName','LIKE','%'.$request->get("role_name").'%');
            }

            if (!empty($request->get('role_date'))){
                $first = $request->get("role_date");
                $second = $request->get("role_date_to");

                if($second != $first && $second != null){
                    $roles = GlobalController::SearchBetween($first, $second, 'roles.DateCreate', $roles);
                }
                else{
                    $roles->Where('roles.DateCreate','LIKE','%'.$request->get("role_date").'%');
                }
            }

            $roles = $roles->get();
//            foreach ($roles as $role){
//                $role->Auth = RoleAuthentication::where('RoleSkey', '=', $role->RoleCode)->get();
//            }
//            return $roles;
            $all_data = [];
            if (isset($roles[0]))
            {
                for ($i = 0; $i < count($roles); $i++){
                    $emp_to_create = DB::table('employees')->where('id', '=', $roles[$i]->UserCreate)->get();
                    $emp_to_change = DB::table('employees')->where('id', '=', $roles[$i]->LastUpdateUser)->get();
                    $all_data[$i] = [];

                    for($j = 0; $j < count($arr); $j++){
                        $all_data[$i] = array_merge($all_data[$i], array('id' => $roles[$i]->id,));

                        if($arr[$j] == 'RoleCode'){
                            $all_data[$i] = array_merge($all_data[$i], array('RoleCode' => $roles[$i]->RoleCode,));
                        }
                        if($arr[$j] == 'RoleName'){
                            $all_data[$i] = array_merge($all_data[$i], array('RoleName' => $roles[$i]->RoleName,));
                        }
                        if($arr[$j] == 'RoleStatus'){
                            $all_data[$i] = array_merge($all_data[$i], array('RoleStatus' => $roles[$i]->RoleStatus,));
                        }
                        if($arr[$j] == 'DateCreate'){
                            $all_data[$i] = array_merge($all_data[$i], array('DateCreate' => $roles[$i]->DateCreate,));
                        }
                        if($arr[$j] == 'UserCreate'){
                            $all_data[$i] = array_merge($all_data[$i], array('UserCreate' => $emp_to_create[0]->email,));
                        }
                        if($arr[$j] == 'LastUpdateDate'){
                            $all_data[$i] = array_merge($all_data[$i], array('LastUpdateDate' => $roles[$i]->LastUpdateDate,));
                        }
                        if($arr[$j] == 'LastUpdateUser'){
                            if(isset($roles[$i]->LastUpdateUser)){
                                $all_data[$i] = array_merge($all_data[$i], array('LastUpdateUser' => $emp_to_change[0]->email,));
                            }else{
                                $all_data[$i] = array_merge($all_data[$i], array('LastUpdateUser' => $roles[$i]->LastUpdateUser,));
                            }
                        }

                    }
                     $all_data[$i] = array_merge($all_data[$i], array('Auth' => RoleAuthentication::where('RoleSkey', '=', $roles[$i]->RoleCode)->get()));


                }
            }

            if($all_data == []){
                $all_data = $roles;
            }

            $array = json_decode(json_encode($all_data), true);

            return $array;
        } catch (\Exception $exception)
        {
            return response()->json([
                "status" => "error",
                "message" => "Bad Request"
            ], 400);
        }
    }

    // create role
    public function CreateCRRole(Request $request){
        $allow_header = GlobalController::CheckHeader($request);
        $check_header = json_decode($allow_header->content(), true);
        if ($check_header['status'] == 'error') {
            return $allow_header;
        }
        try {
            $last = GlobalController::CreateApprove($check_header['id'], 'Create Role', 'role', $check_header);
            $no = DB::table('cr_roles')->orderBy('RoleCode', 'desc')->select('RoleCode')->get();
            if(count($no) == 0){
                $lastRole = 'R0001';
            }else{
                $lastRole = $no[0]->RoleCode;
                $lastRole++;
            }


            $roles = new CrRole();
            $roles->ApproveKey = $last;
            $roles->RoleCode = $lastRole;
            $roles->RoleName = $request->get('role_name');
            $roles->RoleStatus = $request->get('role_status');
            $roles->YellowName = 'true';
            $roles->YellowStatus = 'true';
            $roles->DateCreate = now();
            $roles->UserCreate = $check_header['id'];
            $roles->LastUpdateDate = now();
            $roles->LastUpdateUser = $check_header['id'];

            if ($roles->save()) {
                // add role auth
                $role_screen = RoleScreen::all();
                for ($i = 0; $i < count($role_screen); $i++){
                    $role_auth = new CRRoleAuthentications();
                    $role_auth->ApproveKey = $last;
                    $role_auth->RoleSkey = $roles->RoleCode ;
                    $role_auth->ScreenSkey = $role_screen[$i]->ScreenNo;
                    $role_auth->PermissionType = 'Invisible';
                    $role_auth->UserCreate = $check_header['id'];
                    $role_auth->Yellow = 'true';
                    $role_auth->DateCreate = now();
                    $role_auth->UserUpdate = $check_header['id'];
                    $role_auth->DateUpdate = now();
                    $role_auth->save();
                }

                return response()->json([
                    "status" => "success",
                    "message" => "created"
                ], 201);
            } else {
                return response()->json([
                    "status" => "error",
                    "message" => "Bad Request"
                ], 400);
            }
            return $roles;

        } catch (\Exception $exception)
        {
            return $exception;
        }

    }

    public function CreateRole(Request $request){
        $allow_header = GlobalController::CheckHeader($request);
        $check_header = json_decode($allow_header->content(), true);
        if ($check_header['status'] == 'error') {
            return $allow_header;
        }
        try {
            $no = DB::table('roles')->orderBy('RoleCode', 'desc')->select('RoleCode')->get();
            if(count($no) == 0){
                $lastRole = 'R0001';
            }else{
                $lastRole = $no[0]->RoleCode;
                $lastRole++;
            }


            $roles = new Role();
            $roles->RoleCode = $lastRole;
            $roles->RoleName = $request->get('role_name');
            $roles->RoleStatus = $request->get('role_status');
            $roles->YellowName = 'true';
            $roles->YellowStatus = 'true';
            $roles->RoleName = $request->get('role_name');
            $roles->RoleStatus = $request->get('role_status');

            $roles->DateCreate = now();
            $roles->UserCreate = $check_header['id'];
            $roles->LastUpdateDate = now();
            $roles->LastUpdateUser = $check_header['id'];

            if ($roles->save()) {
                // add role auth
                $role_screen = RoleScreen::all();
                for ($i = 0; $i < count($role_screen); $i++){
                    $role_auth = new RoleAuthentication();
                    $role_auth->RoleSkey = $roles->RoleCode ;
                    $role_auth->ScreenSkey = $role_screen[$i]->ScreenNo;
                    $role_auth->PermissionType = 'Invisible';
                    $role_auth->UserCreate = $check_header['id'];
                    $role_auth->DateCreate = now();
                    $role_auth->UserUpdate = $check_header['id'];
                    $role_auth->DateUpdate = now();
                    $role_auth->save();
                }

                return response()->json([
                    "status" => "success",
                    "message" => "created"
                ], 201);
            } else {
                return response()->json([
                    "status" => "error",
                    "message" => "Bad Request"
                ], 400);
            }
            return $roles;

        } catch (\Exception $exception)
        {
            return $exception;
        }

    }


    // get cr role
    public function GetCRRole(Request $request){
        $allow_header = GlobalController::CheckHeader($request);
        $check_header = json_decode($allow_header->content(), true);
        if ($check_header['status'] == 'error')
        {
            return $allow_header;
        }

        try {
            $cr_role = CrRole::join('approves', 'approves.ApproveKey', '=', 'cr_roles.ApproveKey')->get();


            foreach ($cr_role as $data){

                $data['CRAuth'] =  DB::table('c_r_role_authentications')->where('ApproveKey', '=', $data->ApproveKey)->get();
            }

            return $cr_role;

        } catch (\Exception $exception){
            return $exception;
        }


    }

    // create role
    public function UpdateCRRole(Request $request){
        $allow_header = GlobalController::CheckHeader($request);
        $check_header = json_decode($allow_header->content(), true);
        if ($check_header['status'] == 'error')
        {
            return $allow_header;
        }
        try {
            $role = Role::where('id', '=', $request->get('id'))->first();
            $haveRole = Role::where('RoleCode', '=', $request->get('role_code'))->first();

            if($haveRole !== null and $haveRole->id !== $role->id){
                return response()->json([
                    "status" => "error",
                    "message" => "Duplicate Role Code "
                ], 400);
            }



            $last = GlobalController::CreateApprove($check_header['id'], 'Update Role', 'role', $check_header);
            $roles = new CrRole();
            $roles->ApproveKey = $last;
            $roles->RoleCode = $request->get('role_code');
            $roles->RoleName = $request->get('role_name');
            $roles->RoleStatus = $request->get('role_status');
            $roles->UserCreate = $role->UserCreate;
            $roles->DateCreate = $role->DateCreate;
            $roles->LastUpdateDate = now();
            $roles->LastUpdateUser = $check_header['id'];

            $arrEdit = $request->get('role_edit');
            $roles->YellowName = $arrEdit[0]['role_name'];
            $roles->YellowStatus = $arrEdit[1]['role_status'];

            $arrScreen = $request->get('screen');


            for ($i = 0; $i < count($arrScreen); $i++){
                $role_auth = new CRRoleAuthentications();
                $role_auth->ApproveKey = $last;
                $role_auth->RoleSkey = $roles->RoleCode ;
                $role_auth->ScreenSkey = $arrScreen[$i]['screenSkey'];
                $role_auth->PermissionType = $arrScreen[$i]['permission'];
                $role_auth->Yellow = $arrScreen[$i]['yellow'];
                $role_auth->UserCreate = $check_header['id'];
                $role_auth->DateCreate = now();
                $role_auth->UserUpdate = $check_header['id'];
                $role_auth->DateUpdate = now();
                $role_auth->save();
            }

            if ($roles->save()) {
                return response()->json([
                    "status" => "success",
                    "message" => "update"
                ], 200);
            } else {
                return response()->json([
                    "status" => "error",
                    "message" => "Bad Request"
                ], 400);
            }
            return $roles;

        } catch (\Exception $exception)
        {
            return $exception;
        }
    }

    // remember col roles of user
    public function StateRoles(Request $request){
        $allow_header = GlobalController::CheckHeader($request);
        $check_header = json_decode($allow_header->content(), true);
        if ($check_header['status'] == 'error')
        {
            return $allow_header;
        }

        try {
            $emp = Employee::where('id', '=', $check_header['id'])->first();
            $emp->StateRoles = $request->get('data');

            if ($emp->save()) {
                return response()->json([
                    "status" => "success",
                    "message" => "update"
                ], 200);
            } else {
                return response()->json([
                    "status" => "error",
                    "message" => "Bad Request"
                ], 400);
            }

        } catch (\Exception $exception){
            return $this->ErrorDataBase();
        }
    }

    // get col roles of user
    public function GetStateRoles(Request $request){
        $allow_header = GlobalController::CheckHeader($request);
        $check_header = json_decode($allow_header->content(), true);
        if ($check_header['status'] == 'error')
        {
            return $allow_header;
        }

        try {
            $emp = Employee::where('id', '=', $check_header['id'])->first();
            return response()->json([
                "status" => "success",
                "message" => $emp->StateRoles
            ], 200);

        } catch (\Exception $exception){
            return $this->ErrorDataBase();
        }
    }

    // get all row
    public function GetAllRoles(Request $request){
        $allow_header = GlobalController::CheckHeader($request);
        $check_header = json_decode($allow_header->content(), true);
        if ($check_header['status'] == 'error')
        {
            return $allow_header;
        }

        try {

            $roles = Role::all();
            foreach ($roles as $role){
                $role['Auth'] = RoleAuthentication::where('RoleSkey', '=', $role->RoleCode)->get();
            }
            return $roles;

        } catch (\Exception $exception){
            return $this->ErrorDataBase();
        }
    }

    // sort roles
    public function SortRoles(Request $request){
        $allow_header = GlobalController::CheckHeader($request);
        $check_header = json_decode($allow_header->content(), true);
        if ($check_header['status'] == 'error')
        {
            return $allow_header;
        }

        try {
            $sorts = $request->get("array_sort");
            $role = DB::table('roles');
            foreach ($sorts as $sort){
                if($sort['order'] == "maxtomin"){
                    $role = $role->orderBy($sort['coloum_name'], 'desc');
                }else{
                    $role = $role->orderBy($sort['coloum_name'], 'asc');
                }
            }
            $role = $role->get();
            foreach ($role as $r){
                $r->Auth =  RoleAuthentication::where('RoleSkey', '=', $r->RoleCode)->get();
            }
            return response()->json([
                "status" => "success",
                "message" => $role
            ], 200);
        }catch (\Exception $exception){
            return $exception;
        }
    }

    // =================== Role Screen ===============================

    // Create Roles Screen
    public function CreateRolesScreen(Request $request){
        $allow_header = GlobalController::CheckHeader($request);
        $check_header = json_decode($allow_header->content(), true);
        if ($check_header['status'] == 'error')
        {
            return $allow_header;
        }

        try {
            $no = DB::table('role_screens')->orderBy('ScreenNo', 'desc')->select('ScreenNo')->get();
            $last = $no[0]->ScreenNo;
            $last++;

            $roles = new RoleScreen();
            $roles->ScreenNo = $last;
            $roles->ScreenName = $request->get('role_name');
            $roles->ScreenStatus = $request->get('role_status');
            $roles->DateCreate = now();
            $token = $request->header('authorization');
            $token = explode(" ",$token);
            $user = Employee::where('bearer_token', '=', $token[1])->first();
            $roles->UserCreate = $user['id'];

            if ($roles->save()) {
                return response()->json([
                    "status" => "success",
                    "message" => "created"
                ], 201);
            } else {
                return response()->json([
                    "status" => "error",
                    "message" => "Bad Request"
                ], 400);
            }
            return $roles;

        } catch (\Exception $exception)
        {
            return $this->ErrorDataBase();
        }
    }


    public function GetAllRolesScreen(Request $request){
        $allow_header = GlobalController::CheckHeader($request);
        $check_header = json_decode($allow_header->content(), true);
        if ($check_header['status'] == 'error') return $allow_header;
        try {
            $roleAuth = RoleScreen::all();
            return $roleAuth;
        }catch (\Exception $exception){
            return $this->ErrorDataBase();
        }
    }

    // =================== Role Auth ===============================

    public function GetAllRolesAuth(Request $request){
        $allow_header = GlobalController::CheckHeader($request);
        $check_header = json_decode($allow_header->content(), true);
        if ($check_header['status'] == 'error') return $allow_header;
        try {
            $roleAuth = RoleAuthentication::all();
            return $roleAuth;
        }catch (\Exception $exception){
            return $this->ErrorDataBase();
        }
    }

    public function UpdateRolesAuth(Request $request){
        $allow_header = GlobalController::CheckHeader($request);
        $check_header = json_decode($allow_header->content(), true);
        if ($check_header['status'] == 'error') return $allow_header;
        try {
            $id = $request->get('role_skey');
            $arrScreen = $request->get('screen_no');
            $arrPermission = $request->get('permission');

            $last = GlobalController::CreateApprove($check_header['id'], 'create_role', 'role');


            $roles = new CrRole();
            $roles->ApproveKey = $last;
            $roles->RoleCode = $id;
            $roles->RoleName = $request->get('role_name');
            $roles->RoleStatus = $request->get('role_status');
            $roles->DateCreate = now();
            $roles->UserCreate = $check_header['id'];
            $roles->LastUpdateDate = now();
            $roles->LastUpdateUser = $check_header['id'];
            $roles->save();

                // add role auth
                $role_screen = RoleScreen::all();
                $countPermission = 0;
                for ($i = 0; $i < count($role_screen); $i++){
                    $role_auth = new CRRoleAuthentications();
                    $role_auth->ApproveKey = $last;
                    $role_auth->RoleSkey = $roles->RoleCode ;
                    $role_auth->ScreenSkey = $role_screen[$i]->ScreenNo;

                    $role_auth->PermissionType = 'Invisible';
                    $role_auth->UserCreate = $check_header['id'];
                    $role_auth->DateCreate = now();
                    $role_auth->UserUpdate = $check_header['id'];
                    $role_auth->DateUpdate = now();


                    if ($arrScreen[$countPermission] == $role_auth->ScreenSkey){
                        $role_auth->PermissionType = $arrPermission[$countPermission];
                        $countPermission++;
                    }

                    $role_auth->save();

                }


            for ($i = 0; $i < count($arrPermission); $i++){
                $roleAuth = RoleAuthentication::where([
                    ['RoleSkey', '=', $id],
                    ['ScreenSkey', '=', $arrScreen[$i]],
                ])->get();

                $roleRole = RoleScreen::where('ScreenNo', '=', $arrScreen[$i])->first();
                if ($roleRole->ScreenNo == $roleAuth[0]->ScreenSkey){
                    $roleAuth[0]->PermissionType = $arrPermission[$i];
                    $roleAuth[0]->save();
                }
            }

            return response()->json([
                "status" => "success",
                "message" => "updated"
            ], 200);
        }catch (\Exception $exception){
            return $exception;
        }
    }

}
