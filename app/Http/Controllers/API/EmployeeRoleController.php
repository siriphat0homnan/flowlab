<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Providers\Model\CREmployee;
use App\Providers\Model\Employee;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class EmployeeRoleController extends Controller
{
    public function GetEmployeeNotRole(Request $request){
        $allow_header = GlobalController::CheckHeader($request);
        $check_header = json_decode($allow_header->content(), true);
        if ($check_header['status'] == 'error') return $allow_header;

        try {
            $data = DB::table('employees')->where('RoleCodeSkey', '=', null)->select('EmployeeNo', 'PrefixEng', 'NameEng', 'SurNameEng')->get();

            return $data;

        }catch (\Exception $exception){
            return $this->ErrorDataBase();

        }
    }

    public function SetEmployeeRole(Request $request){
        $allow_header = GlobalController::CheckHeader($request);
        $check_header = json_decode($allow_header->content(), true);
        if ($check_header['status'] == 'error') return $allow_header;

        try {
            $role = $request->get("role");
            $emps = $request->get("employee_no");

            foreach ($emps as $emp){
                $lastApprove = GlobalController::CreateApprove($check_header['id'], 'Authentication of Employee', 'roleemp', $check_header);
//                $user = Employee::where('EmployeeNo', '=', $emp)->get();
//                $user = $user[0];
                $cr_emp = new CREmployee();
                $emp = Employee::where("EmployeeNo", '=', $emp)->get();
                $emp = $emp[0];
                $cr_emp->ApproveKey =  $lastApprove;
                $cr_emp->PrefixEng = $emp['PrefixEng'];
                $cr_emp->NameEng = $emp['NameEng'];
                $cr_emp->SurNameEng = $emp['SurNameEng'];
                $cr_emp->EmployeeNo = $emp['EmployeeNo'];

                $cr_emp->RoleCodeSkey = $role;


                $cr_emp->save();
//                $changeRole = ::where('EmployeeNo', '=', $emp)->first();
//                $changeRole->RoleCodeSkey = $role;
//                $changeRole->save();
            }

            return response()->json([
                "status" => "success",
            ], 201);

        }catch (\Exception $exception){
            return $exception;

        }
    }

    public function GetEmployeeRole(Request $request){
        $allow_header = GlobalController::CheckHeader($request);
        $check_header = json_decode($allow_header->content(), true);
        if ($check_header['status'] == 'error') return $allow_header;

        try {
//            $data = Employee::whereNotNull('RoleCodeSkey')->select('EmployeeNo', 'PrefixEng', 'NameEng', 'SurNameEng')->get();
            $data = DB::table('employees')->whereNotNull('RoleCodeSkey')
                ->join('roles', 'employees.RoleCodeSkey', '=', 'roles.RoleCode')
                ->select('roles.RoleCode', 'roles.RoleName', 'employees.EmployeeNo', 'employees.PrefixEng', 'employees.NameEng', 'employees.SurNameEng')->get();

            return $data;

        }catch (\Exception $exception){
            return $this->ErrorDataBase();

        }
    }

    public function SearchEmployeeRole(Request $request){
        $allow_header = GlobalController::CheckHeader($request);
        $check_header = json_decode($allow_header->content(), true);
        if ($check_header['status'] == 'error') return $allow_header;

        try {
            $data = DB::table('employees')->where('RoleCodeSkey', '=', $request->get('role_code'))
                ->join('roles', 'employees.RoleCodeSkey', '=', 'roles.RoleCode')
                ->select('roles.RoleCode', 'roles.RoleName', 'employees.EmployeeNo', 'employees.PrefixEng', 'employees.NameEng', 'employees.SurNameEng')->get();

//            return $data;
//            $data = DB::table('employees')->where('RoleCodeSkey', '=', $request->get('role_code'))->select('EmployeeNo', 'PrefixEng', 'NameEng', 'SurNameEng')->get();


            return $data;

        }catch (\Exception $exception){
            return $this->ErrorDataBase();

        }
    }



}
