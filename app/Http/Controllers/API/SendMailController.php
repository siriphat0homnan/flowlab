<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Providers\Model\Employee;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;

class SendMailController extends Controller
{
    public function SendEmailForForgotPassword(Request $request)
    {
        try {
            $to_name = 'User';
            $to_email = $request->get('email');
            $user = Employee::where('email', '=', $to_email)->first();
            $pass = Str::random(10);
            $password = Hash::make($pass);
            $user->password = $password;
            $data = array('msg'=>'กรุณาเปลี่ยนพาสเวิร์ด', 'password'=> $pass, 'body' => 'Change Password');
            Mail::send('mail_forget_password', $data, function($message) use ($to_name, $to_email) {

                $message->to($to_email, $to_name)->subject('Change Password');
                $message->from('SM_flowlab_service@hotmail.com','Change Password');
            });
        } catch (\Exception $exception)
        {
            return $exception;
        }
        return response()->json(['result' => "send success"], 200);
    }


    // send mail when create employee
    static public function SendEmailToEmployeeForChangePassword($email, $name, $surname, $pass)
    {
        try {
            $to_name = $name;
            $to_email = $email;
            $data = array('msg'=>'กรุณาเปลี่ยนพาสเวิร์ด', 'password' => $pass, 'body' => 'Change Password');
            Mail::send('mail_forget_password', $data, function($message) use ($to_name, $to_email) {
                $message->to($to_email, $to_name)->subject('Change Password');
                $message->from('SM_flowlab_service@hotmail.com','Change Password');
            });
        } catch (\Exception $exception)
        {
            return $exception;
        }
        return response()->json(['result' => "send success"], 200);
    }

}
