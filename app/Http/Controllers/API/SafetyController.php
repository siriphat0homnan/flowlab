<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SafetyController extends Controller
{
    //
    // get training rule
    public function GetTrainingRule(Request $request){
        $allow_header = GlobalController::CheckHeader($request);
        $check_header = json_decode($allow_header->content(), true);
        if ($check_header['status'] == 'error') {
            return $allow_header;
        }
        try {
            $training_rules = DB::table('training_rules')->get();
            return $training_rules;
        } catch (\Exception $exception)
        {
            return $this->ErrorDataBase();
        }
    }

    // search training rule
    public function SearchTrainingRule(Request $request){
        $allow_header = GlobalController::CheckHeader($request);
        $check_header = json_decode($allow_header->content(), true);
        if ($check_header['status'] == 'error') {
            return $allow_header;
        }
        try {

            $training_rules = DB::table('training_rules');

            if (!empty($request->get('training_rules_code'))){
                $first = $request->get("training_rules_code");
                $second = $request->get("training_rules_code_to");
                if($second){
                    $training_rules = GlobalController::SearchBetween($first, $second, 'TrainingRuleCode', $training_rules);
                }
                else{
                    $training_rules->Where('TrainingRuleCode','LIKE','%'.$request->get("training_rules_code").'%');
                }
            }

            if (!empty($request->get('training_rules_type'))){
                $training_rules->Where('TrainingRuleType','LIKE','%'.$request->get("training_rules_type").'%');
            }

            // ขาด Customer, Customer Location, Training Rule Status because must to join in customer table
//
////            if (!empty($request->get('customer'))){
////                $training_rules->Where('CustomerSkey', '=', $request->get("customer"));
////            }
////            if (!empty($request->get('customer_location'))){
////                $training_rules->Where('LocationSkey', '=', $request->get("customer_location"));
////            }
////            if (!empty($request->get('training_rules_status'))){
////                $training_rules->Where('TrainingRuleStatus', '=', $request->get("training_rules_status"));
////            }

            if (!empty($request->get('create_date'))){
                $first = $request->get("create_date");
                $second = $request->get("create_date_to");
                if($second){
                    $training_rules = GlobalController::SearchBetween($first, $second, 'DateCreate', $training_rules);
                }
                else{
                    $training_rules->Where('DateCreate','LIKE','%'.$request->get("create_date").'%');
                }
            }

            $data = $training_rules->get();
            $array = json_decode(json_encode($data), true);

            return $array;
        } catch (\Exception $exception)
        {
            return $this->ErrorDataBase();
        }
    }
}
