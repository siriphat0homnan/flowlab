<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Providers\Model\Approve;
use App\Providers\Model\CRCustomer;
use App\Providers\Model\CRCustomerContact;
use App\Providers\Model\CRCustomerLocation;
use App\Providers\Model\CREmployee;
use App\Providers\Model\CREmployeeAttachedFiles;
use App\Providers\Model\CREmployeeChildrens;
use App\Providers\Model\CrRole;
use App\Providers\Model\CRRoleAuthentications;
use App\Providers\Model\Customer;
use App\Providers\Model\CustomerContact;
use App\Providers\Model\CustomerLocation;
use App\Providers\Model\Employee;
use App\Providers\Model\EmployeeAttachedFile;
use App\Providers\Model\EmployeeChildren;
use App\Providers\Model\Role;
use App\Providers\Model\RoleAuthentication;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;

class ApproveController extends Controller
{
    // get approve
    public function GetApprove(Request $request){
        $allow_header = GlobalController::CheckHeader($request);
        $check_header = json_decode($allow_header->content(), true);
        if ($check_header['status'] == 'error') return $allow_header;
        try {
            $approves = Approve::where('CRStatus', '=', 'Pending')->get();
            foreach ($approves as $approve){
                if ($approve->Master == 'role'){
                    $approve['CRRole'] = CrRole::where('ApproveKey', '=', $approve->ApproveKey)->get();
                    foreach ($approve['CRRole'] as $cr_auth){
                        $cr_auth['CRAuth'] = CRRoleAuthentications::where('ApproveKey', '=', $approve->ApproveKey)->get();
                    }
                }
                if ($approve->Master == 'employee'){
                    $approve['CREmployee'] = CREmployee::where('ApproveKey', '=', $approve->ApproveKey)->get();
                    foreach ($approve['CREmployee'] as $cr){
                        array_walk_recursive($cr,function(&$item){$item=strval($item);});

                        $cr['CRChildren'] = CREmployeeChildrens::where('ApproveKey', '=', $approve->ApproveKey)->get();
                        if ($cr['CRChildren']->isNotEmpty()) {
                            for ($j = 0; $j < count($cr['CRChildren']); $j++){
                                array_walk_recursive($cr['CRChildren'][$j],function(&$item){$item=strval($item);});
                            }
                        }

                        $cr['CRFile'] = CREmployeeAttachedFiles::where('ApproveKey', '=', $approve->ApproveKey)->get();
                        if ($cr['CRFile']->isNotEmpty()) {
                            for ($j = 0; $j < count($cr['CRFile']); $j++){
                                array_walk_recursive($cr['CRFile'][$j],function(&$item){$item=strval($item);});
                            }
                        }
                    }
                }
                if ($approve->Master == 'roleemp'){
                    $approve['CREmployee'] = CREmployee::where('ApproveKey', '=', $approve->ApproveKey)->get();
                    foreach ($approve['CREmployee'] as $cr){
                        $cr['CRChildren'] = CREmployeeChildrens::where('ApproveKey', '=', $approve->ApproveKey)->get();
                        $cr['CRFile'] = CREmployeeAttachedFiles::where('ApproveKey', '=', $approve->ApproveKey)->get();
                    }
                }
                if ($approve->Master == 'customer'){
                    $approve['CRCustomer'] = CRCustomer::where('ApproveKey', '=', $approve->ApproveKey)->get();
                    foreach ($approve['CRCustomer'] as $cr){
                        $cr['CRLocation'] = CRCustomerLocation::where('ApproveKey', '=', $approve->ApproveKey)->get();
                        $cr['CRContact'] = CRCustomerContact::where('ApproveKey', '=', $approve->ApproveKey)->get();
                    }
                }
            }
            return $approves;
        }catch (\Exception $exception) {
            return $exception;
        }
    }
    // get approve
    public function GetApproveOfUser(Request $request){
        $allow_header = GlobalController::CheckHeader($request);
        $check_header = json_decode($allow_header->content(), true);
        if ($check_header['status'] == 'error') return $allow_header;
        try {
            $approves = Approve::where('ApproveUserCreate', '=', $check_header['id'])->get();
            foreach ($approves as $approve){
                if ($approve->Master == 'role'){
                    $approve['CRRole'] = CrRole::where('ApproveKey', '=', $approve->ApproveKey)->get();
                    foreach ($approve['CRRole'] as $cr_auth){
                        $cr_auth['CRAuth'] = CRRoleAuthentications::where('ApproveKey', '=', $approve->ApproveKey)->get();
                    }
                }
                if ($approve->Master == 'employee'){
                    $approve['CREmployee'] = CREmployee::where('ApproveKey', '=', $approve->ApproveKey)->get();
                    foreach ($approve['CREmployee'] as $cr){
                        array_walk_recursive($cr,function(&$item){$item=strval($item);});

                        $cr['CRChildren'] = CREmployeeChildrens::where('ApproveKey', '=', $approve->ApproveKey)->get();
                        if ($cr['CRChildren']->isNotEmpty()) {
                            for ($j = 0; $j < count($cr['CRChildren']); $j++){
                                array_walk_recursive($cr['CRChildren'][$j],function(&$item){$item=strval($item);});
                            }
                        }

                        $cr['CRFile'] = CREmployeeAttachedFiles::where('ApproveKey', '=', $approve->ApproveKey)->get();
                        if ($cr['CRFile']->isNotEmpty()) {
                            for ($j = 0; $j < count($cr['CRFile']); $j++){
                                array_walk_recursive($cr['CRFile'][$j],function(&$item){$item=strval($item);});
                            }
                        }
                    }
                }
                if ($approve->Master == 'roleemp'){
                    $approve['CREmployee'] = CREmployee::where('ApproveKey', '=', $approve->ApproveKey)->get();
                    foreach ($approve['CREmployee'] as $cr){
                        $cr['CRChildren'] = CREmployeeChildrens::where('ApproveKey', '=', $approve->ApproveKey)->get();
                        $cr['CRFile'] = CREmployeeAttachedFiles::where('ApproveKey', '=', $approve->ApproveKey)->get();
                    }
                }
                if ($approve->Master == 'customer'){
                    $approve['CRCustomer'] = CRCustomer::where('ApproveKey', '=', $approve->ApproveKey)->get();
                    foreach ($approve['CRCustomer'] as $cr){
                        $cr['CRLocation'] = CRCustomerLocation::where('ApproveKey', '=', $approve->ApproveKey)->get();
                        $cr['CRContact'] = CRCustomerContact::where('ApproveKey', '=', $approve->ApproveKey)->get();
                    }
                }
            }
            return $approves;
        }catch (\Exception $exception) {
            return $exception;
        }
    }

    //
    public function ReturnApprove(Request $request){
        $allow_header = GlobalController::CheckHeader($request);
        $check_header = json_decode($allow_header->content(), true);
        if ($check_header['status'] == 'error') return $allow_header;
        try {
            $confirm = $request->get("confirm");

            foreach ($confirm as $approve_key){
                $approve = Approve::where('ApproveKey', '=', $approve_key)->get();
                $approve = $approve[0];
                if($approve['CRStatus'] == 'Pending'){
                    // role
                    $approve['CRStatus'] = 'Return';
                    $approve['CRReturnReason'] = $request->get("reason");
                    $approve['CRReturnDate'] = now();
                    $approve['CRApproveUser'] = $check_header['id'];
                    $approve['CRApproveDate'] = now();
                    $approve->save();
                }
            }

            return response()->json([
                "status" => "Return",
                "message" => "Success"
            ], 200);
        }catch (\Exception $exception) {
            return $exception;
        }
    }

    // confirm approve
    public function ConfirmApprove(Request $request){
        $allow_header = GlobalController::CheckHeader($request);
        $check_header = json_decode($allow_header->content(), true);
        if ($check_header['status'] == 'error') return $allow_header;
        try {
            $confirm = $request->get("confirm");

            foreach ($confirm as $approve_key){
                $approve = Approve::where('ApproveKey', '=', $approve_key)->get();
                $approve = $approve[0];
                if($approve['CRStatus'] == 'Pending'){
                    // role
                    if($approve['Master'] == 'role'){
                        ApproveController::Role($approve, $check_header);
                    }
                    if($approve['Master'] == 'employee'){

                        $res = ApproveController::Employee($approve, $check_header);
                        $to_name = $res[1];
                        $to_email = $res[0];
                        $data = array('msg'=>'กรุณาเปลี่ยนพาสเวิร์ด', 'password' => $res[3], 'body' => 'Change Password');
                        Mail::send('mail_forget_password', $data, function($message) use ($to_name, $to_email) {
                            $message->to($to_email, $to_name)->subject('Change Password');
                            $message->from('SM_flowlab_service@hotmail.com','Change Password');
                        });
//                        return SendMailController::SendEmailToEmployeeForChangePassword($res[0], 'test', 'test', 'test');

                    }
                    if($approve['Master'] == 'roleemp'){
                        ApproveController::EmployeeRole($approve, $check_header);
                    }
                    if($approve['Master'] == 'customer'){
                        ApproveController::Customer($approve, $check_header);
                    }
                }
            }

            return response()->json([
                "status" => "confirm",
                "message" => "Success"
            ], 200);

        }catch (\Exception $exception) {
            return $exception;
        }

    }

    public function Role($approve, $check_header){
        // create role
        $cr_role = CrRole::where('ApproveKey', '=', $approve['ApproveKey'])->get();
        $cr_auth= CRRoleAuthentications::where('ApproveKey', '=', $approve['ApproveKey'])->get();

        if($approve['CRType'] == 'Create Role'){
            $role = new Role();
            $role->RoleCode = $cr_role[0]['RoleCode'];
            $role->RoleName = $cr_role[0]['RoleName'];
            $role->RoleStatus = $cr_role[0]['RoleStatus'];
            $role->DateCreate = $cr_role[0]['DateCreate'];
            $role->UserCreate = $cr_role[0]['UserCreate'];

            foreach ($cr_auth as $data){
                $auth = new RoleAuthentication();
                $auth->RoleSkey = $data['RoleSkey'];
                $auth->ScreenSkey = $data['ScreenSkey'];
                $auth->PermissionType = $data['PermissionType'];
                $auth->DateCreate = $data['DateCreate'];
                $auth->UserCreate = $data['UserCreate'];
                $auth->DateUpdate = $data['DateUpdate'];
                $auth->UserUpdate = $data['UserUpdate'];
                // delete when reject
                if(!$auth->save()){
                    $dels = RoleAuthentication::where('RoleSkey', '=', $data['RoleSkey'])->get();
                    foreach ($dels as $del){
                        $del->delete();
                    }
                    return response()->json([
                        "status" => "error",
                        "message" => "Create Reject"
                    ], 400);
                }
            }
            if(!$role->save()){
                return response()->json([
                    "status" => "error",
                    "message" => "Create Reject"
                ], 400);
            }else{
                $approve['CRStatus'] = 'Success';
                $approve['CRApproveUser'] = $check_header['id'];
                $approve['CRApproveDate'] = now();
                $approve->save();
            }
            // update
        }
        else if($approve['CRType'] == 'Update Role'){

            $role = Role::where('RoleCode', '=', $cr_role[0]['RoleCode'])->get();
            $auth = RoleAuthentication::where('RoleSkey', '=', $cr_role[0]['RoleCode'])->get();
            $role[0]->RoleCode = $cr_role[0]['RoleCode'];
            $role[0]->RoleName = $cr_role[0]['RoleName'];
            $role[0]->RoleStatus = $cr_role[0]['RoleStatus'];
            $role[0]->DateCreate = $cr_role[0]['DateCreate'];
            $role[0]->UserCreate = $cr_role[0]['UserCreate'];
            $role[0]->LastUpdateDate = $cr_role[0]['LastUpdateDate'];
            $role->LastUpdateUser = $cr_role[0]['LastUpdateUser'];

            $i = 0;
            foreach ($cr_auth as $data){
                $auth[$i]->RoleSkey = $data['RoleSkey'];
                $auth[$i]->ScreenSkey = $data['ScreenSkey'];
                $auth[$i]->PermissionType = $data['PermissionType'];
                $auth[$i]->DateCreate = $data['DateCreate'];
                $auth[$i]->UserCreate = $data['UserCreate'];
                $auth[$i]->DateUpdate = $data['DateUpdate'];
                $auth[$i]->UserUpdate = $data['UserUpdate'];
                // delete when reject
                if(!$auth[$i]->save()){

                    $dels = RoleAuthentication::where('RoleSkey', '=', $data['RoleSkey'])->get();
                    foreach ($dels as $del){
                        $del->delete();
                    }
                    return response()->json([
                        "status" => "error",
                        "message" => "Update Reject"
                    ], 400);
                }
                $i++;
            }
            if(!$role[0]->save()){

                return response()->json([
                    "status" => "error",
                    "message" => "Update Reject"
                ], 400);
            }else{
                $approve['CRStatus'] = 'Success';
                $approve['CRApproveUser'] = $check_header['id'];
                $approve['CRApproveDate'] = now();
                $approve->save();
            }
        }
    }
    public function Employee($approve, $check_header){
        // create role

        $cr_emp = CREmployee::where('ApproveKey', '=', $approve['ApproveKey'])->get();
        $cr_emp = $cr_emp[0];

        if($approve['CRType'] == 'Create Employee'){
            $user = new Employee();
            $user->email = $cr_emp['email'];
            $user->password = $cr_emp['password'];
            $user->old_password = $cr_emp['old_password'];
            $user->remember_token = $cr_emp['remember_token'];
            $user->EmployeeNo = $cr_emp['EmployeeNo'];
            $user->PrefixThai = $cr_emp['PrefixThai'];
            $user->NameThai = $cr_emp['NameThai'];
            $user->SurNameThai = $cr_emp['SurNameThai'];
            $user->PrefixEng = $cr_emp['PrefixEng'];
            $user->NameEng = $cr_emp['NameEng'];
            $user->SurNameEng = $cr_emp['SurNameEng'];
            $user->NickName = $cr_emp['NickName'];
            $user->IDCard = $cr_emp['IDCard'];
            $user->MobilePhone = $cr_emp['MobilePhone'];
            $user->BirthDate = $cr_emp['BirthDate'];
            $user->StartWorkingDate = $cr_emp['StartWorkingDate'];
            $user->ResignDate =$cr_emp['ResignDate'];
            $user->EmployeeStatus = $cr_emp['EmployeeStatus'];
            $user->Nationality = $cr_emp['Nationality'];
            $user->Origin = $cr_emp['Origin'];
            $user->Religious = $cr_emp['Religious'];
            $user->MaritalStatus = $cr_emp['MaritalStatus'];
            $user->Note = $cr_emp['Note'];
            $user->PostCode = $cr_emp['PostCode'];
            $user->Address = $cr_emp['Address'];
            $user->FacebookID = $cr_emp['FacebookID'];
            $user->LineID = $cr_emp['LineID'];
            $user->DateCreate = now();
            $user->SubDistrict= $cr_emp['SubDistrict'];
            $user->District = $cr_emp['District'];
            $user->Province = $cr_emp['Province'];
            $user->Country = $cr_emp['Country'];

            // fix data
            $user->UserCreate = $check_header['id'];
            $user->StateRoles = "RoleCode,RoleStatus,UserCreate";
            $user->StateEmployee = "id,EmployeeNo,NameEng,SurNameEng,EmployeeStatus,email,MobilePhone";
            $user->StateEmployeeChild= "LineNo,ChildrenName,ChildrenLastName,Sex,Birthdate,IdCard";
            $user->StateEmployeeAttach= 'Status,EmpAttachedListNo,AttachedPath,FileName	DateCreate,UserCreate';
            $user->StateCustomer="CustomerCode,CustomerName,CustomerStatus,DateCreate,UserCreate,LastUpdateDate,LastUpdateUser";
            $user->StateCustomerLocation="LineNo,LocationNameThai,LocationNameEng,LocationInitialsName,Address,LocationStatus,Distance,Latitude,Longitude";
            $user->StateCustomerContact="LineNo,PrefixThai,NameThai,SurnameThai,PrefixEng,NameEng,SurnameEng,Nickname,Telephone,Mobilephone";
            $user->StateApprove = "ApproveKey,Master,CRType,CRStatus";
            $user->StateApproveOfUser = "ApproveKey,Master,CRType,CRStatus";

//          id 1, 2, 3
            $user->EmployeeTypeID = $cr_emp['EmployeeTypeID'];
            $user->PositionID = $cr_emp['PositionID'];
            $user->SectionID = $cr_emp['SectionID'];
            $user->DepartmentID = $cr_emp['DepartmentID'];

            $cr_child = CREmployeeChildrens::where('ApproveKey', '=', $approve['ApproveKey'])->get();
            foreach ($cr_child as $data){
                $child = new EmployeeChildren();
                $child->Status = $data['Status'];
                $child->LineNo = $data['LineNo'];
                $child->ChildrenName = $data['ChildrenName'];
                $child->ChildrenLastName = $data['ChildrenLastName'];
                $child->Sex = $data['Sex'];
                $child->Birthdate = $data['Birthdate'];
                $child->IdCard = $data['IdCard'];
                // delete when reject
                if(!$child->save()){
                    $dels = EmployeeChildren::where('LineNo', '=', $data['LineNo'])->get();
                    foreach ($dels as $del){
                        $del->delete();
                    }
                    return response()->json([
                        "status" => "error",
                        "message" => "Create Reject"
                    ], 400);
                }
            }

            $cr_file= CREmployeeAttachedFiles::where('ApproveKey', '=', $approve['ApproveKey'])->get();
            foreach ($cr_file as $data){
                $file = new EmployeeAttachedFile();
                $file->Status = $data['Status'];
                $file->EmpAttachedListNo = $data['EmpAttachedListNo'];
                $file->AttachedPath = '/images/approve/'.$data['AttachedPath'];
                $file->FileName = $data['FileName'];
                $file->DateCreate = $data['DateCreate'];
                $file->UserCreate = $data['UserCreate'];
//                Storage::move('old/file.jpg', 'new/file.jpg');
                \File::move( public_path(). '/images/waiting_for_approve/'. $data['AttachedPath'], public_path(). '/images/approve/'. $data['AttachedPath']);

                // delete when reject
                if(!$file->save()){
                    $dels = EmployeeChildren::where('LineNo', '=', $data['LineNo'])->get();
                    foreach ($dels as $del){
                        $del->delete();
                    }
                    $dels = EmployeeAttachedFile::where('EmpAttachedListNo', '=', $data['EmpAttachedListNo'])->get();
                    foreach ($dels as $del){
                        $del->delete();
                    }
                    return response()->json([
                        "status" => "error",
                        "message" => "Create Reject"
                    ], 400);
                }
            }

            if(!$user->save()){
                $dels = EmployeeChildren::where('LineNo', '=', $data['LineNo'])->get();
                foreach ($dels as $del){
                    $del->delete();
                }
                $dels = EmployeeAttachedFile::where('EmpAttachedListNo', '=', $data['EmpAttachedListNo'])->get();
                foreach ($dels as $del){
                    $del->delete();
                }
                return response()->json([
                    "status" => "error",
                    "message" => "Create Reject"
                ], 400);
            }else{
////                 send mail to employee for change password
//                SendMailController::SendEmailToEmployeeForChangePassword($cr_emp['email'], $cr_emp['NameEng'], $cr_emp['SurNameEng'], $cr_emp['password']);

                $approve['CRStatus'] = 'Success';
                $approve['CRApproveUser'] = $check_header['id'];
                $approve['CRApproveDate'] = now();
                $approve->save();
                $res = [$cr_emp['email'], $cr_emp['NameEng'], $cr_emp['SurNameEng'], $cr_emp['password']];
                return $res;
            }
            // update
        }

        else if($approve['CRType'] == 'Update Employee'){

            $user = Employee::where('EmployeeNo', '=', $cr_emp['EmployeeNo'])->get();
//            $auth = RoleAuthentication::where('RoleSkey', '=', $cr_emp['RoleCode'])->get();
            $user = $user[0];

            $user->PrefixThai = $cr_emp['PrefixThai'];
            $user->NameThai = $cr_emp['NameThai'];
            $user->SurNameThai = $cr_emp['SurNameThai'];
            $user->PrefixEng = $cr_emp['PrefixEng'];
            $user->NameEng = $cr_emp['NameEng'];
            $user->SurNameEng = $cr_emp['SurNameEng'];
            $user->NickName = $cr_emp['NickName'];
            $user->IDCard = $cr_emp['IDCard'];
            $user->MobilePhone = $cr_emp['MobilePhone'];
            $user->BirthDate = $cr_emp['BirthDate'];
            $user->StartWorkingDate = $cr_emp['StartWorkingDate'];
            $user->ResignDate =$cr_emp['ResignDate'];
            $user->EmployeeStatus = $cr_emp['EmployeeStatus'];
            $user->Nationality = $cr_emp['Nationality'];
            $user->Origin = $cr_emp['Origin'];
            $user->Religious = $cr_emp['Religious'];
            $user->MaritalStatus = $cr_emp['MaritalStatus'];
            $user->Note = $cr_emp['Note'];
            $user->PostCode = $cr_emp['PostCode'];
            $user->Address = $cr_emp['Address'];
            $user->FacebookID = $cr_emp['FacebookID'];
            $user->LineID = $cr_emp['LineID'];
            $user->DateCreate = now();
            $user->SubDistrict= $cr_emp['SubDistrict'];
            $user->District = $cr_emp['District'];
            $user->Province = $cr_emp['Province'];
            $user->Country = $cr_emp['Country'];

            // child
            $cr_child = CREmployeeChildrens::where('ApproveKey', '=', $approve['ApproveKey'])->get();
            $child = EmployeeChildren::where('LineNo', '=', $cr_emp['EmployeeNo'])->get();

            if(count($child) == count($cr_child)){
                $i = 0;
                foreach ($cr_child as $result){
                    $child[$i]->Status = $result['Status'];
                    $child[$i]->ChildrenName = $result['ChildrenName'];
                    $child[$i]->ChildrenLastName = $result['ChildrenLastName'];
                    $child[$i]->Sex = $result['Sex'];
                    $child[$i]->Birthdate = $result['Birthdate'];
                    $child[$i]->IdCard = $result['IdCard'];
                    $child[$i]->save();
                    $i++;
                }
            }

            if(count($child) < count($cr_child)){
                $countIndexChild = 0;
                $diffLen = count($cr_child) - count($child);
                for ($i = 0; $i <= count($child) - $diffLen; $i++ ){
                    $child[$i]->Status = $cr_child[$i]['Status'];
                    $child[$i]->ChildrenName = $cr_child[$i]["ChildrenName"];
                    $child[$i]->ChildrenLastName = $cr_child[$i]["ChildrenLastName"];
                    $child[$i]->Sex = $cr_child[$i]["Sex"];
                    $child[$i]->Birthdate = $cr_child[$i]["Birthdate"];
                    $child[$i]->IdCard = $cr_child[$i]["IdCard"];
                    $child[$i]->save();
                    $countIndexChild++;
                }

                for ($i = $countIndexChild; $i < $countIndexChild + $diffLen; $i++ ){
                    $child = new EmployeeChildren();
                    $child->LineNo = $cr_emp['EmployeeNo'];
                    $child->Status = $cr_child[$i]['Status'];
                    $child->ChildrenName = $cr_child[$i]["ChildrenName"];
                    $child->ChildrenLastName = $cr_child[$i]["ChildrenLastName"];
                    $child->Sex = $cr_child[$i]["Sex"];
                    $child->Birthdate = $cr_child[$i]["Birthdate"];
                    $child->IdCard = $cr_child[$i]["IdCard"];
                    $child->save();
                }
            }

            // attach
            $cr_attach = CREmployeeAttachedFiles::where('ApproveKey', '=', $approve['ApproveKey'])->get();
            $file = EmployeeAttachedFile::where('EmpAttachedListNo', '=', $cr_emp['EmployeeNo'])->get();

            if(count($file) == count($cr_attach)){
                $i = 0;
                foreach ($cr_attach as $result){
                    $file[$i]->Status = $result['Status'];
                    $file[$i]->AttachedPath = $file[$i]->AttachedPath;
                    $file[$i]->EmpAttachedListNo = $file[$i]->EmpAttachedListNo;
                    $file[$i]->FileName = $file[$i]->FileName ;
                    $file[$i]->DateCreate = $file[$i]->DateCreate;
                    $file[$i]->UserCreate = $file[$i]->UserCreate ;
                    $file[$i]->save();
                    $i++;
                }
            }

            if(count($file) < count($cr_attach)){
                $countIndexFile = 0;
                $diffLen = count($cr_attach) - count($file);
                for ($i = 0; $i <= count($file) - $diffLen; $i++ ){

                    $file[$i]->Status = $cr_attach[$i]['Status'];
                    $file[$i]->AttachedPath = $file[$i]->AttachedPath;
                    $file[$i]->EmpAttachedListNo = $file[$i]->EmpAttachedListNo;
                    $file[$i]->FileName = $file[$i]->FileName ;
                    $file[$i]->DateCreate = $file[$i]->DateCreate;
                    $file[$i]->UserCreate = $file[$i]->UserCreate ;
//                    \File::move( public_path(). '/images/waiting_for_approve/'. $result['AttachedPath'], public_path(). '/images/approve/'. $result['AttachedPath']);
                    $file[$i]->save();
                    $countIndexFile++;
                }

                for ($i = $countIndexFile; $i < $countIndexFile + $diffLen; $i++ ){
                    $file = new EmployeeAttachedFile();
                    $file->Status = $cr_attach[$i]['Status'];
                    $file->EmpAttachedListNo = $cr_attach[$i]['EmpAttachedListNo'];
                    $file->AttachedPath = '/images/approve/'.$cr_attach[$i]['AttachedPath'];
                    $file->FileName = $cr_attach[$i]['FileName'];
                    $file->DateCreate = $cr_attach[$i]['DateCreate'];
                    $file->UserCreate = $cr_attach[$i]['UserCreate'];
                    $file->save();
//                Storage::move('old/file.jpg', 'new/file.jpg');
                    \File::move( public_path(). '/images/waiting_for_approve/'. $cr_attach[$i]['AttachedPath'], public_path(). '/images/approve/'. $cr_attach[$i]['AttachedPath']);

                }
            }

            if(!$user->save()){

                return response()->json([
                    "status" => "error",
                    "message" => "Update Reject"
                ], 400);
            }else{
                $approve['CRStatus'] = 'Success';
                $approve['CRApproveUser'] = $check_header['id'];
                $approve['CRApproveDate'] = now();
                $approve->save();
            }
        }

        else if($approve['CRType'] == 'Authentication of Employee') {

            $user = Employee::where('EmployeeNo', '=', $cr_emp['EmployeeNo'])->get();
//            $auth = RoleAuthentication::where('RoleSkey', '=', $cr_emp['RoleCode'])->get();
            $user = $user[0];
            $user->RoleCodeSkey = $cr_emp['RoleCodeSkey'];

            if(!$user->save()){

                return response()->json([
                    "status" => "error",
                    "message" => "Update Reject"
                ], 400);
            }else{
                $approve['CRStatus'] = 'Success';
                $approve['CRApproveUser'] = $check_header['id'];
                $approve['CRApproveDate'] = now();
                $approve->save();
            }
        }
    }

    public function EmployeeRole($approve, $check_header){
        // create role
        $cr_emp = CREmployee::where('ApproveKey', '=', $approve['ApproveKey'])->get();
        $cr_emp = $cr_emp[0];

        if($approve['CRType'] == 'Authentication of Employee') {

            $user = Employee::where('EmployeeNo', '=', $cr_emp['EmployeeNo'])->get();
//            $auth = RoleAuthentication::where('RoleSkey', '=', $cr_emp['RoleCode'])->get();
            $user = $user[0];
            $user->RoleCodeSkey = $cr_emp['RoleCodeSkey'];

            if(!$user->save()){

                return response()->json([
                    "status" => "error",
                    "message" => "Update Reject"
                ], 400);
            }else{
                $approve['CRStatus'] = 'Success';
                $approve['CRApproveUser'] = $check_header['id'];
                $approve['CRApproveDate'] = now();
                $approve->save();
            }
        }
    }
    public function Customer($approve, $check_header){
        // create role
        $cr_cus = CRCustomer::where('ApproveKey', '=', $approve['ApproveKey'])->get();
        $cr_cus = $cr_cus[0];
        if($approve['CRType'] == 'Create Customer') {

            $customer = new Customer();
            $customer->CustomerCode = $cr_cus['CustomerCode'];
            $customer->CustomerNameEng = $cr_cus['CustomerNameEng'];
            $customer->CustomerNameThai = $cr_cus['CustomerNameThai'];
            $customer->CustomerInitialsName = $cr_cus['CustomerInitialsName'];
            $customer->Business = $cr_cus['Business'];
            $customer->Address = $cr_cus['Address'];
            $customer->CustomerStatus = $cr_cus['CustomerStatus'];
            $customer->Distance = $cr_cus['Distance'];
            $customer->Latitude = $cr_cus['Latitude'];
            $customer->Longitude = $cr_cus['Longitude'];
            $customer->Note = $cr_cus['Note'];
            $customer->Country = $cr_cus['Country'];
            $customer->Province = $cr_cus['Province'];
            $customer->District = $cr_cus['District'];
            $customer->SubDistrict = $cr_cus['SubDistrict'];
            $customer->DateCreate = $cr_cus['DateCreate'];
            $customer->UserCreate = $cr_cus['UserCreate'];
            $customer->DateLastUpdate = $cr_cus['DateLastUpdate'];
            $customer->UserLastUpdate = $cr_cus['UserLastUpdate'];

            $arr_cr_location = CRCustomerLocation::where('ApproveKey', '=', $approve['ApproveKey'])->get();
            foreach ($arr_cr_location as $arrLocation){
                $newChild = new CustomerLocation();
                $newChild->CustomerSkey = $arrLocation['CustomerSkey'];
                $newChild->LocationNameEng = $arrLocation["LocationNameEng"];
                $newChild->LocationNameThai = $arrLocation["LocationNameThai"];
                $newChild->LocationInitialsName = $arrLocation["LocationInitialsName"];
                $newChild->Address = $arrLocation["Address"];
                $newChild->LocationStatus = $arrLocation["LocationStatus"];
                $newChild->Distance = $arrLocation["Distance"];
                $newChild->Latitude = $arrLocation["Latitude"];
                $newChild->Longitude = $arrLocation["Longitude"];
                $newChild->Country = $arrLocation["Country"];
                $newChild->Province = $arrLocation["Province"];
                $newChild->District = $arrLocation["District"];
                $newChild->SubDistrict = $arrLocation["SubDistrict"];
                $newChild->DateCreate = $arrLocation["DateCreate"];
                $newChild->UserCreate = $arrLocation["UserCreate"];
                $newChild->DateLastUpdate = $arrLocation["DateLastUpdate"];
                $newChild->UserLastUpdate = $arrLocation["UserLastUpdate"];
                // delete when reject
                if(!$newChild->save()){
                    $dels = CustomerLocation::where('CustomerSkey', '=', $arrLocation['CustomerSkey'])->get();
                    foreach ($dels as $del){
                        $del->delete();
                    }
                    return response()->json([
                        "status" => "error",
                        "message" => "Create Reject"
                    ], 400);
                }
            }

            $arr_cr_contact = CRCustomerContact::where('ApproveKey', '=', $approve['ApproveKey'])->get();
            foreach ($arr_cr_contact as $cr_contact){
                $newContact = new CustomerContact();
                $newContact->CustomerSkey = $cr_contact['CustomerSkey'];
                $newContact->PrefixThai = $cr_contact["PrefixThai"];
                $newContact->NameThai = $cr_contact["NameThai"];
                $newContact->SurNameThai = $cr_contact["SurNameThai"];
                $newContact->PrefixEng = $cr_contact["PrefixEng"];
                $newContact->NameEng = $cr_contact["NameEng"];
                $newContact->SurNameEng = $cr_contact["SurNameEng"];
                $newContact->NickName = $cr_contact["NickName"];
                $newContact->Telephone = $cr_contact["Telephone"];
                $newContact->MobilePhone = $cr_contact["MobilePhone"];
                $newContact->Fax = $cr_contact["Fax"];
                $newContact->Email = $cr_contact["Email"];
                $newContact->BirthDate = $cr_contact["BirthDate"];
                $newContact->ContactStatus = $cr_contact["ContactStatus"];
                $newContact->DateCreate = $cr_contact['DateCreate'];
                $newContact->UserCreate = $cr_contact['UserCreate'];
                $newContact->DateLastUpdate = $cr_contact['DateLastUpdate'];
                $newContact->UserLastUpdate = $cr_contact['UserLastUpdate'];
                // delete when reject
                if(!$newContact->save()){
                    $dels = CustomerContact::where('CustomerSkey', '=', $cr_contact['CustomerSkey'])->get();
                    foreach ($dels as $del){
                        $del->delete();
                    }
                    return response()->json([
                        "status" => "error",
                        "message" => "Create Reject"
                    ], 400);
                }
            }



            if(!$customer->save()){

                return response()->json([
                    "status" => "error",
                    "message" => "Update Reject"
                ], 400);
            }else{
                $approve['CRStatus'] = 'Success';
                $approve['CRApproveUser'] = $check_header['id'];
                $approve['CRApproveDate'] = now();
                $approve->save();
            }
        }

        if($approve['CRType'] == 'Update Customer') {

            $customer = Customer::where('CustomerCode', '=', $cr_cus['CustomerCode'])->get();
            $customer = $customer[0];

//            $customer->CustomerCode = $cr_cus['CustomerCode'];
            $customer->CustomerNameEng = $cr_cus['CustomerNameEng'];
            $customer->CustomerNameThai = $cr_cus['CustomerNameThai'];
            $customer->CustomerInitialsName = $cr_cus['CustomerInitialsName'];
            $customer->Business = $cr_cus['Business'];
            $customer->Address = $cr_cus['Address'];
            $customer->CustomerStatus = $cr_cus['CustomerStatus'];
            $customer->Distance = $cr_cus['Distance'];
            $customer->Latitude = $cr_cus['Latitude'];
            $customer->Longitude = $cr_cus['Longitude'];
            $customer->Note = $cr_cus['Note'];
            $customer->Country = $cr_cus['Country'];
            $customer->Province = $cr_cus['Province'];
            $customer->District = $cr_cus['District'];
            $customer->SubDistrict = $cr_cus['SubDistrict'];
            $customer->DateCreate = $cr_cus['DateCreate'];
            $customer->UserCreate = $cr_cus['UserCreate'];
            $customer->DateLastUpdate = $cr_cus['DateLastUpdate'];
            $customer->UserLastUpdate = $cr_cus['UserLastUpdate'];

            $arr_cr_location = CRCustomerLocation::where('ApproveKey', '=', $approve['ApproveKey'])->get();
            $location = CustomerLocation::where('CustomerSkey', '=', $cr_cus['CustomerCode'])->get();

            if(count($location) == count($arr_cr_location)){
                $i = 0;
                foreach ($arr_cr_location as $arrLocation){
                    $location[$i]->CustomerSkey = $arrLocation['CustomerSkey'];
                    $location[$i]->LocationNameEng = $arrLocation["LocationNameEng"];
                    $location[$i]->LocationNameThai = $arrLocation["LocationNameThai"];
                    $location[$i]->LocationInitialsName = $arrLocation["LocationInitialsName"];
                    $location[$i]->Address = $arrLocation["Address"];
                    $location[$i]->LocationStatus = $arrLocation["LocationStatus"];
                    $location[$i]->Distance = $arrLocation["Distance"];
                    $location[$i]->Latitude = $arrLocation["Latitude"];
                    $location[$i]->Longitude = $arrLocation["Longitude"];
                    $location[$i]->Country = $arrLocation["Country"];
                    $location[$i]->Province = $arrLocation["Province"];
                    $location[$i]->District = $arrLocation["District"];
                    $location[$i]->SubDistrict = $arrLocation["SubDistrict"];
                    $location[$i]->DateLastUpdate = $arrLocation["DateLastUpdate"];
                    $location[$i]->UserLastUpdate = $arrLocation["UserLastUpdate"];
                    $location[$i]->save();
                    $i++;
                }
            }

            if(count($location) < count($arr_cr_location)){
                $countIndexChild = 0;
                $diffLen = count($arr_cr_location) - count($location);
                for ($i = 0; $i <= count($location) - $diffLen; $i++ ){
                    $location[$i]->CustomerSkey = $arr_cr_location[$i]['CustomerSkey'];
                    $location[$i]->LocationNameEng = $arr_cr_location[$i]["LocationNameEng"];
                    $location[$i]->LocationNameThai = $arr_cr_location[$i]["LocationNameThai"];
                    $location[$i]->LocationInitialsName = $arr_cr_location[$i]["LocationInitialsName"];
                    $location[$i]->Address = $arr_cr_location[$i]["Address"];
                    $location[$i]->LocationStatus = $arr_cr_location[$i]["LocationStatus"];
                    $location[$i]->Distance = $arr_cr_location[$i]["Distance"];
                    $location[$i]->Latitude = $arr_cr_location[$i]["Latitude"];
                    $location[$i]->Longitude = $arr_cr_location[$i]["Longitude"];
                    $location[$i]->Country = $arr_cr_location[$i]["Country"];
                    $location[$i]->Province = $arr_cr_location[$i]["Province"];
                    $location[$i]->District = $arr_cr_location[$i]["District"];
                    $location[$i]->SubDistrict = $arr_cr_location[$i]["SubDistrict"];
                    $location[$i]->DateLastUpdate = $arr_cr_location[$i]["DateLastUpdate"];
                    $location[$i]->UserLastUpdate = $arr_cr_location[$i]["UserLastUpdate"];
                    $location[$i]->save();
                    $countIndexChild++;
                }

                for ($i = $countIndexChild; $i < $countIndexChild + $diffLen; $i++ ){
                    $location = new CustomerLocation();
                    $location->CustomerSkey = $arr_cr_location[$i]['CustomerSkey'];
                    $location->LocationNameEng = $arr_cr_location[$i]["LocationNameEng"];
                    $location->LocationNameThai = $arr_cr_location[$i]["LocationNameThai"];
                    $location->LocationInitialsName = $arr_cr_location[$i]["LocationInitialsName"];
                    $location->Address = $arr_cr_location[$i]["Address"];
                    $location->LocationStatus = $arr_cr_location[$i]["LocationStatus"];
                    $location->Distance = $arr_cr_location[$i]["Distance"];
                    $location->Latitude = $arr_cr_location[$i]["Latitude"];
                    $location->Longitude = $arr_cr_location[$i]["Longitude"];
                    $location->Country = $arr_cr_location[$i]["Country"];
                    $location->Province = $arr_cr_location[$i]["Province"];
                    $location->District = $arr_cr_location[$i]["District"];
                    $location->SubDistrict = $arr_cr_location[$i]["SubDistrict"];
                    $location->DateLastUpdate = $arr_cr_location[$i]["DateLastUpdate"];
                    $location->UserLastUpdate = $arr_cr_location[$i]["UserLastUpdate"];
                    $location->save();
                }
            }


            $arr_cr_contact = CRCustomerContact::where('ApproveKey', '=', $approve['ApproveKey'])->get();
            $contact = CustomerContact::where('CustomerSkey', '=', $cr_cus['CustomerCode'])->get();
            if(count($contact) == count($arr_cr_contact)){
                $i = 0;
                foreach ($arr_cr_contact as $cr_contact){
                    $contact[$i]->CustomerSkey = $cr_contact['CustomerSkey'];
                    $contact[$i]->PrefixThai = $cr_contact["PrefixThai"];
                    $contact[$i]->NameThai = $cr_contact["NameThai"];
                    $contact[$i]->SurNameThai = $cr_contact["SurNameThai"];
                    $contact[$i]->PrefixEng = $cr_contact["PrefixEng"];
                    $contact[$i]->NameEng = $cr_contact["NameEng"];
                    $contact[$i]->SurNameEng = $cr_contact["SurNameEng"];
                    $contact[$i]->NickName = $cr_contact["NickName"];
                    $contact[$i]->Telephone = $cr_contact["Telephone"];
                    $contact[$i]->MobilePhone = $cr_contact["MobilePhone"];
                    $contact[$i]->Fax = $cr_contact["Fax"];
                    $contact[$i]->Email = $cr_contact["Email"];
                    $contact[$i]->BirthDate = $cr_contact["BirthDate"];
                    $contact[$i]->ContactStatus = $cr_contact["ContactStatus"];
                    $contact[$i]->DateCreate = $cr_contact['DateCreate'];
                    $contact[$i]->UserCreate = $cr_contact['UserCreate'];
                    $contact[$i]->DateLastUpdate = $cr_contact['DateLastUpdate'];
                    $contact[$i]->UserLastUpdate = $cr_contact['UserLastUpdate'];
                    $contact[$i]->save();
                    $i++;
                }
            }

            if(count($contact) < count($arr_cr_contact)){
                $countIndexChild = 0;
                $diffLen = count($arr_cr_contact) - count($contact);
                for ($i = 0; $i <= count($contact) - $diffLen; $i++ ){
                    $contact[$i]->CustomerSkey = $arr_cr_contact[$i]['CustomerSkey'];
                    $contact[$i]->PrefixThai = $arr_cr_contact[$i]["PrefixThai"];
                    $contact[$i]->NameThai = $arr_cr_contact[$i]["NameThai"];
                    $contact[$i]->SurNameThai = $arr_cr_contact[$i]["SurNameThai"];
                    $contact[$i]->PrefixEng = $arr_cr_contact[$i]["PrefixEng"];
                    $contact[$i]->NameEng = $arr_cr_contact[$i]["NameEng"];
                    $contact[$i]->SurNameEng = $arr_cr_contact[$i]["SurNameEng"];
                    $contact[$i]->NickName = $arr_cr_contact[$i]["NickName"];
                    $contact[$i]->Telephone = $arr_cr_contact[$i]["Telephone"];
                    $contact[$i]->MobilePhone = $arr_cr_contact[$i]["MobilePhone"];
                    $contact[$i]->Fax = $arr_cr_contact[$i]["Fax"];
                    $contact[$i]->Email = $arr_cr_contact[$i]["Email"];
                    $contact[$i]->BirthDate = $arr_cr_contact[$i]["BirthDate"];
                    $contact[$i]->ContactStatus = $arr_cr_contact[$i]["ContactStatus"];
                    $contact[$i]->DateCreate = $arr_cr_contact[$i]['DateCreate'];
                    $contact[$i]->UserCreate = $arr_cr_contact[$i]['UserCreate'];
                    $contact[$i]->DateLastUpdate = $arr_cr_contact[$i]['DateLastUpdate'];
                    $contact[$i]->UserLastUpdate = $arr_cr_contact[$i]['UserLastUpdate'];
                    $contact[$i]->save();

                    $countIndexChild++;
                }


                for ($i = $countIndexChild; $i < $countIndexChild + $diffLen; $i++ ){
                    $contact = new CustomerContact();
                    $contact->CustomerSkey = $arr_cr_contact[$i]['CustomerSkey'];
                    $contact->PrefixThai = $arr_cr_contact[$i]["PrefixThai"];
                    $contact->NameThai = $arr_cr_contact[$i]["NameThai"];
                    $contact->SurNameThai = $arr_cr_contact[$i]["SurNameThai"];
                    $contact->PrefixEng = $arr_cr_contact[$i]["PrefixEng"];
                    $contact->NameEng = $arr_cr_contact[$i]["NameEng"];
                    $contact->SurNameEng = $arr_cr_contact[$i]["SurNameEng"];
                    $contact->NickName = $arr_cr_contact[$i]["NickName"];
                    $contact->Telephone = $arr_cr_contact[$i]["Telephone"];
                    $contact->MobilePhone = $arr_cr_contact[$i]["MobilePhone"];
                    $contact->Fax = $arr_cr_contact[$i]["Fax"];
                    $contact->Email = $arr_cr_contact[$i]["Email"];
                    $contact->BirthDate = $arr_cr_contact[$i]["BirthDate"];
                    $contact->ContactStatus = $arr_cr_contact[$i]["ContactStatus"];
                    $contact->DateCreate = $arr_cr_contact[$i]['DateCreate'];
                    $contact->UserCreate = $arr_cr_contact[$i]['UserCreate'];
                    $contact->DateLastUpdate = $arr_cr_contact[$i]['DateLastUpdate'];
                    $contact->UserLastUpdate = $arr_cr_contact[$i]['UserLastUpdate'];

                    $contact->save();
                }
            }




            if(!$customer->save()){

                return response()->json([
                    "status" => "error",
                    "message" => "Update Reject"
                ], 400);
            }else{

                $approve['CRStatus'] = 'Success';
                $approve['CRApproveUser'] = $check_header['id'];
                $approve['CRApproveDate'] = now();

                $approve->save();

            }
        }

    }



    // reject approve
    public function RejectApprove(Request $request){
        $allow_header = GlobalController::CheckHeader($request);
        $check_header = json_decode($allow_header->content(), true);
        if ($check_header['status'] == 'error') return $allow_header;
        try {
            $confirm = $request->get("confirm");

            foreach ($confirm as $approve_key){
                $approve = Approve::where('ApproveKey', '=', $approve_key)->get();
                $approve = $approve[0];
                if($approve['CRStatus'] == 'Pending'){
                    // role
                    $approve['CRStatus'] = 'Reject';
                    $approve['CRRejectReason'] = $request->get("reason");
                    $approve['CRRejectDate'] = now();
                    $approve['CRApproveUser'] = $check_header['id'];
                    $approve['CRApproveDate'] = now();
                    $approve->save();
                }
            }

            return response()->json([
                "status" => "Reject",
                "message" => "Success"
            ], 200);

        }catch (\Exception $exception) {
            return $exception;
        }

    }

    // return approve
    public function ReturnRoleApprove(Request $request){
        $allow_header = GlobalController::CheckHeader($request);
        $check_header = json_decode($allow_header->content(), true);
        if ($check_header['status'] == 'error') return $allow_header;
        try {
            $approve = Approve::where('ApproveKey', '=', $request->get('approve_key'))->get();

            $approve = $approve[0];

            $approve->CRStatus = 'Pending';
            $approve->CRApproveUser = $check_header['id'];
            $approve->CRApproveDate = now();
            $approve->save();

            $cr_role = CrRole::where('ApproveKey', '=', $request->get('approve_key'))->get();
            $cr_role = $cr_role[0];
            $cr_role->RoleName = $request->get('role_name');
            $cr_role->RoleStatus = $request->get('role_status');
            $arrEdit = $request->get('role_edit');
            $cr_role->YellowName = $arrEdit[0]['role_name'];
            $cr_role->YellowStatus = $arrEdit[1]['role_status'];
            $cr_role->save();


            $arrScreen = $request->get('screen');

            for ($i = 0; $i < count($arrScreen); $i++){
                $role_auth = CRRoleAuthentications::where('ApproveKey', '=', $request->get('approve_key'))->where('screenSkey', '=', $arrScreen[$i]['screenSkey'])->get();
                $role_auth = $role_auth[0];
                $role_auth->ScreenSkey = $arrScreen[$i]['screenSkey'];
                $role_auth->PermissionType = $arrScreen[$i]['permission'];
                $role_auth->Yellow = $arrScreen[$i]['yellow'];
                $role_auth->UserUpdate = $check_header['id'];
                $role_auth->DateUpdate = now();
                $role_auth->save();
            }

            return response()->json([
                "status" => "Change to Pending",
                "message" => "Success"
            ], 200);

        }catch (\Exception $exception) {
            return $exception;
        }
    }

    // return employee role
    public function ReturnEmployeeRoleApprove(Request $request){
        $allow_header = GlobalController::CheckHeader($request);
        $check_header = json_decode($allow_header->content(), true);
        if ($check_header['status'] == 'error') return $allow_header;
        try {
            $approve = Approve::where('ApproveKey', '=', $request->get('approve_key'))->get();

            $approve = $approve[0];

            $approve->CRStatus = 'Pending';
            $approve->CRApproveUser = $check_header['id'];
            $approve->CRApproveDate = now();
            $approve->save();

            $cr_emp = CREmployee::where('ApproveKey', '=', $request->get('approve_key'))->get();
            $cr_emp->RoleCodeSkey = $request->get('role_code');
            $cr_emp->save();

            return response()->json([
                "status" => "Change to Pending",
                "message" => "Success"
            ], 200);

        }catch (\Exception $exception) {
            return $exception;
        }
    }

    // return employee
    public function ReturnEmployeeApprove(Request $request)
    {

        $allow_header = GlobalController::CheckHeader($request);
        $check_header = json_decode($allow_header->content(), true);
        if ($check_header['status'] == 'error') return $allow_header;

        try {

            $approve = Approve::where('ApproveKey', '=', $request->get('approve_key'))->get();

            $approve = $approve[0];


            $approve->CRStatus = 'Pending';
            $approve->CRApproveUser = $check_header['id'];
            $approve->CRApproveDate = now();
            $approve->save();

            $user = CREmployee::where('ApproveKey', '=', $request->get('approve_key'))->get();
            $user = $user[0];

//            $user->EmployeeNo = $emp->EmployeeNo;

            $user->email = $request->get('email');
            $user->Yellow = $request->get('yellow');
            $user->PrefixThai = $request->get('prefix_thai');
            $user->NameThai = $request->get('name_thai');
            $user->SurNameThai = $request->get('surname_thai');
            $user->PrefixEng = $request->get('prefix_eng');
            $user->NameEng = $request->get('name_eng');
            $user->SurNameEng = $request->get('surname_eng');
            $user->NickName = $request->get('nickname');
            $user->IDCard = $request->get('id_card');
            $user->MobilePhone = $request->get('mobile_phone');
            $user->BirthDate = $request->get('birth_date');
            $user->StartWorkingDate = $request->get('start_working_date');
            $user->ResignDate = $request->get('resign_date');
            $user->EmployeeStatus = $request->get('employee_status');
            $user->Nationality = $request->get('nationality');
            $user->Origin = $request->get('origin');
            $user->Religious = $request->get('religious');
            $user->MaritalStatus = $request->get('marital_status');
            $user->Note = $request->get('note');
            $user->PostCode = $request->get('post_code');
            $user->Address = $request->get('address');
            $user->FacebookID = $request->get('facebook_id');
            $user->LineID = $request->get('line_id');
            $user->DateLastUpdate = now();
            $user->UserLastUpdate = $check_header['id'];

            $user->EmployeeTypeID = $request->get('employee_type_id');
            $user->PositionID = $request->get('position_id');
            $user->SectionID = $request->get('section_id');
            $user->DepartmentID = $request->get('department_id');

            $user->SubDistrict = $request->get('sub_district');
            $user->District = $request->get('district');
            $user->Province = $request->get('province');
            $user->Country = $request->get('country');

            // children
            $arrChildren = $request->get('array_children');

            // change format
            $arrChildren = ($arrChildren == '[]') ? [] : json_decode($arrChildren, true);

            // create cr children
            foreach ($arrChildren as $data){
                $child = CREmployeeChildrens::where('ApproveKey', '=', $request->get('approve_key'))->get();
                $child = $child[0];
                $child->Status =  $data["status"];
                $child->LineNo = $user->EmployeeNo;
                $child->ChildrenName = $data["children_name"];
                $child->ChildrenLastName = $data["children_last_name"];
                $child->Sex = $data["sex"];
                $child->Birthdate = $data["birthdate"];
                $child->IdCard = $data["id_card"];
                $spitYellow = implode('","', $data["yellow"]);
                $child->Yellow = '["'.$spitYellow.'"]';
                if($child->Yellow  == "[\"\"]"){
                    $child->Yellow  = null;
                }
                $child->save();
            }

            // attached
            $attaches = $request->get('array_attached');

            // change format
            $attaches = ($attaches == '[]') ? [] : json_decode($attaches, true);

            // check file
            if($request->hasFile('File')){
                $files = $request->file('File');
                $numOfFile = count($files);
            }else{
                $numOfFile = 0;
            }

            $numOfAttached = count($attaches);
            $numDiff = $numOfAttached - $numOfFile;

            if($numDiff != 0){
                //insert file and update data
                if($numOfFile != 0){
                    for ($i = 0; $i < $numDiff; $i++){
                        $file = CREmployeeAttachedFiles::where('ApproveKey', '=', $request->get('approve_key'))->get();
                        $file = $file[0];
                        $file->Status =  $attaches[$i]["status"];
                        $file->FileName = $attaches[$i]['file_name'];
                        $file->EmpAttachedListNo = $user->EmployeeNo;
                        $path = explode('approve/', $attaches[$i]['attached_path']);
                        $file->AttachedPath = $path[1];
                        $file->DateCreate = $attaches[$i]['date_create'];
                        $file->UserCreate = $attaches[$i]['user_create'];

                        $spitYellow = implode('","', $attaches[$i]["yellow"]);
                        $file->Yellow = '["'.$spitYellow.'"]';
                        if($file->Yellow  == "[\"\"]"){
                            $file->Yellow  = null;
                        }
                        $file->save();
                    }
                    foreach ($files as $file){
                        $newFile = CREmployeeAttachedFiles::where('ApproveKey', '=', $request->get('approve_key'))->get();
                        $newFile = $newFile[0];
                        $newFile->Status = $attaches[$numDiff]['status'];
                        $newFile->EmpAttachedListNo = $user->EmployeeNo;
                        $newFile->FileName = $attaches[$numDiff]['file_name'];
                        $newFile->DateCreate = now();
                        $newFile->UserCreate = $check_header['id'];
                        $spitYellow = implode('","', $attaches[$numDiff]["yellow"]);
                        $newFile->Yellow = '["'.$spitYellow.'"]';
                        if($newFile->Yellow  == "[\"\"]"){
                            $newFile->Yellow  = null;
                        }
                        $path = '';

                        if('IDCard' == $attaches[$numDiff]['file_name']){
                            $path = 'id_card';
                        }
                        if('EmployeePicture' == $attaches[$numDiff]['file_name']){
                            $path = 'employee_picture';
                        }
                        if('DrivingLicense' == $attaches[$numDiff]['file_name']){
                            $path = 'driving_licence';
                        }
                        if('Other' == $attaches[$numDiff]['file_name']){
                            $path = 'other';
                        }

                        $path = $path.'/'.$path.'-cut-'.$user->EmployeeNo.'-cut-'.base64_encode(random_bytes(10)).'.jpg';

                        \File::put(public_path(). '/images/waiting_for_approve/'.$path, file_get_contents($file));
                        $newFile->AttachedPath = $path;
                        $newFile->save();
                        $numDiff++;
                    }
                }else{
                    // update only data
                    for ($i = 0; $i < $numOfAttached; $i++){
                        $file = CREmployeeAttachedFiles::where('ApproveKey', '=', $request->get('approve_key'))->get();
                        $file = $file[0];
                        $file->Status =  $attaches[$i]["status"];
                        $file->FileName = $attaches[$i]['file_name'];
                        $file->EmpAttachedListNo = $user->EmployeeNo;
                        $path = explode('approve/', $attaches[$i]['attached_path']);
                        $file->AttachedPath = $path[1];
                        $file->DateCreate = $attaches[$i]['date_create'];
                        $file->UserCreate = $attaches[$i]['user_create'];
                        $spitYellow = implode('","', $attaches[$i]["yellow"]);
                        $file->Yellow = '["'.$spitYellow.'"]';
                        if($file->Yellow  == "[\"\"]"){
                            $file->Yellow  = null;
                        }
                        $file->save();
                    }
                }
            }else{
                // insert file only
                if($numOfFile != 0){
                    foreach ($files as $file){
                        $newFile = CREmployeeAttachedFiles::where('ApproveKey', '=', $request->get('approve_key'))->get();
                        $newFile = $newFile[0];
                        $newFile->Status = $attaches[$numDiff]['status'];
                        $newFile->EmpAttachedListNo = $user->EmployeeNo;
                        $newFile->FileName = $attaches[$numDiff]['file_name'];
                        $newFile->DateCreate = now();
                        $newFile->UserCreate = $check_header['id'];
                        $spitYellow = implode('","', $attaches[$numDiff]["yellow"]);
                        $newFile->Yellow = '["'.$spitYellow.'"]';
                        if($newFile->Yellow  == "[\"\"]"){
                            $newFile->Yellow  = null;
                        }
                        $path = '';

                        if('IDCard' == $attaches[$numDiff]['file_name']){
                            $path = 'id_card';
                        }
                        if('EmployeePicture' == $attaches[$numDiff]['file_name']){
                            $path = 'employee_picture';
                        }
                        if('DrivingLicense' == $attaches[$numDiff]['file_name']){
                            $path = 'driving_licence';
                        }
                        if('Other' == $attaches[$numDiff]['file_name']){
                            $path = 'other';
                        }

                        $path = $path.'/'.$path.'-cut-'.$user->EmployeeNo.'-cut-'.base64_encode(random_bytes(10)).'.jpg';

                        \File::put(public_path(). '/images/waiting_for_approve/'.$path, file_get_contents($file));
                        $newFile->AttachedPath = $path;
                        $newFile->save();
                        $numDiff++;
                    }
                }else{
                    // update only data
                    for ($i = 0; $i < $numOfAttached; $i++){
                        $file = CREmployeeAttachedFiles::where('ApproveKey', '=', $request->get('approve_key'))->get();
                        $file = $file[0];
                        $file->Status =  $attaches[$i]["status"];
                        $file->FileName = $attaches[$i]['file_name'];
                        $file->EmpAttachedListNo = $user->EmployeeNo;
                        $path = explode('approve/', $attaches[$i]['attached_path']);
                        $file->AttachedPath = $path[1];
                        $file->DateCreate = $attaches[$i]['date_create'];
                        $file->UserCreate = $attaches[$i]['user_create'];
                        $spitYellow = implode('","', $attaches[$i]["yellow"]);
                        $file->Yellow = '["'.$spitYellow.'"]';
                        if($file->Yellow  == "[\"\"]"){
                            $file->Yellow  = null;
                        }
                        $file->save();
                    }
                }
            }

            if ($user->save()) {
                return response()->json([
                    "status" => "success",
                    "message" => "user updated"
                ], 200);
            } else {
                return response()->json([
                    "status" => "error",
                    "message" => "can not updated"
                ], 400);
            }

        } catch (\Exception $e)
        {
            return response()->json([
                "status" => "error",
                "message" => $e
            ], 400);
        }
    }

    // return customer
    public function ReturnCustomerApprove(Request $request){
        $allow_header = GlobalController::CheckHeader($request);
        $check_header = json_decode($allow_header->content(), true);
        if ($check_header['status'] == 'error') return $allow_header;
        try {
            $approve = Approve::where('ApproveKey', '=', $request->get('approve_key'))->get();
            $approve = $approve[0];
            $approve->CRStatus = 'Pending';
            $approve->CRApproveUser = $check_header['id'];
            $approve->CRApproveDate = now();
            $approve->save();

            // update cr customer
            $customer = CRCustomer::where('ApproveKey', '=', $request->get('approve_key'))->get();
            $customer = $customer[0];

            $customer->CustomerCode = $request->get('customer_code');
            $customer->CustomerNameEng = $request->get('customer_name_eng');
            $customer->CustomerNameThai = $request->get('customer_name_thai');
            $customer->CustomerInitialsName = $request->get('customer_initials_name');
            $customer->Business = $request->get('business');
            $customer->Address = $request->get('address');
            $customer->CustomerStatus = $request->get('customer_status');
            $customer->Distance = $request->get('distance');
            $customer->Latitude = $request->get('latitude');
            $customer->Longitude = $request->get('longitude');
            $customer->Note = $request->get('note');
            $customer->Country = $request->get("country");
            $customer->Province = $request->get("province");
            $customer->District = $request->get("district");
            $customer->SubDistrict = $request->get("sub_district");
            $customer->DateCreate = $request->get("date_create");
            $customer->UserCreate = $request->get("user_create");
            $customer->DateLastUpdate = now();
            $customer->UserLastUpdate = $check_header['id'];
            $spitYellow = implode('","', $request->get("yellow"));

            $customer->Yellow = '["'.$spitYellow.'"]';
            if($customer->Yellow  == "[\"\"]"){
                $customer->Yellow  = null;
            }

            $locations = $request->get("array_location");
//            $locations = ($locations == '[]') ? [] : json_decode($locations, true);

            foreach ($locations as $arrLocation){
                $newChild = CRCustomerLocation::where('ApproveKey', '=', $request->get('approve_key'))->get();
                $newChild = $newChild[0];

                $newChild->LocationNameEng = $arrLocation["location_name_eng"];
                $newChild->LocationNameThai = $arrLocation["location_name_thai"];
                $newChild->LocationInitialsName = $arrLocation["location_initials_name"];
                $newChild->Address = $arrLocation["address"];
                $newChild->LocationStatus = $arrLocation["location_status"];
                $newChild->Distance = $arrLocation["distance"];
                $newChild->Latitude = $arrLocation["latitude"];
                $newChild->Longitude = $arrLocation["longitude"];
                $newChild->Country = $arrLocation["country"];
                $newChild->Province = $arrLocation["province"];
                $newChild->District = $arrLocation["district"];
                $newChild->SubDistrict = $arrLocation["sub_district"];
                $newChild->DateLastUpdate = now();
                $newChild->UserLastUpdate = $check_header['id'];

                $spitYellow = implode('","', $arrLocation["yellow"]);
                $newChild->Yellow  = '["'.$spitYellow.'"]';
                if($newChild->Yellow  == "[\"\"]"){
                    $newChild->Yellow   = null;
                }

                $newChild->save();
            }

            $arrContact = $request->get('array_contact');
            if(count($arrContact) != 0){
                for ($i = 0; $i < count($arrContact); $i++){
                    $newContact = CRCustomerContact::where('ApproveKey', '=', $request->get('approve_key'))->get();
                    $newContact = $newContact[0];

                    $newContact->PrefixThai = $arrContact[$i]["prefix_thai"];
                    $newContact->NameThai = $arrContact[$i]["name_thai"];
                    $newContact->SurNameThai = $arrContact[$i]["surname_thai"];
                    $newContact->PrefixEng = $arrContact[$i]["prefix_eng"];
                    $newContact->NameEng = $arrContact[$i]["name_eng"];
                    $newContact->SurNameEng = $arrContact[$i]["surname_eng"];
                    $newContact->NickName = $arrContact[$i]["nickname"];
                    $newContact->Telephone = $arrContact[$i]["telephone"];
                    $newContact->MobilePhone = $arrContact[$i]["mobile_phone"];
                    $newContact->Fax = $arrContact[$i]["fax"];
                    $newContact->Email = $arrContact[$i]["email"];
                    $newContact->BirthDate = $arrContact[$i]["birthdate"];
                    $newContact->ContactStatus = $arrContact[$i]["contact_status"];

                    $spitYellow = implode('","', $arrContact[$i]["yellow"]);
                    $newContact->Yellow  = '["'.$spitYellow.'"]';
                    if( $newContact->Yellow  == "[\"\"]"){
                        $newContact->Yellow   = null;
                    }


                    $newContact->DateCreate = now();
                    $newContact->UserCreate = $check_header['id'];
                    $newContact->DateLastUpdate = now();
                    $newContact->UserLastUpdate = $check_header['id'];
                    $newContact->save();
                }
            }

            $customer->save();


            return response()->json([
                "status" => "Change to Pending",
                "message" => "Success"
            ], 200);

        }catch (\Exception $exception) {
            return $exception;
        }
    }




    // sort approve
    public function SortApprove(Request $request){
        $allow_header = GlobalController::CheckHeader($request);
        $check_header = json_decode($allow_header->content(), true);
        if ($check_header['status'] == 'error')
        {
            return $allow_header;
        }

        try {
            $sorts = $request->get("array_sort");
            $approve = DB::table('approves')->where('CRStatus', '=', 'Pending');
            foreach ($sorts as $sort){
                if($sort['order'] == "maxtomin"){
                    $approve = $approve->orderBy($sort['coloum_name'], 'desc');
                }else{
                    $approve = $approve->orderBy($sort['coloum_name'], 'asc');

                }
            }
            $approves = $approve->get();
            foreach ($approves as $approve){
                if ($approve->Master == 'role'){
                    $approve->CRRole = CrRole::where('ApproveKey', '=', $approve->ApproveKey)->get();
                    foreach ($approve->CRRole as $cr_auth){
                        $cr_auth->CRAuth = CRRoleAuthentications::where('ApproveKey', '=', $approve->ApproveKey)->get();
                    }
                }
            }

            return $approves;
        }catch (\Exception $exception){
            return $exception;
        }
    }
    public function StateApprove(Request $request){
        $allow_header = GlobalController::CheckHeader($request);
        $check_header = json_decode($allow_header->content(), true);
        if ($check_header['status'] == 'error') return $allow_header;

        try {
            $emp = Employee::where('id', '=', $check_header['id'])->first();
            $emp->StateApprove = $request->get('data');
            if ($emp->save()) {
                return response()->json([
                    "status" => "success",
                    "message" => "update"
                ], 200);
            } else {
                return response()->json([
                    "status" => "error",
                    "message" => "Bad Request"
                ], 400);
            }
        } catch (\Exception $exception){
            return $this->ErrorDataBase();
        }
    }
    public function GetStateApprove(Request $request){
        $allow_header = GlobalController::CheckHeader($request);
        $check_header = json_decode($allow_header->content(), true);
        if ($check_header['status'] == 'error')
        {
            return $allow_header;
        }

        try {
            $emp = Employee::where('id', '=', $check_header['id'])->first();
            return response()->json([
                "status" => "success",
                "message" => $emp->StateApprove
            ], 200);

        } catch (\Exception $exception){
            return $this->ErrorDataBase();
        }
    }

//    sort approve of user
    public function SortApproveOfUser(Request $request){
        $allow_header = GlobalController::CheckHeader($request);
        $check_header = json_decode($allow_header->content(), true);
        if ($check_header['status'] == 'error')
        {
            return $allow_header;
        }

        try {
            $sorts = $request->get("array_sort");
//            $approve = DB::table('approves')->where('CRStatus', '=', 'Pending');
            $approve = DB::table('approves')->where('ApproveUserCreate', '=', $check_header['id']);

            foreach ($sorts as $sort){
                if($sort['order'] == "maxtomin"){
                    $approve = $approve->orderBy($sort['coloum_name'], 'desc');
                }else{
                    $approve = $approve->orderBy($sort['coloum_name'], 'asc');

                }
            }
            $approves = $approve->get();
            foreach ($approves as $approve){
                if ($approve->Master == 'role'){
                    $approve->CRRole = CrRole::where('ApproveKey', '=', $approve->ApproveKey)->get();
                    foreach ($approve->CRRole as $cr_auth){
                        $cr_auth->CRAuth = CRRoleAuthentications::where('ApproveKey', '=', $approve->ApproveKey)->get();
                    }
                }
            }

            return $approves;
        }catch (\Exception $exception){
            return $exception;
        }
    }
    public function StateApproveOfUser(Request $request){
        $allow_header = GlobalController::CheckHeader($request);
        $check_header = json_decode($allow_header->content(), true);
        if ($check_header['status'] == 'error') return $allow_header;

        try {
            $emp = Employee::where('id', '=', $check_header['id'])->first();
            $emp->StateApproveOfUser = $request->get('data');
            if ($emp->save()) {
                return response()->json([
                    "status" => "success",
                    "message" => "update"
                ], 200);
            } else {
                return response()->json([
                    "status" => "error",
                    "message" => "Bad Request"
                ], 400);
            }
        } catch (\Exception $exception){
            return $this->ErrorDataBase();
        }
    }
    public function GetStateApproveOfUser(Request $request){
        $allow_header = GlobalController::CheckHeader($request);
        $check_header = json_decode($allow_header->content(), true);
        if ($check_header['status'] == 'error')
        {
            return $allow_header;
        }

        try {
            $emp = Employee::where('id', '=', $check_header['id'])->first();
            return response()->json([
                "status" => "success",
                "message" => $emp->StateApproveOfUser
            ], 200);

        } catch (\Exception $exception){
            return $this->ErrorDataBase();
        }
    }


}
