<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\API\GlobalController;
use App\Http\Controllers\API\SendMailController;
use App\Http\Controllers\Controller;
use App\Providers\Model\CREmployee;
use App\Providers\Model\CREmployeeAttachedFiles;
use App\Providers\Model\CREmployeeChildrens;
use App\Providers\Model\CrRole;
use App\Providers\Model\Department;
use App\Providers\Model\Employee;
use App\Providers\Model\EmployeeAttachedFile;
use App\Providers\Model\EmployeeChildren;
use App\Providers\Model\EmployeeType;
use App\Providers\Model\MaterialType;
use App\Providers\Model\Position;
use App\Providers\Model\Province;
use App\Providers\Model\Section;
use App\Providers\Model\District;
use App\Providers\Model\SubDistrict;
use App\Providers\Model\Country;



use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class EmployeeController extends Controller
{

    //create super user
    public function CreateSuperEmployee(Request $request){
        try {
            $token = Str::random(60);
            $user = new Employee();
            $user->email = $request->get('email');
            $haveUser = Employee::where('email', '=', $user->email)->first();
            if($haveUser){
                return response()->json([
                    "status" => "error",
                    "message" => "Duplicate Email"
                ], 400);
            }

            $user->password = Hash::make($request->get('password'));
            $user->old_password = Hash::make($request->get('password'));
            $user->remember_token = hash('sha256',$token);

            if ($user->save()) {
                return response()->json([
                    "status" => "success",
                    "message" => "user created"
                ], 201);
            } else {
                return response()->json([
                    "status" => "error",
                    "message" => "can not create user"
                ], 400);
            }
        } catch (\Exception $e)
        {
            return response()->json([
                "status" => "error",
                "message" => "can not create user"
            ], 400);
        }
    }

    // create user and send email
    public function CreateEmployee(Request $request)
    {
        $allow_header = GlobalController::CheckHeader($request);
        $check_header = json_decode($allow_header->content(), true);
        if ($check_header['status'] == 'error') {
            return $allow_header;
        }
        try {

            $token = Str::random(60);
            $user = new CREmployee();
            $user->email = $request->get('email');
            $haveUser = Employee::where('email', '=', $user->email)->first();
            if($haveUser){
                return response()->json([
                    "status" => "error",
                    "message" => "Duplicate Email"
                ], 400);
            }
            // tab 1
            $passBeforeHash = Str::random(10);
            $pass = Hash::make($passBeforeHash);
            $user->password = $pass;
            $user->old_password = $pass;
            $user->remember_token = hash('sha256',$token);


            $last_emp = DB::table('c_r_employees')->orderBy('EmployeeNo', 'desc')->select('EmployeeNo')->get();
            $lastApprove = GlobalController::CreateApprove($check_header['id'], 'Create Employee', 'employee', $check_header);


            if(count($last_emp) == 0){
                $lastEmp = 'F0001';
            }else{
                $lastEmp = $last_emp[0]->EmployeeNo;
                $lastEmp++;
            }

            $user->ApproveKey = $lastApprove;
            $user->EmployeeNo = $lastEmp;

            $user->PrefixThai = $request->get('prefix_thai');
            $user->NameThai = $request->get('name_thai');
            $user->SurNameThai = $request->get('surname_thai');
            $user->PrefixEng = $request->get('prefix_eng');
            $user->NameEng = $request->get('name_eng');
            $user->SurNameEng = $request->get('surname_eng');
            $user->NickName = $request->get('nickname');
            $user->IDCard = $request->get('id_card');
            $user->MobilePhone = $request->get('mobile_phone');
            $user->BirthDate = $request->get('birth_date');
            $user->StartWorkingDate = $request->get('start_working_date');
            $user->ResignDate = $request->get('resign_date');
            $user->EmployeeStatus = $request->get('employee_status');
            $user->Nationality = $request->get('nationality');
            $user->Origin = $request->get('origin');
            $user->Religious = $request->get('religious');
            $user->MaritalStatus = $request->get('marital_status');
            $user->Note = $request->get('note');
            $user->PostCode = $request->get('post_code');
            $user->Address = $request->get('address');
            $user->FacebookID = $request->get('facebook_id');
            $user->LineID = $request->get('line_id');
            $user->DateCreate = now();
            $user->UserCreate = $check_header['id'];


//          id 1, 2, 3
            $user->EmployeeTypeID = $request->get('employee_type_id');
            $user->PositionID = $request->get('position_id');
            $user->SectionID = $request->get('section_id');
            $user->DepartmentID = $request->get('department_id');
            $user->SubDistrict= $request->get('sub_district');
            $user->District = $request->get('district');
            $user->Province = $request->get('province');
            $user->Country = $request->get('country');


            if ($user->save()) {
                // insert children employees
                $arrChildren = $request->get('array_children');
                $arrChildren = json_decode($arrChildren, true);
                if(count($arrChildren) != 0){
                    for ($i = 0; $i < count($arrChildren); $i++){
                        $newChild[$i] = new CREmployeeChildrens();
                        $newChild[$i]->ApproveKey = $lastApprove;
                        $newChild[$i]->Status = 'Active';
                        $newChild[$i]->LineNo = $lastEmp;
                        $newChild[$i]->ChildrenName = $arrChildren[$i]["children_name"];
                        $newChild[$i]->ChildrenLastName = $arrChildren[$i]["children_last_name"];
                        $newChild[$i]->Sex = $arrChildren[$i]["sex"];
                        $newChild[$i]->Birthdate = $arrChildren[$i]["birthdate"];
                        $newChild[$i]->IdCard = $arrChildren[$i]["id_card"];
                        $newChild[$i]->save();
                    }
                }

                // insert attached file
                $attaches = $request->get('array_attached');
                $attaches = json_decode($attaches, true);
                $i = 0;

                if (count($attaches) != 0){

                    $files = $request->file('File');
                    foreach ($files as $file){
                        $newFile[$i] = new CREmployeeAttachedFiles();
                        $newFile[$i]->ApproveKey = $lastApprove;
                        $newFile[$i]->Status = 'Active';
                        $newFile[$i]->EmpAttachedListNo = $lastEmp;
                        $newFile[$i]->FileName = $attaches[$i]['file_name'];
                        $newFile[$i]->DateCreate = now();
                        $newFile[$i]->UserCreate = $check_header['id'];
                        $path = '';

                        if('IDCard' == $attaches[$i]['file_name']){
                            $path = 'id_card';
                        }
                        if('EmployeePicture' == $attaches[$i]['file_name']){
                            $path = 'employee_picture';
                        }
                        if('DrivingLicense' == $attaches[$i]['file_name']){
                            $path = 'driving_license';
                        }
                        if('Other' == $attaches[$i]['file_name']){
                            $path = 'other';
                        }

                        $path = $path.'/'.$path.'-cut-'.$lastEmp.'-cut-'.base64_encode(random_bytes(10)).'.jpg';
                        \File::put(public_path(). '/images/waiting_for_approve/'.$path, file_get_contents($file));

                        $newFile[$i]->AttachedPath = $path;
                        $newFile[$i]->save();
                        $i++;
                    }
                }

                // send mail to employee for change password
                //SendMailController::SendEmailToEmployeeForChangePassword($request->get('email'), $request->get('name_eng'), $request->get('surname_eng'), $passBeforeHash);

                return response()->json([
                    "status" => "success",
                    "message" => "user created"
                ], 201);
            } else {
                $dels = CREmployee::where('ApproveKey', '=', $lastApprove)->get();
                foreach ($dels as $del){
                    $del->delete();
                }
                return response()->json([
                    "status" => "error",
                    "message" => "can not create user"
                ], 400);
            }
        } catch (\Exception $e)
        {
            return response()->json([
                "status" => "error",
                "message" => $e
            ], 400);
        }
    }

    // update user
    public function UpdateEmployee(Request $request)
    {

        $allow_header = GlobalController::CheckHeader($request);
        $check_header = json_decode($allow_header->content(), true);
        if ($check_header['status'] == 'error') return $allow_header;

        try {

            $lastApprove = GlobalController::CreateApprove($check_header['id'], 'Update Employee', 'employee', $check_header);
            $user = new CREmployee();

            $emp = Employee::where('id', '=', $request->get('id'))->first();

            $user->ApproveKey = $lastApprove;
            $user->EmployeeNo = $emp->EmployeeNo;
            $user->email = $request->get('email');
            $user->Yellow = $request->get('yellow');
            $user->PrefixThai = $request->get('prefix_thai');
            $user->NameThai = $request->get('name_thai');
            $user->SurNameThai = $request->get('surname_thai');
            $user->PrefixEng = $request->get('prefix_eng');
            $user->NameEng = $request->get('name_eng');
            $user->SurNameEng = $request->get('surname_eng');
            $user->NickName = $request->get('nickname');
            $user->IDCard = $request->get('id_card');
            $user->MobilePhone = $request->get('mobile_phone');
            $user->BirthDate = $request->get('birth_date');
            $user->StartWorkingDate = $request->get('start_working_date');
            $user->ResignDate = $request->get('resign_date');
            $user->EmployeeStatus = $request->get('employee_status');
            $user->Nationality = $request->get('nationality');
            $user->Origin = $request->get('origin');
            $user->Religious = $request->get('religious');
            $user->MaritalStatus = $request->get('marital_status');
            $user->Note = $request->get('note');
            $user->PostCode = $request->get('post_code');
            $user->Address = $request->get('address');
            $user->FacebookID = $request->get('facebook_id');
            $user->LineID = $request->get('line_id');
            $user->DateLastUpdate = now();
            $user->UserLastUpdate = $check_header['id'];

            $user->EmployeeTypeID = $request->get('employee_type_id');
            $user->PositionID = $request->get('position_id');
            $user->SectionID = $request->get('section_id');
            $user->DepartmentID = $request->get('department_id');

            $user->SubDistrict = $request->get('sub_district');
            $user->District = $request->get('district');
            $user->Province = $request->get('province');
            $user->Country = $request->get('country');

            // children
            $arrChildren = $request->get('array_children');

            // change format
            $arrChildren = ($arrChildren == '[]') ? [] : json_decode($arrChildren, true);

            // create cr children
            foreach ($arrChildren as $data){
                $child = new CREmployeeChildrens();
                $child->ApproveKey = $lastApprove;
                $child->Status =  $data["status"];
                $child->LineNo = $emp->EmployeeNo;
                $child->ChildrenName = $data["children_name"];
                $child->ChildrenLastName = $data["children_last_name"];
                $child->Sex = $data["sex"];
                $child->Birthdate = $data["birthdate"];
                $child->IdCard = $data["id_card"];
                $spitYellow = implode('","', $data["yellow"]);
                $child->Yellow = '["'.$spitYellow.'"]';
                if($child->Yellow  == "[\"\"]"){
                    $child->Yellow  = null;
                }
                $child->save();
            }

            // attached
            $attaches = $request->get('array_attached');

            // change format
            $attaches = ($attaches == '[]') ? [] : json_decode($attaches, true);

            // check file
            if($request->hasFile('File')){
                $files = $request->file('File');
                $numOfFile = count($files);
            }else{
                $numOfFile = 0;
            }

            $numOfAttached = count($attaches);
            $numDiff = $numOfAttached - $numOfFile;

            if($numDiff != 0){
                //insert file and update data
                if($numOfFile != 0){
                    for ($i = 0; $i < $numDiff; $i++){
                        $file = new CREmployeeAttachedFiles();
                        $file->ApproveKey = $lastApprove;
                        $file->Status =  $attaches[$i]["status"];
                        $file->FileName = $attaches[$i]['file_name'];
                        $file->EmpAttachedListNo = $emp->EmployeeNo;
                        $path = explode('approve/', $attaches[$i]['attached_path']);
                        $file->AttachedPath = $path[1];
                        $file->DateCreate = $attaches[$i]['date_create'];
                        $file->UserCreate = $attaches[$i]['user_create'];

                        $spitYellow = implode('","', $attaches[$i]["yellow"]);
                        $file->Yellow = '["'.$spitYellow.'"]';
                        if($file->Yellow  == "[\"\"]"){
                            $file->Yellow  = null;
                        }
                        $file->save();
                    }
                    foreach ($files as $file){
                        $newFile = new CREmployeeAttachedFiles();
                        $newFile->ApproveKey = $lastApprove;
                        $newFile->Status = $attaches[$numDiff]['status'];
                        $newFile->EmpAttachedListNo = $emp->EmployeeNo;
                        $newFile->FileName = $attaches[$numDiff]['file_name'];
                        $newFile->DateCreate = now();
                        $newFile->UserCreate = $check_header['id'];
                        $spitYellow = implode('","', $attaches[$numDiff]["yellow"]);
                        $newFile->Yellow = '["'.$spitYellow.'"]';
                        if($newFile->Yellow  == "[\"\"]"){
                            $newFile->Yellow  = null;
                        }
                        $path = '';

                        if('IDCard' == $attaches[$numDiff]['file_name']){
                            $path = 'id_card';
                        }
                        if('EmployeePicture' == $attaches[$numDiff]['file_name']){
                            $path = 'employee_picture';
                        }
                        if('DrivingLicense' == $attaches[$numDiff]['file_name']){
                            $path = 'driving_licence';
                        }
                        if('Other' == $attaches[$numDiff]['file_name']){
                            $path = 'other';
                        }

                        $path = $path.'/'.$path.'-cut-'.$emp->EmployeeNo.'-cut-'.base64_encode(random_bytes(10)).'.jpg';

                        \File::put(public_path(). '/images/waiting_for_approve/'.$path, file_get_contents($file));
                        $newFile->AttachedPath = $path;
                        $newFile->save();
                        $numDiff++;
                    }
                }else{
                    // update only data
                    for ($i = 0; $i < $numOfAttached; $i++){
                        $file = new CREmployeeAttachedFiles();
                        $file->ApproveKey = $lastApprove;
                        $file->Status =  $attaches[$i]["status"];
                        $file->FileName = $attaches[$i]['file_name'];
                        $file->EmpAttachedListNo = $emp->EmployeeNo;
                        $path = explode('approve/', $attaches[$i]['attached_path']);
                        $file->AttachedPath = $path[1];
                        $file->DateCreate = $attaches[$i]['date_create'];
                        $file->UserCreate = $attaches[$i]['user_create'];
                        $spitYellow = implode('","', $attaches[$i]["yellow"]);
                        $file->Yellow = '["'.$spitYellow.'"]';
                        if($file->Yellow  == "[\"\"]"){
                            $file->Yellow  = null;
                        }
                        $file->save();
                    }
                }
            }else{
                // insert file only
                if($numOfFile != 0){
                    foreach ($files as $file){
                        $newFile = new CREmployeeAttachedFiles();
                        $newFile->ApproveKey = $lastApprove;
                        $newFile->Status = $attaches[$numDiff]['status'];
                        $newFile->EmpAttachedListNo = $emp->EmployeeNo;
                        $newFile->FileName = $attaches[$numDiff]['file_name'];
                        $newFile->DateCreate = now();
                        $newFile->UserCreate = $check_header['id'];
                        $spitYellow = implode('","', $attaches[$numDiff]["yellow"]);
                        $newFile->Yellow = '["'.$spitYellow.'"]';
                        if($newFile->Yellow  == "[\"\"]"){
                            $newFile->Yellow  = null;
                        }
                        $path = '';

                        if('IDCard' == $attaches[$numDiff]['file_name']){
                            $path = 'id_card';
                        }
                        if('EmployeePicture' == $attaches[$numDiff]['file_name']){
                            $path = 'employee_picture';
                        }
                        if('DrivingLicense' == $attaches[$numDiff]['file_name']){
                            $path = 'driving_licence';
                        }
                        if('Other' == $attaches[$numDiff]['file_name']){
                            $path = 'other';
                        }

                        $path = $path.'/'.$path.'-cut-'.$emp->EmployeeNo.'-cut-'.base64_encode(random_bytes(10)).'.jpg';

                        \File::put(public_path(). '/images/waiting_for_approve/'.$path, file_get_contents($file));
                        $newFile->AttachedPath = $path;
                        $newFile->save();
                        $numDiff++;
                    }
                }else{
                    // update only data
                    for ($i = 0; $i < $numOfAttached; $i++){
                        $file = new CREmployeeAttachedFiles();
                        $file->ApproveKey = $lastApprove;
                        $file->Status =  $attaches[$i]["status"];
                        $file->FileName = $attaches[$i]['file_name'];
                        $file->EmpAttachedListNo = $emp->EmployeeNo;
                        $path = explode('approve/', $attaches[$i]['attached_path']);
                        $file->AttachedPath = $path[1];
                        $file->DateCreate = $attaches[$i]['date_create'];
                        $file->UserCreate = $attaches[$i]['user_create'];
                        $spitYellow = implode('","', $attaches[$i]["yellow"]);
                        $file->Yellow = '["'.$spitYellow.'"]';
                        if($file->Yellow  == "[\"\"]"){
                            $file->Yellow  = null;
                        }
                        $file->save();
                    }
                }
            }

            if ($user->save()) {
                return response()->json([
                    "status" => "success",
                    "message" => "user updated"
                ], 200);
            } else {
                return response()->json([
                    "status" => "error",
                    "message" => "can not updated"
                ], 400);
            }

        } catch (\Exception $e)
        {
            return response()->json([
                "status" => "error",
                "message" => $e
            ], 400);
        }
    }

    // get user
    public function GetEmployee(Request $request)
    {
        $allow_header = GlobalController::CheckHeader($request);
        $check_header = json_decode($allow_header->content(), true);
        if ($check_header['status'] == 'error') return $allow_header;
        try {
            $emps = Employee::all();
            for ($i = 0; $i < count($emps); $i++){

                array_walk_recursive($emps[$i],function(&$item){$item=strval($item);});
                $emps[$i]['array_children'] =  DB::table('employee_childrens')->where('LineNo', '=', $emps[$i]->EmployeeNo)->get();
                if ($emps[$i]['array_children']->isNotEmpty()) {
                    for ($j = 0; $j < count($emps[$i]['array_children']); $j++){
                        array_walk_recursive($emps[$i]['array_children'][$j],function(&$item){$item=strval($item);});
                    }
                }

                $emps[$i]['array_attached'] =  DB::table('employee_attached_files')->where('EmpAttachedListNo', '=', $emps[$i]->EmployeeNo)->get();
                if ($emps[$i]['array_attached']->isNotEmpty()) {
                    for ($j = 0; $j < count($emps[$i]['array_attached']); $j++){
                        array_walk_recursive($emps[$i]['array_attached'][$j],function(&$item){$item=strval($item);});
                    }
                }
            }

            return $emps;
        } catch (\Exception $exception)
        {
            return $this->ErrorDataBase();
        }
    }

    public function SearchEmployee(Request $request)
    {
        $allow_header = GlobalController::CheckHeader($request);
        $check_header = json_decode($allow_header->content(), true);
        if ($check_header['status'] == 'error') return $allow_header;
        try {
            $emps = DB::table('employees');

            $check = count($request->get('employee_no'));
            if ($check != 0){

                $first = $request->get("employee_no");
                if($check == 1){
                    $second = $request->get("employee_no_to");
                    $first = $first[0];
                    if($second){
                        $emps = GlobalController::SearchBetween($first, $second, 'employees.EmployeeNo', $emps);
                    }
                    else{
                        if(isset($first)){
                            $emps = $emps->Where('employees.EmployeeNo','LIKE','%'.$first.'%');
                        }
                    }
                }else {
                    $emps = $emps->whereIn('employees.EmployeeNo', $first);

                }
            }



//            if (!empty($request->get('employee_no'))){
//                $first = $request->get("employee_no");
//                $second = $request->get("employee_no_to");
//                if($second){
//                    $emps = GlobalController::SearchBetween($first, $second, 'EmployeeNo', $emps);
//                }
//                else{
//                    $emps->Where('EmployeeNo','LIKE','%'.$request->get("employee_no").'%');
//                }
//            }

            if (!empty($request->get('name_thai'))){
                $first = $request->get("name_thai");
                $second = $request->get("name_thai_to");
                if($second){
                    $emps = GlobalController::SearchBetween($first, $second, 'NameThai', $emps);
                }
                else{
                    $emps->Where('NameThai','LIKE','%'.$request->get("name_thai").'%');
                }
            }


            if (!empty($request->get('name_eng'))){
                $first = $request->get("name_eng");
                $second = $request->get("name_eng_to");
                if($second){
                    $emps = GlobalController::SearchBetween($first, $second, 'NameEng', $emps);
                }
                else{
                    $emps->Where('NameEng','LIKE','%'.$request->get("name_eng").'%');
                }
            }

            if (!empty($request->get('position'))){

                $position = $request->get('position');


                // find db and contain id in array

                $data = [];
                if($position[0] != 'All'){
                    for ($i = 0; $i < count($position); $i++){
                        $id = DB::table('positions')->where('PositionCode', '=', $position[$i])->select('id')->first();
                        array_push($data, $id->id);
                    }
                    $emps = $emps->whereIn('PositionID', $data);
                }else{
                    $emps = $emps;
                }
            }


            if (!empty($request->get('section'))){
                $section = $request->get('section');
//                $section = explode('[',$section);
//                $section = explode(']',$section[1]);
//                $section = explode(',',$section[0]);
//                $section = str_replace('"', '', $section);
                // find db and contain id in array
                $data = [];
                if($section[0] != 'All'){
                    for ($i = 0; $i < count($section); $i++){
                        $id = DB::table('sections')->where('SectionCode', '=', $section[$i])->select('id')->first();
                        array_push($data, $id->id);
                    }
                    $emps = $emps->whereIn('SectionID', $data);
                }else{
                    $emps = $emps;
                }
            }

            if (!empty($request->get('department'))) {
                $department = $request->get('department');

                // find db and contain id in array
                $data = [];
                if($department[0] != 'All'){
                    for ($i = 0; $i < count($department); $i++){
                        $id = DB::table('departments')->where('DepartmentCode', '=', $department[$i])->select('id')->first();
                        array_push($data, $id->id);
                    }
                    $emps = $emps->whereIn('DepartmentID', $data);
                }else{
                    $emps = $emps;
                }
            }
            if (!empty($request->get('work_date'))){
                $first = $request->get("work_date");
                $second = $request->get("work_date_to");
                if($second){
                    $emps = GlobalController::SearchBetween($first, $second, 'StartWorkingDate', $emps);
                }
                else{
                    $emps->Where('StartWorkingDate','LIKE','%'.$request->get("work_date").'%');
                }
            }

            if (!empty($request->get('age'))){
                $first = $request->get("age");
                $second = $request->get("age_to");
                $date = explode('/',date("Y/m/d"));
                $first = $date[0] - $first .'-'. $date[1] .'-'. $date[2];
                $first = date("Y-m-d",strtotime($first));
                $second = $date[0] - $second .'-'. $date[1] .'-'. $date[2];
                $second = date("Y-m-d",strtotime($second));

                if($first != ''){
                    $emps->where('BirthDate', '<', $first)->where('BirthDate', '>', $second);
                }
                else{
                    $emps = $emps;
                }
            }
            if (!empty($request->get('sex'))){
                $sex = $request->get("sex");
                if($sex == 'All'){
                    $emps = $emps;
                }
                if($sex == 'Male'){
                    $emps->Where('PrefixEng','=','Mr');
                }
                if ($sex == 'Female'){
                    $emps->whereIn('PrefixEng', ['Ms','Miss']);
                }
            }


            if (!empty($request->get('employee_type'))){
                if($request->get('employee_type') == 'All'){
                    $emps = $emps;
                }else{
                    $emps->Where('EmployeeTypeId','=',$request->get("employee_type"));
                }
            }

            if (!empty($request->get('employee_status'))){
                if($request->get('employee_status') == 'All'){
                    $emps = $emps;
                }else{
                    $emps->Where('EmployeeStatus','=',$request->get("employee_status"));
                }
            }

            if (!empty($request->get('create_date'))){
                $first = $request->get("create_date");
                $second = $request->get("create_date_to");
                if($second){
                    $emps = GlobalController::SearchBetween($first, $second, 'DateCreate', $emps);
                }
                else{
                    $emps->Where('DateCreate','LIKE','%'.$request->get("create_date").'%');
                }
            }

            if (!empty($request->get('start_working_date'))){
                $first = $request->get("start_working_date");
                $second = $request->get("start_working_date_to");
                if($second){
                    $emps = GlobalController::SearchBetween($first, $second, 'StartWorkingDate', $emps);
                }
                else{
                    $emps->Where('StartWorkingDate','LIKE','%'.$request->get("start_working_date").'%');
                }
            }
            $emps = $emps->get();
            if (isset($emps[0]))
            {
                for ($i = 0; $i < count($emps); $i++){
                    $array_children =  DB::table('employee_childrens')->where('LineNo', '=', $emps[$i]->EmployeeNo)->get();
                    if ($array_children->isNotEmpty()) {
                        for ($j = 0; $j < count($array_children); $j++){
                            array_walk_recursive($array_children[$j],function(&$item){$item=strval($item);});
                        }
                    }

                    $array_attached =  DB::table('employee_attached_files')->where('EmpAttachedListNo', '=', $emps[$i]->EmployeeNo)->get();
                    if ($array_attached->isNotEmpty()) {
                        for ($j = 0; $j < count($array_attached); $j++){
                            array_walk_recursive($array_attached[$j],function(&$item){$item=strval($item);});
                        }
                    }

                    array_walk_recursive($emps[$i],function(&$item){$item=strval($item);});

                    $all_data[] = [
                        'id' => $emps[$i]->id,
                        'EmployeeNo'=> $emps[$i]->EmployeeNo,
                        'IDCard'=>$emps[$i]->IDCard,
                        'MotorcycleDrivingLicenseID'=>$emps[$i]->MotorcycleDrivingLicenseID,
                        'CarDrivingLicenseID'=>$emps[$i]->CarDrivingLicenseID,
                        'TruckDrivingLicenseID'=>$emps[$i]->TruckDrivingLicenseID,
                        'PrefixThai'=>$emps[$i]->PrefixThai,
                        'NameThai'=>$emps[$i]->NameThai,
                        'SurNameThai'=>$emps[$i]->SurNameThai,
                        'PrefixEng'=>$emps[$i]->PrefixEng,
                        'NameEng'=>$emps[$i]->NameEng,
                        'SurNameEng'=>$emps[$i]->SurNameEng,
                        'NickName'=>$emps[$i]->NickName,
                        'EmployeeTypeID'=>$emps[$i]->EmployeeTypeID,
                        'PositionID'=>$emps[$i]->PositionID,
                        'SectionID'=>$emps[$i]->SectionID,
                        'DepartmentID'=>$emps[$i]->DepartmentID,
                        'MobilePhone'=>$emps[$i]->MobilePhone,
                        'email'=>$emps[$i]->email,
                        'BirthDate'=>$emps[$i]->BirthDate,
                        'StartWorkingDate'=>$emps[$i]->StartWorkingDate,
                        'ResignDate'=>$emps[$i]->ResignDate,
                        'SocialSecurityID'=>$emps[$i]->SocialSecurityID,
                        'EmployeeStatus'=>$emps[$i]->EmployeeStatus,
                        'CustomerIDRef'=>$emps[$i]->CustomerIDRef,
                        'Note'=>$emps[$i]->Note,
                        'IDCardExpireDate'=>$emps[$i]->IDCardExpireDate,
                        'MortocycleDrivingLicenseExpireDate'=>$emps[$i]->MortocycleDrivingLicenseExpireDate,
                        'CarDrivingLicenseExpireDate'=>$emps[$i]->CarDrivingLicenseExpireDate,
                        'TruckDrivingLicenseExpireDate'=>$emps[$i]->TruckDrivingLicenseExpireDate,
                        'DateCreate'=>$emps[$i]->DateCreate,
                        'UserCreate'=>$emps[$i]->UserCreate,
                        'DateLastUpdate'=>$emps[$i]->DateLastUpdate,
                        'UserLastUpdate'=>$emps[$i]->UserLastUpdate,
                        'MilitaryStatus'=>$emps[$i]->MilitaryStatus,
                        'MilitaryStatusExtension'=>$emps[$i]->MilitaryStatusExtension,
                        'Nationality'=>$emps[$i]->Nationality,
                        'Origin'=>$emps[$i]->Origin,
                        'Religious'=>$emps[$i]->Religious,
                        'MaritalStatus'=>$emps[$i]->MaritalStatus,
                        'NativeHabitat'=>$emps[$i]->NativeHabitat,
                        'Address'=>$emps[$i]->Address,
                        'PostCode'=>$emps[$i]->PostCode,
                        'Country'=>$emps[$i]->Country,
                        'Province'=>$emps[$i]->Province,
                        'District'=>$emps[$i]->District,
                        'SubDistrict'=>$emps[$i]->SubDistrict,
                        'FacebookID'=>$emps[$i]->FacebookID,
                        'LineID'=>$emps[$i]->LineID,
                        'Reference'=>$emps[$i]->Reference,
                        'ReferecncePhone'=>$emps[$i]->ReferecncePhone,
                        'ReferenceRelationship'=>$emps[$i]->ReferenceRelationship,
                        'FatherName'=>$emps[$i]->FatherName,
                        'FatherLastName'=>$emps[$i]->FatherLastName,
                        'MotherName'=>$emps[$i]->MotherName,
                        'MotherLastName'=>$emps[$i]->MotherLastName,
                        'BloodGroup'=>$emps[$i]->BloodGroup,
                        'MateName'=>$emps[$i]->MateName,
                        'MateSurname'=>$emps[$i]->MateSurname,
                        'MateIDCard'=>$emps[$i]->MateIDCard,
                        'MateBirthDate'=>$emps[$i]->MateBirthDate,
                        'FirstEducationalBackground'=>$emps[$i]->FirstEducationalBackground,
                        'FirstEducationalBackgroundFaculty'=>$emps[$i]->FirstEducationalBackgroundFaculty,
                        'FirstEducationalPlace'=>$emps[$i]->FirstEducationalPlace,
                        'FirstGraduateYear'=>$emps[$i]->FirstGraduateYear,
                        'CurrentEducationalBackground'=>$emps[$i]->CurrentEducationalBackground,
                        'CurrentEducationalBackgroundFaculty'=>$emps[$i]->CurrentEducationalBackgroundFaculty,
                        'CurrentEducationalPlace'=>$emps[$i]->CurrentEducationalPlace,
                        'CurrentGraduateYear'=>$emps[$i]->CurrentGraduateYear,
                        'ExternalExperienceYear'=>$emps[$i]->ExternalExperienceYear,
                        'ExternalExperienceMonth'=>$emps[$i]->ExternalExperienceMonth,
                        'UnderlyingDisease'=>$emps[$i]->UnderlyingDisease,
                        'Height'=>$emps[$i]->Height,
                        'Weight'=>$emps[$i]->Weight,
                        'BonusPerYear'=>$emps[$i]->BonusPerYear,
                        'RatePerYear'=>$emps[$i]->RatePerYear,
                        'AdjustPerYear'=>$emps[$i]->AdjustPerYear,
                        'LastSalaryAdjust'=>$emps[$i]->LastSalaryAdjust,
                        'LastPotentialAdjust'=>$emps[$i]->LastPotentialAdjust,
                        'ExternalExp'=>$emps[$i]->ExternalExp,
                        'Bonus'=>$emps[$i]->Bonus,
                        'array_children' => $array_children,
                        'array_attached' => $array_attached,
                    ];

                }
            }

            $array = json_decode(json_encode($all_data), true);

            return $array;
        } catch (\Exception $exception)
        {
            return response()->json([
                "status" => "not found",
                "message" => "bad requests"
            ], 401);
        }
    }
    // sort emp
    public function SortEmployee(Request $request){
        $allow_header = GlobalController::CheckHeader($request);
        $check_header = json_decode($allow_header->content(), true);
        if ($check_header['status'] == 'error')
        {
            return $allow_header;
        }

        try {
            $sorts = $request->get("array_sort");
            $emp = DB::table('employees');
            foreach ($sorts as $sort){
                if($sort['order'] == "maxtomin"){
                    $emp = $emp->orderBy($sort['coloum_name'], 'desc');


                }else{
                    $emp = $emp->orderBy($sort['coloum_name'], 'asc');
//                    $role = $role->orderBy($sort['coloum_name'], 'asc');
                }
            }
            $emp = $emp->get();
            return response()->json([
                "status" => "success",
                "message" => $emp
            ], 200);
        }catch (\Exception $exception){
            return $exception;
        }
    }


    public function StateEmployee(Request $request){
        $allow_header = GlobalController::CheckHeader($request);
        $check_header = json_decode($allow_header->content(), true);
        if ($check_header['status'] == 'error') return $allow_header;

        try {
            $emp = Employee::where('id', '=', $check_header['id'])->first();
            $emp->StateEmployee = $request->get('data');
            if ($emp->save()) {
                return response()->json([
                    "status" => "success",
                    "message" => "update"
                ], 200);
            } else {
                return response()->json([
                    "status" => "error",
                    "message" => "Bad Request"
                ], 400);
            }
        } catch (\Exception $exception){
            return $this->ErrorDataBase();
        }
    }
    public function GetStateEmployee(Request $request){
        $allow_header = GlobalController::CheckHeader($request);
        $check_header = json_decode($allow_header->content(), true);
        if ($check_header['status'] == 'error')
        {
            return $allow_header;
        }

        try {
            $emp = Employee::where('id', '=', $check_header['id'])->first();
            return response()->json([
                "status" => "success",
                "message" => $emp->StateEmployee
            ], 200);

        } catch (\Exception $exception){
            return $this->ErrorDataBase();
        }
    }

    public function StateEmployeeChild(Request $request){
        $allow_header = GlobalController::CheckHeader($request);
        $check_header = json_decode($allow_header->content(), true);
        if ($check_header['status'] == 'error') return $allow_header;

        try {
            $emp = Employee::where('id', '=', $check_header['id'])->first();
            $emp->StateEmployeeChild = $request->get('data');
            if ($emp->save()) {
                return response()->json([
                    "status" => "success",
                    "message" => "update"
                ], 200);
            } else {
                return response()->json([
                    "status" => "error",
                    "message" => "Bad Request"
                ], 400);
            }
        } catch (\Exception $exception){
            return $this->ErrorDataBase();
        }
    }
    public function GetStateEmployeeChild(Request $request){
        $allow_header = GlobalController::CheckHeader($request);
        $check_header = json_decode($allow_header->content(), true);
        if ($check_header['status'] == 'error')
        {
            return $allow_header;
        }

        try {
            $emp = Employee::where('id', '=', $check_header['id'])->first();
            return response()->json([
                "status" => "success",
                "message" => $emp->StateEmployeeChild
            ], 200);

        } catch (\Exception $exception){
            return $this->ErrorDataBase();
        }
    }
    public function StateEmployeeAttach(Request $request){
        $allow_header = GlobalController::CheckHeader($request);
        $check_header = json_decode($allow_header->content(), true);
        if ($check_header['status'] == 'error') return $allow_header;

        try {
            $emp = Employee::where('id', '=', $check_header['id'])->first();
            $emp->StateEmployeeAttach = $request->get('data');
            if ($emp->save()) {
                return response()->json([
                    "status" => "success",
                    "message" => "update"
                ], 200);
            } else {
                return response()->json([
                    "status" => "error",
                    "message" => "Bad Request"
                ], 400);
            }
        } catch (\Exception $exception){
            return $this->ErrorDataBase();
        }
    }
    public function GetStateEmployeeAttach(Request $request){
        $allow_header = GlobalController::CheckHeader($request);
        $check_header = json_decode($allow_header->content(), true);
        if ($check_header['status'] == 'error')
        {
            return $allow_header;
        }

        try {
            $emp = Employee::where('id', '=', $check_header['id'])->first();

            return response()->json([
                "status" => "success",
                "message" => $emp->StateEmployeeAttach
            ], 200);

        } catch (\Exception $exception){
            return $this->ErrorDataBase();
        }
    }
    public function GetEmployeeType(Request $request){
        $allow_header = GlobalController::CheckHeader($request);
        $check_header = json_decode($allow_header->content(), true);
        if ($check_header['status'] == 'error')
        {
            return $allow_header;
        }
        try {
            $type = EmployeeType::all();
            return $type;

        }catch (\Exception $exception){
            return $this->ErrorDataBase();

        }
    }
    public function GetPosition(Request $request){
        $allow_header = GlobalController::CheckHeader($request);
        $check_header = json_decode($allow_header->content(), true);
        if ($check_header['status'] == 'error') {
            return $allow_header;
        }
        try {
            $type = Position::all();
            return $type;
        }catch (\Exception $exception){
            return $this->ErrorDataBase();
        }
    }
    public function GetDepartment(Request $request){
        $allow_header = GlobalController::CheckHeader($request);
        $check_header = json_decode($allow_header->content(), true);
        if ($check_header['status'] == 'error')
        {
            return $allow_header;
        }
        try {
            $type = Department::all();
            return $type;

        }catch (\Exception $exception){
            return $this->ErrorDataBase();

        }
    }
    public function GetSection(Request $request){
        $allow_header = GlobalController::CheckHeader($request);
        $check_header = json_decode($allow_header->content(), true);
        if ($check_header['status'] == 'error')
        {
            return $allow_header;
        }
        try {
            $data = Section::all();
            return $data;

        }catch (\Exception $exception){
            return $this->ErrorDataBase();

        }
    }
    public function GetSubDistrict(Request $request){
        $allow_header = GlobalController::CheckHeader($request);
        $check_header = json_decode($allow_header->content(), true);
        if ($check_header['status'] == 'error')
        {
            return $allow_header;
        }
        try {
            $data = SubDistrict::all();
            return $data;

        }catch (\Exception $exception){
            return $this->ErrorDataBase();

        }
    }
    public function GetDistrict(Request $request){
        $allow_header = GlobalController::CheckHeader($request);
        $check_header = json_decode($allow_header->content(), true);
        if ($check_header['status'] == 'error')
        {
            return $allow_header;
        }
        try {
            $data = District::all();
            return $data;

        }catch (\Exception $exception){
            return $this->ErrorDataBase();

        }
    }
    public function GetProvince(Request $request){
        $allow_header = GlobalController::CheckHeader($request);
        $check_header = json_decode($allow_header->content(), true);
        if ($check_header['status'] == 'error')
        {
            return $allow_header;
        }
        try {
            $data = Province::all();
            return $data;

        }catch (\Exception $exception){
            return $this->ErrorDataBase();

        }
    }
    public function GetCountry(Request $request){
        $allow_header = GlobalController::CheckHeader($request);
        $check_header = json_decode($allow_header->content(), true);
        if ($check_header['status'] == 'error')
        {
            return $allow_header;
        }
        try {
            $data = Country::all();
            return $data;

        }catch (\Exception $exception){
            return $this->ErrorDataBase();

        }
    }
    public function GetMaterialType(Request $request){
        $allow_header = GlobalController::CheckHeader($request);
        $check_header = json_decode($allow_header->content(), true);
        if ($check_header['status'] == 'error')
        {
            return $allow_header;
        }
        try {
            $data = MaterialType::all();
            return $data;

        }catch (\Exception $exception){
            return $this->ErrorDataBase();

        }
    }

    public function ChangePassword(Request $request){

        $allow_header = GlobalController::CheckHeader($request);
        $check_header = json_decode($allow_header->content(), true);
        if ($check_header['status'] == 'error')
        {
            return $allow_header;
        }

        if($user = Employee::where('email', '=', $request->get('email'))->first()){

            if ($request->get('password')) {

                $user->old_password = $user->password;
                $user->password = Hash::make($request->get('newpass'));
                $user->save();
            }
        }
    }
}
