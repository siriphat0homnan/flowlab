<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Providers\Model\CRCustomer;
use App\Providers\Model\CRCustomerContact;
use App\Providers\Model\CRCustomerLocation;
use App\Providers\Model\CustomerContact;
use App\Providers\Model\CustomerLocation;
use App\Providers\Model\Employee;
use App\Providers\Model\Customer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CustomerController extends Controller
{
    //
    // create customer
    public function CreateCustomer(Request $request){

        $allow_header = GlobalController::CheckHeader($request);
        $check_header = json_decode($allow_header->content(), true);
        if ($check_header['status'] == 'error')
        {
            return $allow_header;
        }
        try {
            $haveCustomer = Customer::where('CustomerCode', '=', $request->get('customer_code'))->first();
            if($haveCustomer){
                return response()->json([
                    "status" => "error",
                    "message" => "Duplicate Customer Code "
                ], 400);
            }


//            $last_customer = DB::table('customers')->orderBy('CustomerCode', 'desc')->select('CustomerCode')->get();
//
//            $last = $last_customer[0]->CustomerCode;
//            $last++;
            $last_customer = DB::table('c_r_customers')->orderBy('CustomerCode', 'desc')->select('CustomerCode')->get();
            $lastApprove = GlobalController::CreateApprove($check_header['id'], 'Create Customer', 'customer', $check_header);


            if(count($last_customer) == 0){
                $lastCus = 'CTM0001';
            }else{
                $lastCus = $last_customer[0]->CustomerCode;
                $lastCus++;
            }

            $customer = new CRCustomer();
            $customer->ApproveKey = $lastApprove;

            // tab 1
            $customer->CustomerCode = $lastCus;
            $customer->CustomerNameEng = $request->get('customer_name_eng');
            $customer->CustomerNameThai = $request->get('customer_name_thai');
            $customer->CustomerInitialsName = $request->get('customer_initials_name');
            $customer->Business = $request->get('business');
            $customer->Address = $request->get('address');
            $customer->CustomerStatus = $request->get('customer_status');
            $customer->Distance = $request->get('distance');
            $customer->Latitude = $request->get('latitude');
            $customer->Longitude = $request->get('longitude');
            $customer->Note = $request->get('note');

            $customer->Country = $request->get("country");
            $customer->Province = $request->get("province");
            $customer->District = $request->get("district");
            $customer->SubDistrict = $request->get("sub_district");

            $customer->DateCreate = now();
            $customer->UserCreate = $check_header['id'];
            $customer->DateLastUpdate = now();
            $customer->UserLastUpdate = $check_header['id'];
            if ($customer->save()) {
                // tab2
                $arrLocation = $request->get('array_location');
                if(count($arrLocation) != 0){
                    for ($i = 0; $i < count($arrLocation); $i++){
                        $newChild = new CRCustomerLocation();
                        $newChild->ApproveKey = $lastApprove;
                        $newChild->CustomerSkey = $lastCus;

                        $newChild->LocationNameEng = $arrLocation[$i]["location_name_eng"];
                        $newChild->LocationNameThai = $arrLocation[$i]["location_name_thai"];
                        $newChild->LocationInitialsName = $arrLocation[$i]["location_initials_name"];
                        $newChild->Address = $arrLocation[$i]["address"];
                        $newChild->LocationStatus = $arrLocation[$i]["location_status"];
                        $newChild->Distance = $arrLocation[$i]["distance"];
                        $newChild->Latitude = $arrLocation[$i]["latitude"];
                        $newChild->Longitude = $arrLocation[$i]["longitude"];
                        $newChild->Country = $arrLocation[$i]["country"];
                        $newChild->Province = $arrLocation[$i]["province"];
                        $newChild->District = $arrLocation[$i]["district"];
                        $newChild->SubDistrict = $arrLocation[$i]["sub_district"];
                        $newChild->DateCreate = now();
                        $newChild->UserCreate = $check_header['id'];
                        $newChild->DateLastUpdate = now();
                        $newChild->UserLastUpdate = $check_header['id'];
                        $newChild->save();
                    }
                }

                // tab3
                $arrContact = $request->get('array_contact');
                if(count($arrContact) != 0){
                    for ($i = 0; $i < count($arrContact); $i++){
                        $newContact = new CRCustomerContact();
                        $newContact->ApproveKey = $lastApprove;

                        $newContact->CustomerSkey = $lastCus;

                        $newContact->PrefixThai = $arrContact[$i]["prefix_thai"];
                        $newContact->NameThai = $arrContact[$i]["name_thai"];
                        $newContact->SurNameThai = $arrContact[$i]["surname_thai"];
                        $newContact->PrefixEng = $arrContact[$i]["prefix_eng"];
                        $newContact->NameEng = $arrContact[$i]["name_eng"];
                        $newContact->SurNameEng = $arrContact[$i]["surname_eng"];
                        $newContact->NickName = $arrContact[$i]["nickname"];
                        $newContact->Telephone = $arrContact[$i]["telephone"];
                        $newContact->MobilePhone = $arrContact[$i]["mobile_phone"];
                        $newContact->Fax = $arrContact[$i]["fax"];
                        $newContact->Email = $arrContact[$i]["email"];
                        $newContact->BirthDate = $arrContact[$i]["birthdate"];
                        $newContact->ContactStatus = $arrContact[$i]["contact_status"];

                        $newContact->DateCreate = now();
                        $newContact->UserCreate = $check_header['id'];
                        $newContact->DateLastUpdate = now();
                        $newContact->UserLastUpdate = $check_header['id'];
                        $newContact->save();
                    }
                }


                return response()->json([
                    "status" => "success",
                    "message" => "created"
                ], 201);
            } else {
                return response()->json([
                    "status" => "error",
                    "message" => "Bad Request"
                ], 400);
            }
            return $customer;

        } catch (\Exception $exception)
        {
            return $this->ErrorDataBase();
        }

    }

    //get customer
    public function GetCustomer(Request $request){
        $allow_header = GlobalController::CheckHeader($request);
        $check_header = json_decode($allow_header->content(), true);
        if ($check_header['status'] == 'error') return $allow_header;

        try {
//            $customers = DB::table('customers')->get();
            $customers = Customer::all();
            // tab 1
            for ($i = 0; $i < count($customers); $i++){

                if(isset($customers[$i]->UserCreate)){
                    $emp_to_create = DB::table('employees')->where('id', '=', $customers[$i]->UserCreate)->get();
                    $customers[$i]->UserCreate = $emp_to_create[0]->email;
                }
                if(isset($customers[$i]->UserLastUpdate)){
                    $emp_to_update = DB::table('employees')->where('id', '=', $customers[$i]->UserLastUpdate)->get();
                    $customers[$i]->UserLastUpdate = $emp_to_update[0]->email;
                }

                $customers[$i]['array_contact'] =  DB::table('customer_contacts')->where('CustomerSkey', '=', $customers[$i]->CustomerCode)->get();
                if ($customers[$i]['array_contact']->isNotEmpty()) {
                    for ($j = 0; $j < count($customers[$i]['array_contact']); $j++){
                        array_walk_recursive($customers[$i]['array_contact'][$j],function(&$item){$item=strval($item);});
                    }
                }

                $customers[$i]['array_location'] =  DB::table('customer_locations')->where('CustomerSkey', '=', $customers[$i]->CustomerCode)->get();
                if ($customers[$i]['array_location']->isNotEmpty()) {
                    for ($j = 0; $j < count($customers[$i]['array_location']); $j++){
                        array_walk_recursive($customers[$i]['array_location'][$j],function(&$item){$item=strval($item);});
                    }
                }
            }

            return $customers;

        } catch (\Exception $exception)
        {
            return $this->ErrorDataBase();
        }
    }

    //put customer
    public function UpdateCustomer(Request $request){
        $allow_header = GlobalController::CheckHeader($request);
        $check_header = json_decode($allow_header->content(), true);
        if ($check_header['status'] == 'error') return $allow_header;
        try {
            $lastApprove = GlobalController::CreateApprove($check_header['id'], 'Update Customer', 'customer', $check_header);

            $customer = new CRCustomer();
            $customer->ApproveKey = $lastApprove;

            // tab 1
            $customer->CustomerCode = $request->get('customer_code');
            $customer->CustomerNameEng = $request->get('customer_name_eng');
            $customer->CustomerNameThai = $request->get('customer_name_thai');
            $customer->CustomerInitialsName = $request->get('customer_initials_name');
            $customer->Business = $request->get('business');
            $customer->Address = $request->get('address');
            $customer->CustomerStatus = $request->get('customer_status');
            $customer->Distance = $request->get('distance');
            $customer->Latitude = $request->get('latitude');
            $customer->Longitude = $request->get('longitude');
            $customer->Note = $request->get('note');
            $customer->Country = $request->get("country");
            $customer->Province = $request->get("province");
            $customer->District = $request->get("district");
            $customer->SubDistrict = $request->get("sub_district");
            $customer->DateCreate = $request->get("date_create");
            $customer->UserCreate = $request->get("user_create");
            $customer->DateLastUpdate = now();
            $customer->UserLastUpdate = $check_header['id'];
            $spitYellow = implode('","', $request->get("yellow"));

            $customer->Yellow = '["'.$spitYellow.'"]';
            if($customer->Yellow  == "[\"\"]"){
                $customer->Yellow  = null;
            }

            $locations = $request->get("array_location");
//            $locations = ($locations == '[]') ? [] : json_decode($locations, true);

            foreach ($locations as $arrLocation){
                $newChild = new CRCustomerLocation();
                $newChild->ApproveKey = $lastApprove;
                $newChild->CustomerSkey = $customer->CustomerCode;
                $newChild->LocationNameEng = $arrLocation["location_name_eng"];
                $newChild->LocationNameThai = $arrLocation["location_name_thai"];
                $newChild->LocationInitialsName = $arrLocation["location_initials_name"];
                $newChild->Address = $arrLocation["address"];
                $newChild->LocationStatus = $arrLocation["location_status"];
                $newChild->Distance = $arrLocation["distance"];
                $newChild->Latitude = $arrLocation["latitude"];
                $newChild->Longitude = $arrLocation["longitude"];
                $newChild->Country = $arrLocation["country"];
                $newChild->Province = $arrLocation["province"];
                $newChild->District = $arrLocation["district"];
                $newChild->SubDistrict = $arrLocation["sub_district"];
                $newChild->DateLastUpdate = now();
                $newChild->UserLastUpdate = $check_header['id'];

                $spitYellow = implode('","', $arrLocation["yellow"]);
                $newChild->Yellow  = '["'.$spitYellow.'"]';
                if($newChild->Yellow  == "[\"\"]"){
                    $newChild->Yellow   = null;
                }

//                $spitYellow = implode('","', $request->get("yellow"));
//
//                $newChild->Yellow = '["'.$spitYellow.'"]';
//                if($newChild->Yellow  == "[\"\"]"){
//                    $newChild->Yellow  = null;
//                }

                $newChild->save();
            }

            $arrContact = $request->get('array_contact');
            if(count($arrContact) != 0){
                for ($i = 0; $i < count($arrContact); $i++){
                    $newContact = new CRCustomerContact();
                    $newContact->ApproveKey = $lastApprove;
                    $newContact->CustomerSkey = $customer->CustomerCode;
                    $newContact->PrefixThai = $arrContact[$i]["prefix_thai"];
                    $newContact->NameThai = $arrContact[$i]["name_thai"];
                    $newContact->SurNameThai = $arrContact[$i]["surname_thai"];
                    $newContact->PrefixEng = $arrContact[$i]["prefix_eng"];
                    $newContact->NameEng = $arrContact[$i]["name_eng"];
                    $newContact->SurNameEng = $arrContact[$i]["surname_eng"];
                    $newContact->NickName = $arrContact[$i]["nickname"];
                    $newContact->Telephone = $arrContact[$i]["telephone"];
                    $newContact->MobilePhone = $arrContact[$i]["mobile_phone"];
                    $newContact->Fax = $arrContact[$i]["fax"];
                    $newContact->Email = $arrContact[$i]["email"];
                    $newContact->BirthDate = $arrContact[$i]["birthdate"];
                    $newContact->ContactStatus = $arrContact[$i]["contact_status"];

                    $spitYellow = implode('","', $arrContact[$i]["yellow"]);
                     $newContact->Yellow  = '["'.$spitYellow.'"]';
                    if( $newContact->Yellow  == "[\"\"]"){
                         $newContact->Yellow   = null;
                    }


                    $newContact->DateCreate = now();
                    $newContact->UserCreate = $check_header['id'];
                    $newContact->DateLastUpdate = now();
                    $newContact->UserLastUpdate = $check_header['id'];
                    $newContact->save();
                }
            }



            // =======

//
//            $customer = Customer::where('id', '=', $request->get('id'))->first();
//            // tab 1
//
//            $customer->CustomerNameEng = $request->get('customer_name_eng');
//            $customer->CustomerNameThai = $request->get('customer_name_thai');
//            $customer->CustomerInitialsName = $request->get('customer_initials_name');
//            $customer->Business = $request->get('business');
//            $customer->Address = $request->get('address');
//            $customer->CustomerStatus = $request->get('customer_status');
//            $customer->Distance = $request->get('distance');
//            $customer->Latitude = $request->get('latitude');
//            $customer->Longitude = $request->get('longitude');
//            $customer->Note = $request->get('note');
//            $customer->CountrySkey = $request->get("country");
//            $customer->ProvinceSkey = $request->get("province");
//            $customer->DistrictSkey = $request->get("district");
//            $customer->SubDistrictSkey = $request->get("sub_district");
//            $customer->DateLastUpdate = now();
//            $customer->UserLastUpdate = $check_header['id'];
//
//            // tab 2
//
//            $locations = $request->get("array_location");
//            $dbCustomer = CustomerLocation::where("CustomerSkey", '=', $request->get('customer_code'))->get();
//
//            if (count($dbCustomer) != 0){
//                // delete all
//
//                if (count($locations) == 0){
//                    foreach ($dbCustomer as $Customer){
//                        $Customer->delete();
//                    }
//                }
//                // delete
//
//                if (count($dbCustomer) > count($locations)){
//                    // update
//                    for ($i = 0; $i < count($locations); $i++ ){
//
//                        $dbCustomer[$i]->LocationNameEng = $locations[$i]["location_name_eng"];
//                        $dbCustomer[$i]->LocationNameThai = $locations[$i]["location_name_thai"];
//                        $dbCustomer[$i]->LocationInitialsName = $locations[$i]["location_initials_name"];
//                        $dbCustomer[$i]->Address = $locations[$i]["address"];
//                        $dbCustomer[$i]->CountrySkey = $locations[$i]["country"];
//                        $dbCustomer[$i]->ProvinceSkey = $locations[$i]["province"];
//                        $dbCustomer[$i]->DistrictSkey = $locations[$i]["district"];
//                        $dbCustomer[$i]->SubDistrictSkey = $locations[$i]["sub_district"];
//                        $dbCustomer[$i]->LocationStatus = $locations[$i]["location_status"];
//                        $dbCustomer[$i]->Distance = $locations[$i]["distance"];
//                        $dbCustomer[$i]->Latitude = $locations[$i]["latitude"];
//                        $dbCustomer[$i]->Longitude = $locations[$i]["longitude"];
//                        $dbCustomer[$i]->DateLastUpdate = now();
//                        $dbCustomer[$i]->UserLastUpdate = $check_header['id'];
//                        $dbCustomer[$i]->save();
//
//                    }
//
//                    $delIndex = [];
//                    $delete = $dbCustomer->toArray();
//                    $countIndex = 0;
//
//                    // find index
//                    for ($i = 0; $i < count($locations); $i++ ){
//                        for ($j = 0; $j < count($delete); $j++ ) {
//                            if ( (array_search($locations[$i]["id"], $delete[$j]) )){
//                                $delIndex[$countIndex] = $j;
//                                $countIndex++;
//                            }
//                        }
//                    }
//
//                    // remember data
//                    for ($i = 0; $i < count($delIndex); $i++ ){
//                        unset($delete[$delIndex[$i]]);
//                    }
//
//
//                    $delete = array_values(array_filter($delete));
//
//                    for ($i = 0; $i < count($dbCustomer); $i++ ){
//                        for ($j = 0; $j < count($delete); $j++ ){
//                            if($delete[$j]["id"] == $dbCustomer[$i]["id"]){
//                                $del = CustomerLocation::where("id", '=', $delete[$j]["id"])->first();
//                                $del->delete();
//                            }
//                        }
//                    }
//                }
//
//                // update all
//                if(count($dbCustomer) == count($locations)){
//
//                    foreach ($locations as $location){
//                        $newLocation = CustomerLocation::where('id', '=', $location['id'])->first();
//                        $newLocation->LocationNameEng = $location["location_name_eng"];
//                        $newLocation->LocationNameThai = $location["location_name_thai"];
//                        $newLocation->LocationInitialsName = $location["location_initials_name"];
//                        $newLocation->Address = $location["address"];
//                        $newLocation->CountrySkey = $location["country"];
//                        $newLocation->ProvinceSkey = $location["province"];
//                        $newLocation->DistrictSkey = $location["district"];
//                        $newLocation->SubDistrictSkey = $location["sub_district"];
//                        $newLocation->LocationStatus = $location["location_status"];
//                        $newLocation->Distance = $location["distance"];
//                        $newLocation->Latitude = $location["latitude"];
//                        $newLocation->Longitude = $location["longitude"];
//                        $newLocation->DateLastUpdate = now();
//                        $newLocation->UserLastUpdate = $check_header['id'];
//                        $newLocation->save();
//                    }
//
//                }
//                // update and insert
//                if(count($dbCustomer) < count($locations)){
//                    $diffLen = count($locations) - count($dbCustomer);
//                    // update
//                    for ($i = 0; $i < count($dbCustomer) + 1 - $diffLen; $i++ ){
//                        $newLocation[$i] = CustomerLocation::where('id', '=', $locations[$i]['id'])->first();
//                        $newLocation[$i]->LocationNameEng = $locations[$i]["location_name_eng"];
//                        $newLocation[$i]->LocationNameThai = $locations[$i]["location_name_thai"];
//                        $newLocation[$i]->LocationInitialsName = $locations[$i]["location_initials_name"];
//                        $newLocation[$i]->Address = $locations[$i]["address"];
//                        $newLocation[$i]->CountrySkey = $locations[$i]["country"];
//                        $newLocation[$i]->ProvinceSkey = $locations[$i]["province"];
//                        $newLocation[$i]->DistrictSkey = $locations[$i]["district"];
//                        $newLocation[$i]->SubDistrictSkey = $locations[$i]["sub_district"];
//                        $newLocation[$i]->LocationStatus = $locations[$i]["location_status"];
//                        $newLocation[$i]->Distance = $locations[$i]["distance"];
//                        $newLocation[$i]->Latitude = $locations[$i]["latitude"];
//                        $newLocation[$i]->Longitude = $locations[$i]["longitude"];
//                        $newLocation[$i]->DateCreate = now();
//                        $newLocation[$i]->UserCreate = $check_header['id'];
//                        $newLocation[$i]->DateLastUpdate = now();
//                        $newLocation[$i]->UserLastUpdate = $check_header['id'];
//                        $newLocation[$i]->save();
//                    }
//
//                    // insert
//                    for ($i = 0; $i < $diffLen; $i++ ){
//                        $newLocation[$i] = new CustomerLocation();
//                        $newLocation[$i]->CustomerSkey = $request->get('customer_code');
//                        $newLocation[$i]->LocationNameEng = $locations[$i]["location_name_eng"];
//                        $newLocation[$i]->LocationNameThai = $locations[$i]["location_name_thai"];
//                        $newLocation[$i]->LocationInitialsName = $locations[$i]["location_initials_name"];
//                        $newLocation[$i]->Address = $locations[$i]["address"];
//                        $newLocation[$i]->CountrySkey = $locations[$i]["country"];
//                        $newLocation[$i]->ProvinceSkey = $locations[$i]["province"];
//                        $newLocation[$i]->DistrictSkey = $locations[$i]["district"];
//                        $newLocation[$i]->SubDistrictSkey = $locations[$i]["sub_district"];
//                        $newLocation[$i]->LocationStatus = $locations[$i]["location_status"];
//                        $newLocation[$i]->Distance = $locations[$i]["distance"];
//                        $newLocation[$i]->Latitude = $locations[$i]["latitude"];
//                        $newLocation[$i]->Longitude = $locations[$i]["longitude"];
//                        $newLocation[$i]->DateCreate = now();
//                        $newLocation[$i]->UserCreate = $check_header['id'];
//                        $newLocation[$i]->DateLastUpdate = now();
//                        $newLocation[$i]->UserLastUpdate = $check_header['id'];
//                        $newLocation[$i]->save();
//                    }
//
//                }
//
//            }else{
//                // insert all data
//                foreach ($locations as $location){
//
//                    $newLocation = new CustomerLocation();
//
//                    $newLocation->CustomerSkey = $request->get('customer_code');
//                    $newLocation->LocationNameEng = $location["location_name_eng"];
//                    $newLocation->LocationNameThai = $location["location_name_thai"];
//                    $newLocation->LocationInitialsName = $location["location_initials_name"];
//                    $newLocation->Address = $location["address"];
//                    $newLocation->CountrySkey = $location["country"];
//                    $newLocation->ProvinceSkey = $location["province"];
//                    $newLocation->DistrictSkey = $location["district"];
//                    $newLocation->SubDistrictSkey = $location["sub_district"];
//                    $newLocation->LocationStatus = $location["location_status"];
//                    $newLocation->Distance = $location["distance"];
//                    $newLocation->Latitude = $location["latitude"];
//                    $newLocation->Longitude = $location["longitude"];
//                    $newLocation->DateCreate = now();
//                    $newLocation->UserCreate = $check_header['id'];
//                    $newLocation->DateLastUpdate = now();
//                    $newLocation->UserLastUpdate = $check_header['id'];
//                    $newLocation->save();
//                }
//            }
//
//            // tab 3
//            $contacts = $request->get("array_contact");
//            $dbCustomer = CustomerContact::where("CustomerSkey", '=', $request->get('customer_code'))->get();
//            if (count($dbCustomer) != 0){
//                if (count($contacts) == 0){
//                    foreach ($dbCustomer as $Customer){
//                        $Customer->delete();
//                    }
//                }
//                // delete
//                if (count($dbCustomer) > count($contacts)){
//                    // update
//                    for ($i = 0; $i < count($contacts); $i++ ){
//
//                        $dbCustomer[$i]->PrefixThai = $contacts[$i]["prefix_thai"];
//                        $dbCustomer[$i]->NameThai = $contacts[$i]["name_thai"];
//                        $dbCustomer[$i]->SurNameThai = $contacts[$i]["surname_thai"];
//                        $dbCustomer[$i]->PrefixEng = $contacts[$i]["prefix_eng"];
//                        $dbCustomer[$i]->NameEng = $contacts[$i]["name_eng"];
//                        $dbCustomer[$i]->SurNameEng = $contacts[$i]["surname_eng"];
//
//                        $dbCustomer[$i]->NickName = $contacts[$i]["nickname"];
//                        $dbCustomer[$i]->PositionSkey = $contacts[$i]["position"];
//                        $dbCustomer[$i]->Telephone = $contacts[$i]["telephone"];
//                        $dbCustomer[$i]->MobilePhone = $contacts[$i]["mobile_phone"];
//                        $dbCustomer[$i]->Fax = $contacts[$i]["fax"];
//                        $dbCustomer[$i]->Email = $contacts[$i]["email"];
//                        $dbCustomer[$i]->BirthDate = $contacts[$i]["birthdate"];
//                        $dbCustomer[$i]->ContactStatus = $contacts[$i]["contact_status"];
//                        $dbCustomer[$i]->DateCreate = now();
//                        $dbCustomer[$i]->UserCreate = $check_header['id'];
//                        $dbCustomer[$i]->DateLastUpdate = now();
//                        $dbCustomer[$i]->UserLastUpdate = $check_header['id'];
//                        $dbCustomer[$i]->save();
//
//                    }
//
//                    $delIndex = [];
//                    $delete = $dbCustomer->toArray();
//                    $countIndex = 0;
//                    // find index
//                    for ($i = 0; $i < count($contacts); $i++ ){
//                        for ($j = 0; $j < count($delete); $j++ ) {
//
//                            if ( (array_search((string)$contacts[$i]["id"], $delete[$j]) )){
//                                $delIndex[$countIndex] = $j;
//                                $countIndex++;
//                            }
//
//                        }
//                    }
//
//                    // remember data
//                    for ($i = 0; $i < count($delIndex); $i++ ){
//                        unset($delete[$delIndex[$i]]);
//                    }
//                    $delete = array_values(array_filter($delete));
//                    for ($i = 0; $i < count($dbCustomer); $i++ ){
//                        for ($j = 0; $j < count($delete); $j++ ){
//                            if($delete[$j]["id"] == $dbCustomer[$i]["id"]){
//
//                                $del = CustomerContact::where("id", '=', $delete[$j]["id"])->first();
//                                $del->delete();
//                            }
//                        }
//                    }
//
//                }
//                // update all
//                if(count($dbCustomer) == count($contacts)){
//                    foreach ($contacts as $contact){
//                        $newContact = CustomerContact::where('id', '=', $contact['id'])->first();
//                        $newContact->PrefixThai = $contact["prefix_thai"];
//                        $newContact->NameThai = $contact["name_thai"];
//                        $newContact->SurNameThai = $contact["surname_thai"];
//                        $newContact->PrefixEng = $contact["prefix_eng"];
//                        $newContact->NameEng = $contact["name_eng"];
//                        $newContact->SurNameEng = $contact["surname_eng"];
//
//                        $newContact->NickName = $contact["nickname"];
//                        $newContact->PositionSkey = $contact["position"];
//                        $newContact->Telephone = $contact["telephone"];
//                        $newContact->MobilePhone = $contact["mobile_phone"];
//                        $newContact->Fax = $contact["fax"];
//                        $newContact->Email = $contact["email"];
//                        $newContact->BirthDate = $contact["birthdate"];
//                        $newContact->ContactStatus = $contact["contact_status"];
//                        $newContact->DateCreate = now();
//                        $newContact->UserCreate = $check_header['id'];
//                        $newContact->DateLastUpdate = now();
//                        $newContact->UserLastUpdate = $check_header['id'];
//                        $newContact->save();
//                    }
//
//                }
//                // update and insert
//                if(count($dbCustomer) < count($contacts)){
//                    $diffLen = count($contacts) - count($dbCustomer);
//                    // update
//
//                    for ($i = 0; $i < count($dbCustomer) + 1 - $diffLen; $i++ ){
//                        $newContact[$i] = CustomerContact::where('id', '=', $contacts[$i]['id'])->first();
//
//                        $newContact[$i]->PrefixThai = $contacts[$i]["prefix_thai"];
//
//                        $newContact[$i]->NameThai = $contacts[$i]["name_thai"];
//                        $newContact[$i]->SurNameThai = $contacts[$i]["surname_thai"];
//                        $newContact[$i]->PrefixEng = $contacts[$i]["prefix_eng"];
//                        $newContact[$i]->NameEng = $contacts[$i]["name_eng"];
//                        $newContact[$i]->SurNameEng = $contacts[$i]["surname_eng"];
//
//                        $newContact[$i]->NickName = $contacts[$i]["nickname"];
//                        $newContact[$i]->PositionSkey = $contacts[$i]["position"];
//                        $newContact[$i]->Telephone = $contacts[$i]["telephone"];
//                        $newContact[$i]->MobilePhone = $contacts[$i]["mobile_phone"];
//                        $newContact[$i]->Fax = $contacts[$i]["fax"];
//                        $newContact[$i]->Email = $contacts[$i]["email"];
//                        $newContact[$i]->BirthDate = $contacts[$i]["birthdate"];
//                        $newContact[$i]->ContactStatus = $contacts[$i]["contact_status"];
//                        $newContact[$i]->DateCreate = now();
//                        $newContact[$i]->UserCreate = $check_header['id'];
//                        $newContact[$i]->DateLastUpdate = now();
//                        $newContact[$i]->UserLastUpdate = $check_header['id'];
//
//                        $newContact[$i]->save();
//                    }
//                    // insert
//                    for ($i = 0; $i < $diffLen; $i++ ){
//                        $newContact[$i] = new CustomerContact();
//                        $newContact[$i]->CustomerSkey = $request->get('customer_code');
//                        $newContact[$i]->PrefixThai = $contacts[$i]["prefix_thai"];
//                        $newContact[$i]->NameThai = $contacts[$i]["name_thai"];
//                        $newContact[$i]->SurNameThai = $contacts[$i]["surname_thai"];
//                        $newContact[$i]->PrefixEng = $contacts[$i]["prefix_eng"];
//                        $newContact[$i]->NameEng = $contacts[$i]["name_eng"];
//                        $newContact[$i]->SurNameEng = $contacts[$i]["surname_eng"];
//
//                        $newContact[$i]->NickName = $contacts[$i]["nickname"];
//                        $newContact[$i]->PositionSkey = $contacts[$i]["position"];
//                        $newContact[$i]->Telephone = $contacts[$i]["telephone"];
//                        $newContact[$i]->MobilePhone = $contacts[$i]["mobile_phone"];
//                        $newContact[$i]->Fax = $contacts[$i]["fax"];
//                        $newContact[$i]->Email = $contacts[$i]["email"];
//                        $newContact[$i]->BirthDate = $contacts[$i]["birthdate"];
//                        $newContact[$i]->ContactStatus = $contacts[$i]["contact_status"];
//                        $newContact[$i]->DateCreate = now();
//                        $newContact[$i]->UserCreate = $check_header['id'];
//                        $newContact[$i]->DateLastUpdate = now();
//                        $newContact[$i]->UserLastUpdate = $check_header['id'];
//                        $newContact[$i]->save();
//                    }
//
//                }
//
//            }else{
//                // insert all data
//                foreach ($contacts as $contact){
//                    $newContact = new CustomerContact();
//                    $newContact->CustomerSkey = $request->get('customer_code');
//                    $newContact->PrefixThai = $contact["prefix_thai"];
//                    $newContact->NameThai = $contact["name_thai"];
//                    $newContact->SurNameThai = $contact["surname_thai"];
//                    $newContact->PrefixEng = $contact["prefix_eng"];
//                    $newContact->NameEng = $contact["name_eng"];
//                    $newContact->SurNameEng = $contact["surname_eng"];
//
//                    $newContact->NickName = $contact["nickname"];
//                    $newContact->PositionSkey = $contact["position"];
//                    $newContact->Telephone = $contact["telephone"];
//                    $newContact->MobilePhone = $contact["mobile_phone"];
//                    $newContact->Fax = $contact["fax"];
//                    $newContact->Email = $contact["email"];
//                    $newContact->BirthDate = $contact["birthdate"];
//                    $newContact->ContactStatus = $contact["contact_status"];
//                    $newContact->DateCreate = now();
//                    $newContact->UserCreate = $check_header['id'];
//                    $newContact->DateLastUpdate = now();
//                    $newContact->UserLastUpdate = $check_header['id'];
//                    $newContact->save();
//                }
//            }



            $customer->save();

            return response()->json([
                "status" => "success",
                "message" => "updated"
            ], 200);

        } catch (\Exception $exception)
        {
            return $exception;
        }
    }

    public function StateCustomer(Request $request){
        $allow_header = GlobalController::CheckHeader($request);
        $check_header = json_decode($allow_header->content(), true);
        if ($check_header['status'] == 'error') return $allow_header;

        try {
            $emp = Employee::where('id', '=', $check_header['id'])->first();
            $emp->StateCustomer = $request->get('data');
            if ($emp->save()) {
                return response()->json([
                    "status" => "success",
                    "message" => "update"
                ], 200);
            } else {
                return response()->json([
                    "status" => "error",
                    "message" => "Bad Request"
                ], 400);
            }
        } catch (\Exception $exception){
            return $this->ErrorDataBase();
        }
    }
    public function GetStateCustomer(Request $request){
        $allow_header = GlobalController::CheckHeader($request);
        $check_header = json_decode($allow_header->content(), true);
        if ($check_header['status'] == 'error')
        {
            return $allow_header;
        }

        try {
            $emp = Employee::where('id', '=', $check_header['id'])->first();
            return response()->json([
                "status" => "success",
                "message" => $emp->StateCustomer
            ], 200);

        } catch (\Exception $exception){
            return $this->ErrorDataBase();
        }
    }

    // sort roles
    public function SortCustomer(Request $request){
        $allow_header = GlobalController::CheckHeader($request);
        $check_header = json_decode($allow_header->content(), true);
        if ($check_header['status'] == 'error')
        {
            return $allow_header;
        }

        try {
            $sorts = $request->get("array_sort");
            $cus = DB::table('customers');
            foreach ($sorts as $sort){
                if($sort['order'] == "maxtomin"){
                    $cus = $cus->orderBy($sort['coloum_name'], 'desc');

                }else{
                    $cus = $cus->orderBy($sort['coloum_name'], 'asc');
//                    $role = $role->orderBy($sort['coloum_name'], 'asc');
                }
            }
            $cus = $cus->get();
            return response()->json([
                "status" => "success",
                "message" => $cus
            ], 200);
        }catch (\Exception $exception){
            return $exception;
        }
    }

    public function SearchCustomer(Request $request)
    {
        $allow_header = GlobalController::CheckHeader($request);
        $check_header = json_decode($allow_header->content(), true);
        if ($check_header['status'] == 'error') return $allow_header;
        try {
            $cus = DB::table('customers');

            $check = count($request->get('customer_code'));
            if ($check != 0){
                $first = $request->get("customer_code");
                if($check == 1){
                    $second = $request->get("customer_code_to");
                    $first = $first[0];
                    if($second){
                        $cus = GlobalController::SearchBetween($first, $second, 'customers.CustomerCode', $cus);
                    }
                    else{
                        if(isset($first)){
                            $cus = $cus->Where('customers.CustomerCode','LIKE','%'.$first.'%');
                        }
                    }
                }else {
                    $cus = $cus->whereIn('customers.CustomerCode', $first);
                }
            }

//            if (!empty($request->get('customer_code'))){
//                $first = $request->get("customer_code");
//                $second = $request->get("customer_code_to");
//                if($second){
//                    $cus = GlobalController::SearchBetween($first, $second, 'CustomerCode', $cus);
//                }
//                else{
//                    $cus->Where('CustomerCode','LIKE','%'.$request->get("customer_code").'%');
//                }
//            }

            if (!empty($request->get('name_thai'))){
                $first = $request->get("name_thai");
                $second = $request->get("name_thai_to");
                if($second){
                    $cus = GlobalController::SearchBetween($first, $second, 'CustomerNameThai', $cus);
                }
                else{
                    $cus->Where('CustomerNameThai','LIKE','%'.$request->get("name_thai").'%');
                }
            }

            if (!empty($request->get('name_eng'))){
                $first = $request->get("name_eng");
                $second = $request->get("name_eng_to");
                if($second){
                    $cus = GlobalController::SearchBetween($first, $second, 'CustomerNameEng', $cus);
                }
                else{
                    $cus->Where('CustomerNameEng','LIKE','%'.$request->get("name_eng").'%');
                }
            }

            if (!empty($request->get('initials_name'))){
                $cus->Where('CustomerInitialsName','LIKE','%'.$request->get("initials_name").'%');
            }

            if (!empty($request->get('business'))){
                $cus->Where('Business','LIKE','%'.$request->get("business").'%');
            }

            if (!empty($request->get('address'))){
                $cus->Where('Address','LIKE','%'.$request->get("address").'%');
            }
            if (!empty($request->get('country_skey'))){
                $cus->Where('CountrySkey','=',$request->get("country_skey"));
            }
            if (!empty($request->get('province_skey'))){
                $cus->Where('ProvinceSkey','=',$request->get("province_skey"));
            }
            if (!empty($request->get('district_skey'))){
                $cus->Where('DistrictSkey','=',$request->get("district_skey"));
            }
            if (!empty($request->get('sub_district_skey'))){
                $cus->Where('SubDistrictSkey','=',$request->get("sub_district_skey"));
            }
            if (!empty($request->get('customer_status'))){
                $cus->Where('CustomerStatus','=',$request->get("customer_status"));
            }

            if (!empty($request->get('create_date'))){
                $first = $request->get("create_date");
                $second = $request->get("create_date_to");
                if($second){
                    $cus = GlobalController::SearchBetween($first, $second, 'DateCreate', $cus);
                }
                else{
                    $cus->Where('DateCreate','LIKE','%'.$request->get("create_date").'%');
                }
            }



            $cus = $cus->get();
//            return $cus;$cus
            if (isset($cus[0]))
            {
                for ($i = 0; $i < count($cus); $i++){
                    $array_contact =  DB::table('customer_contacts')->where('CustomerSkey', '=', $cus[$i]->CustomerCode)->get();
                    if ($array_contact->isNotEmpty()) {
                        for ($j = 0; $j < count($array_contact); $j++){
                            array_walk_recursive($array_contact[$j],function(&$item){$item=strval($item);});
                        }
                    }

                    $array_location =  DB::table('customer_locations')->where('CustomerSkey', '=', $cus[$i]->CustomerCode)->get();
                    if ($array_location->isNotEmpty()) {
                        for ($j = 0; $j < count($array_location); $j++){
                            array_walk_recursive($array_location[$j],function(&$item){$item=strval($item);});
                        }
                    }

                    array_walk_recursive($cus[$i],function(&$item){$item=strval($item);});

                    $all_data[] = [
                        'id' => $cus[$i]->id,
                        'CustomerCode' => $cus[$i]->CustomerCode,
                    'CustomerNameThai' => $cus[$i]->CustomerNameThai,
                    'CustomerNameEng' => $cus[$i]->CustomerNameEng,
                    'CustomerInitialsName' => $cus[$i]->CustomerInitialsName,
                    'Business' => $cus[$i]->Business,
                    'Address' => $cus[$i]->Address,
                    'Country' => $cus[$i]->Country,
                    'Province' => $cus[$i]->Province,
                    'District' => $cus[$i]->District,
                    'SubDistrict' => $cus[$i]->SubDistrict,
                    'CustomerStatus' => $cus[$i]->CustomerStatus,
                    'Distance' => $cus[$i]->Distance,
                    'Latitude' => $cus[$i]->Latitude,
                    'Longitude' => $cus[$i]->Longitude,
                    'Note' => $cus[$i]->Note,
                    'BillingDate' => $cus[$i]->BillingDate,
                    'DateCreate' => $cus[$i]->DateCreate,
                    'UserCreate' => $cus[$i]->UserCreate,
                    'DateLastUpdate' => $cus[$i]->DateLastUpdate,
                    'UserLastUpdate' => $cus[$i]->UserLastUpdate,
                    'array_contact' => $array_contact,
                    'array_location' => $array_location,
                    ];
                }
            }

            $array = json_decode(json_encode($all_data), true);

            return $array;
        } catch (\Exception $exception)
        {
            return response()->json([
                "status" => "not found",
                "message" => "bad requests"
            ], 401);
        }
    }

//set column location กับ contact
    public function GetStateCustomerLocation(Request $request){
        $allow_header = GlobalController::CheckHeader($request);
        $check_header = json_decode($allow_header->content(), true);
        if ($check_header['status'] == 'error')
        {
            return $allow_header;
        }

        try {
            $emp = Employee::where('id', '=', $check_header['id'])->first();
            return response()->json([
                "status" => "success",
                "message" => $emp->StateCustomerLocation
            ], 200);

        } catch (\Exception $exception){
            return $this->ErrorDataBase();
        }
    }

    public function StateCustomerLocation(Request $request){
        $allow_header = GlobalController::CheckHeader($request);
        $check_header = json_decode($allow_header->content(), true);
        if ($check_header['status'] == 'error') return $allow_header;

        try {
            $emp = Employee::where('id', '=', $check_header['id'])->first();
            $emp->StateCustomerLocation = $request->get('data');
            if ($emp->save()) {
                return response()->json([
                    "status" => "success",
                    "message" => "update"
                ], 200);
            } else {
                return response()->json([
                    "status" => "error",
                    "message" => "Bad Request"
                ], 400);
            }
        } catch (\Exception $exception){
            return $this->ErrorDataBase();
        }
    }

    public function GetStateCustomerContact(Request $request){
        $allow_header = GlobalController::CheckHeader($request);
        $check_header = json_decode($allow_header->content(), true);
        if ($check_header['status'] == 'error')
        {
            return $allow_header;
        }

        try {
            $emp = Employee::where('id', '=', $check_header['id'])->first();
            return response()->json([
                "status" => "success",
                "message" => $emp->StateCustomerContact
            ], 200);

        } catch (\Exception $exception){
            return $this->ErrorDataBase();
        }
    }

    public function StateCustomerContact(Request $request){
        $allow_header = GlobalController::CheckHeader($request);
        $check_header = json_decode($allow_header->content(), true);
        if ($check_header['status'] == 'error') return $allow_header;

        try {
            $emp = Employee::where('id', '=', $check_header['id'])->first();
            $emp->StateCustomerContact = $request->get('data');
            if ($emp->save()) {
                return response()->json([
                    "status" => "success",
                    "message" => "update"
                ], 200);
            } else {
                return response()->json([
                    "status" => "error",
                    "message" => "Bad Request"
                ], 400);
            }
        } catch (\Exception $exception){
            return $this->ErrorDataBase();
        }
    }

}
