<?php

namespace App\Providers\Model;

use Illuminate\Database\Eloquent\Model;

class CREmployee extends Model
{

    protected $hidden = [
        'password', 'old_password', 'created_at', 'updated_at', 'remember_token', 'bearer_token'
    ];
}
