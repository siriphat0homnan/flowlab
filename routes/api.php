<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
// Access about login system

Route::group(['middleware' => 'api'], function () {

    //auth
    Route::post('/login', "Auth\LoginController@login");
    Route::post('/logout', "Auth\LoginController@logout");

    //send mail for forget password
    Route::post('/mail_forget_password', "API\SendMailController@SendEmailForForgotPassword");
    Route::post('/change_password', "API\EmployeeController@ChangePassword");



    // employee
    Route::post('/employees', "API\EmployeeController@CreateEmployee");
    Route::get('/employees', "API\EmployeeController@GetEmployee");
    Route::post('/update_employees', "API\EmployeeController@UpdateEmployee");
//    Route::post('/change_password', "API\AccessController@ChangePassword");
    Route::post('/search_employees', "API\EmployeeController@SearchEmployee");
    Route::get('/employee_types', "API\EmployeeController@GetEmployeeType");
    Route::get('/sections', "API\EmployeeController@GetSection");
    Route::get('/departments', "API\EmployeeController@GetDepartment");
    Route::get('/positions', "API\EmployeeController@GetPosition");
    Route::get('/sub_districts', "API\EmployeeController@GetSubDistrict");
    Route::get('/districts', "API\EmployeeController@GetDistrict");
    Route::get('/provinces', "API\EmployeeController@GetProvince");
    Route::get('/countries', "API\EmployeeController@GetCountry");
    Route::get('/material_types', "API\EmployeeController@GetMaterialType");
    Route::post('/sort_emp', "API\EmployeeController@SortEmployee");


    Route::put('/receive_state_employee_child', "API\EmployeeController@StateEmployeeChild");
    Route::put('/receive_state_employee_attach', "API\EmployeeController@StateEmployeeAttach");
    Route::put('/receive_state_employee', "API\EmployeeController@StateEmployee");
    Route::get('/receive_state_employee', "API\EmployeeController@GetStateEmployee");
    Route::get('/receive_state_employee_child', "API\EmployeeController@GetStateEmployeeChild");
    Route::get('/receive_state_employee_attach', "API\EmployeeController@GetStateEmployeeAttach");

    // role
    Route::get('/roles', "API\RoleController@GetRole");
    Route::post('/search_roles', "API\RoleController@SearchRole");
    Route::get('/cr_roles', "API\RoleController@GetCRRole");
    Route::post('/roles', "API\RoleController@CreateCRRole");
    Route::post('/test_roles', "API\RoleController@CreateRole");

    Route::put('/roles', "API\RoleController@UpdateCRRole");
    Route::put('/receive_state', "API\RoleController@StateRoles");
    Route::get('/receive_state', "API\RoleController@GetStateRoles");
    Route::get('/all_roles', "API\RoleController@GetAllRoles");
    Route::post('/sort_roles', "API\RoleController@SortRoles");
    Route::post('/role_screen', "API\RoleController@CreateRolesScreen");
    Route::get('/role_auth', "API\RoleController@GetAllRolesAuth");
//    Route::put('/role_auth', "API\RoleController@UpdateRolesAuth");
    Route::get('/role_screen', "API\RoleController@GetAllRolesScreen");

    // customer
    Route::post('/customer', "API\CustomerController@CreateCustomer");
    Route::get('/customer', "API\CustomerController@GetCustomer");
    Route::put('/customer', "API\CustomerController@UpdateCustomer");
    Route::post('/sort_customer', "API\CustomerController@SortCustomer");
    Route::post('/search_customer', "API\CustomerController@SearchCustomer");
    Route::put('/receive_state_customer', "API\CustomerController@StateCustomer");
    Route::get('/receive_state_customer', "API\CustomerController@GetStateCustomer");
    Route::put('/receive_state_customer_location', "API\CustomerController@StateCustomerLocation");
    Route::get('/receive_state_customer_location', "API\CustomerController@GetStateCustomerLocation");
    Route::put('/receive_state_customer_contact', "API\CustomerController@StateCustomerContact");
    Route::get('/receive_state_customer_contact', "API\CustomerController@GetStateCustomerContact");

    // emp role
    Route::get('/emp_not_role', "API\EmployeeRoleController@GetEmployeeNotRole");
    Route::get('/emp_role', "API\EmployeeRoleController@GetEmployeeRole");
    Route::put('/emp_role', "API\EmployeeRoleController@SetEmployeeRole");
    Route::post('/search_emp_role', "API\EmployeeRoleController@SearchEmployeeRole");

    // approve
    Route::get('/approve', "API\ApproveController@GetApprove");
    Route::post('/confirm_approve', "API\ApproveController@ConfirmApprove");
    Route::post('/sort_approve', "API\ApproveController@SortApprove");
    Route::put('/receive_state_approve', "API\ApproveController@StateApprove");
    Route::get('/receive_state_approve', "API\ApproveController@GetStateApprove");
    Route::get('/approve_of_user', "API\ApproveController@GetApproveOfUser");
    Route::post('/reject_approve', "API\ApproveController@RejectApprove");
    Route::post('/return_approve', "API\ApproveController@ReturnApprove");
    Route::put('/return_role_approve', "API\ApproveController@ReturnRoleApprove");
    Route::post('/sort_approve_of_user', "API\ApproveController@SortApproveOfUser");
    Route::put('/receive_state_approve_of_user', "API\ApproveController@StateApproveOfUser");
    Route::get('/receive_state_approve_of_user', "API\ApproveController@GetStateApproveOfUser");
    Route::post('/return_employee_approve', "API\ApproveController@ReturnEmployeeApprove");
    Route::put('/return_emp_role_approve', "API\ApproveController@ReturnEmployeeRoleApprove");
    Route::put('/return_customer_approve', "API\ApproveController@ReturnCustomerApprove");




    // safety
    Route::get('/training_rules', "API\SafetyController@GetTrainingRule");
    Route::post('/search_training_rules', "API\SafetyController@SearchTrainingRule");

});
