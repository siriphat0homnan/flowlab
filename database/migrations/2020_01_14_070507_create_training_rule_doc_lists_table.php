<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTrainingRuleDocListsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('training_rule_doc_lists', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->Integer('TrainingRuleSkey')->nullable();
            $table->Integer('DocNo')->nullable();
            $table->Integer('EmpAttachedListSkey')->nullable();
            $table->Text('DocName')->nullable();
            $table->Datetime('DocQuantity')->nullable();
            $table->Text('TrainingRuleDocStatus')->nullable();
            $table->Datetime('DateCreate')->nullable();
            $table->Integer('UserCreate')->nullable();
            $table->Datetime('LastUpdateDate')->nullable();
            $table->Integer('LastUpdateUser')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('training_rule_doc_lists');
    }
}
