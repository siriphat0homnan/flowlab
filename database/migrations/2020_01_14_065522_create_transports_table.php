<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transports', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->Text('LicensePlateNo')->nullable();
            $table->Integer('BrandSkey')->nullable();
            $table->Text('Model')->nullable();
            $table->Text('ContractAgent')->nullable();
            $table->Decimal('ContractPeriod')->nullable();
            $table->Text('Objective')->nullable();
            $table->Datetime('RentalStart')->nullable();
            $table->Datetime('RentalExpire')->nullable();
            $table->Decimal('RentalPrice')->nullable();
            $table->Text('DoorType')->nullable();
            $table->Text('GearType')->nullable();
            $table->Text('TransportType')->nullable();
            $table->Text('Colour')->nullable();
            $table->Text('CarryBoyRoof')->nullable();
            $table->Text('FlowLabSticker')->nullable();
            $table->Text('GPS')->nullable();
            $table->Text('Camera')->nullable();
            $table->Text('ContactName')->nullable();
            $table->Text('ContactPhone')->nullable();
            $table->Text('ServicePhone')->nullable();
            $table->Datetime('InsuranceExpire')->nullable();
            $table->Datetime('VehicleActExpire')->nullable();
            $table->Datetime('LicenseExpire')->nullable();
            $table->Datetime('NextMaintenance')->nullable();
            $table->Text('PIC')->nullable();
            $table->Text('Note')->nullable();
            $table->Datetime('DateCreate')->nullable();
            $table->Integer('UserCreate')->nullable();
            $table->Datetime('DateLastUpdate')->nullable();
            $table->Integer('UserLastUpdate')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transports');
    }
}
