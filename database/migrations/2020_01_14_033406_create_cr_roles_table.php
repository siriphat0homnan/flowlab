<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCrRolesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cr_roles', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->Text('ApproveKey')->nullable();
            $table->Text('RoleCode')->nullable();
            $table->Text('RoleName')->nullable();
            $table->Text('RoleStatus')->nullable();
            $table->Text('YellowName')->nullable();
            $table->Text('YellowStatus')->nullable();
            $table->Datetime('DateCreate')->nullable();
            $table->Integer('UserCreate')->nullable();
            $table->Datetime('LastUpdateDate')->nullable();
            $table->Integer('LastUpdateUser')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cr_roles');
    }
}
