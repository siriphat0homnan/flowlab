<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTrainingRulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('training_rules', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->Text('TrainingRuleCode')->nullable();
            $table->Text('TrainingRuleName')->nullable();
            $table->Text('TrainingRuleDescription')->nullable();
            $table->Text('TrainingRuleType')->nullable();
            $table->Integer('CustomerSkey')->nullable();
            $table->Integer('LocationSkey')->nullable();
            $table->Text('ContactPersonName1')->nullable();
            $table->Text('ContactPersonPhone1')->nullable();
            $table->Text('ContactPersonName2')->nullable();
            $table->Text('ContactPersonPhone2')->nullable();
            $table->Text('ContactPersonName3')->nullable();
            $table->Text('ContactPersonPhone3')->nullable();
            $table->Text('ContactPersonName4')->nullable();
            $table->Text('ContactPersonPhone4')->nullable();
            $table->Text('TrainingRoomContactName')->nullable();
            $table->Text('TrainingRoomContactPhone')->nullable();
            $table->Text('AvailableOnMonday')->nullable();
            $table->Time('AvailableOnMondayTime1')->nullable();
            $table->Time('AvailableOnMondayTime2')->nullable();
            $table->Text('AvailableOnTuesday')->nullable();
            $table->Time('AvailableOnTuesdayTime1')->nullable();
            $table->Time('AvailableOnTuesdayTime2')->nullable();
            $table->Text('AvailableOnWednesday')->nullable();
            $table->Time('AvailableOnWednesdayTime1')->nullable();
            $table->Time('AvailableOnWednesdayTime2')->nullable();
            $table->Text('AvailableOnThursday')->nullable();
            $table->Time('AvailableOnThursdayTime1')->nullable();
            $table->Time('AvailableOnThursdayTime2')->nullable();
            $table->Text('AvailableOnFriday')->nullable();
            $table->Time('AvailableOnFridayTime1')->nullable();
            $table->Time('AvailableOnFridayTime2')->nullable();
            $table->Text('AvailableOnSaturday')->nullable();
            $table->Time('AvailableOnSaturdayTime1')->nullable();
            $table->Time('AvailableOnSaturdayTime2')->nullable();
            $table->Text('AvailableOnSunday')->nullable();
            $table->Time('AvailableOnSundayTime1')->nullable();
            $table->Time('AvailableOnSundayTime2')->nullable();
            $table->Integer('ExpireDay')->nullable();
            $table->Text('ExpireEndOfYear')->nullable();
            $table->Text('SpecialInstruction')->nullable();
            $table->Text('TrainingRuleStatus')->nullable();
            $table->Datetime('DateCreate')->nullable();
            $table->Integer('UserCreate')->nullable();
            $table->Datetime('LastUpdateDate')->nullable();
            $table->Integer('LastUpdateUser')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('training_rules');
    }
}
