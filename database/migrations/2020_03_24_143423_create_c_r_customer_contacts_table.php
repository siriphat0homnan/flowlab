<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCRCustomerContactsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('c_r_customer_contacts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->Text('ApproveKey')->nullable();

            $table->Text('CustomerSkey')->nullable();
            $table->Text('PrefixThai')->nullable();
            $table->Text('NameThai')->nullable();
            $table->Text('SurNameThai')->nullable();
            $table->Text('PrefixEng')->nullable();
            $table->Text('NameEng')->nullable();
            $table->Text('SurNameEng')->nullable();
            $table->Text('NickName')->nullable();
            $table->Integer('PositionSkey')->nullable();
            $table->Text('Telephone')->nullable();
            $table->Text('MobilePhone')->nullable();
            $table->Text('Fax')->nullable();
            $table->Text('Email')->nullable();
            $table->Datetime('BirthDate')->nullable();
            $table->Text('ContactStatus')->nullable();
            $table->Datetime('DateCreate')->nullable();
            $table->Integer('UserCreate')->nullable();
            $table->Datetime('DateLastUpdate')->nullable();
            $table->Integer('UserLastUpdate')->nullable();
            $table->Text('Yellow')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('c_r_customer_contacts');
    }
}
