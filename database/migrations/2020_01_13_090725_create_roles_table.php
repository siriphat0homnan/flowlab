<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRolesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('roles', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->Text('RoleCode')->nullable();
            $table->Text('RoleName')->nullable();
            $table->Text('RoleStatus')->nullable();
            $table->Datetime('DateCreate')->nullable();
            $table->Integer('UserCreate')->nullable();
            $table->Datetime('LastUpdateDate')->nullable();
            $table->Integer('LastUpdateUser')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('roles');
    }
}
