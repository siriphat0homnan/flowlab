<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmployeeAttachedListsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employee_attached_lists', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->Integer('EmpAttachedListNo')->nullable();
            $table->Text('EmpAttachedListName')->nullable();
            $table->Datetime('DateCreate')->nullable();
            $table->Integer('UserCreate')->nullable();
            $table->Datetime('DateLastUpdate')->nullable();
            $table->Integer('UserLastUpdate')->nullable();
            $table->Datetime('HealthCheckResultsBeforeWork')->nullable();
            $table->Datetime('AnnualHealthExaminationResults')->nullable();
            $table->Datetime('HealthCheckResultsForWorkOnSite')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employee_attached_lists');
    }
}
