<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmployeeAttachedFilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employee_attached_files', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->Text('Status')->nullable();
            $table->Text('EmpAttachedListNo')->nullable();
            $table->Text('AttachedPath')->nullable();
            $table->Text('FileName')->nullable();
            $table->Datetime('DateCreate')->nullable();
            $table->Integer('UserCreate')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employee_attached_files');
    }
}
