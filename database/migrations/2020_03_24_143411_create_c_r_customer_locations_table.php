<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCRCustomerLocationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('c_r_customer_locations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->Text('ApproveKey')->nullable();
            $table->Text('CustomerSkey')->nullable();

            $table->Text('LocationNameThai')->nullable();
            $table->Text('LocationNameEng')->nullable();
            $table->Text('LocationInitialsName')->nullable();
            $table->Text('Address')->nullable();
            $table->Text('Country')->nullable();
            $table->Text('Province')->nullable();
            $table->Text('District')->nullable();
            $table->Text('SubDistrict')->nullable();
            $table->Text('LocationStatus')->nullable();
            $table->Decimal('Distance')->nullable();
            $table->Decimal('Latitude')->nullable();
            $table->Decimal('Longitude')->nullable();
            $table->Datetime('DateCreate')->nullable();
            $table->Integer('UserCreate')->nullable();
            $table->Datetime('DateLastUpdate')->nullable();
            $table->Integer('UserLastUpdate')->nullable();
            $table->Text('Yellow')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('c_r_customer_locations');
    }
}
