<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRoleAuthenticationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('role_authentications', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->Text('RoleSkey')->nullable();
            $table->Text('ScreenSkey')->nullable();
            $table->Text('PermissionType')->nullable();
            $table->Text('DateCreate')->nullable();
            $table->integer('UserCreate')->nullable();
            $table->Text('DateUpdate')->nullable();
            $table->integer('UserUpdate')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('role_authentications');
    }
}
