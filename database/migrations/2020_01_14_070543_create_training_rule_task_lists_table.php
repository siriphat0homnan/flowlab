<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTrainingRuleTaskListsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('training_rule_task_lists', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->Integer('TrainingRuleSkey')->nullable();
            $table->Integer('TaskNo')->nullable();
            $table->Text('TaskName')->nullable();
            $table->Text('TaskStatus')->nullable();
            $table->Datetime('DateCreate')->nullable();
            $table->Integer('UserCreate')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('training_rule_task_lists');
    }
}
