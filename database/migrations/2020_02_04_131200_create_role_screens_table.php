<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRoleScreensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('role_screens', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('ScreenNo')->nullable();
            $table->Text('ScreenName')->nullable();
            $table->Text('ScreenStatus')->nullable();
            $table->Text('DateCreate')->nullable();
            $table->integer('UserCreate')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('role_screens');
    }
}
