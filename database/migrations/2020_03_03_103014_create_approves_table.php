<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateApprovesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('approves', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->Text('ApproveKey')->nullable();
            $table->Text('Master')->nullable();


            $table->Text('CRType')->nullable();
            $table->Text('CRStatus')->nullable();
            $table->Datetime('CRReturnDate')->nullable();
            $table->Text('CRReturnReason')->nullable();
            $table->Datetime('CRRejectDate')->nullable();
            $table->Text('CRRejectReason')->nullable();
            $table->Integer('CRApproveUser')->nullable();
            $table->Datetime('CRApproveDate')->nullable();
            $table->Integer('ApproveUserCreate')->nullable();
            $table->Datetime('ApproveDateCreate')->nullable();



            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('approves');
    }
}
