<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCREmployeeChildrensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('c_r_employee_childrens', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->Text('ApproveKey')->nullable();

            $table->Text('Status')->nullable();
            $table->Text('LineNo')->nullable();
            $table->Text('ChildrenName')->nullable();
            $table->Text('ChildrenLastName')->nullable();
            $table->Text('Sex')->nullable();
            $table->Text('Birthdate')->nullable();
            $table->Text('IdCard')->nullable();
            $table->Text('Yellow')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('c_r_employee_childrens');
    }
}
