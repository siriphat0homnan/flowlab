<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCRRoleAuthenticationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('c_r_role_authentications', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->Text('ApproveKey')->nullable();
            $table->Text('RoleSkey')->nullable();
            $table->Text('ScreenSkey')->nullable();
            $table->Text('PermissionType')->nullable();
            $table->Text('Yellow')->nullable();
            $table->Text('DateCreate')->nullable();
            $table->integer('UserCreate')->nullable();
            $table->Text('DateUpdate')->nullable();
            $table->integer('UserUpdate')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('c_r_role_authentications');
    }
}
