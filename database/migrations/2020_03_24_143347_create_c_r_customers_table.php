<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCRCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('c_r_customers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->Text('ApproveKey')->nullable();
            $table->Text('CustomerCode')->nullable();
            $table->Text('CustomerNameThai')->nullable();
            $table->Text('CustomerNameEng')->nullable();
            $table->Text('CustomerInitialsName')->nullable();
            $table->Text('Business')->nullable();
            $table->Text('Address')->nullable();
            $table->Text('Country')->nullable();
            $table->Text('Province')->nullable();
            $table->Text('District')->nullable();
            $table->Text('SubDistrict')->nullable();
            $table->Text('CustomerStatus')->nullable();
            $table->Decimal('Distance')->nullable();
            $table->Decimal('Latitude')->nullable();
            $table->Decimal('Longitude')->nullable();
            $table->Text('Note')->nullable();
            $table->Datetime('BillingDate')->nullable();
            $table->Datetime('DateCreate')->nullable();
            $table->Integer('UserCreate')->nullable();
            $table->Datetime('DateLastUpdate')->nullable();
            $table->Integer('UserLastUpdate')->nullable();
            $table->Text('Yellow')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('c_r_customers');
    }
}
