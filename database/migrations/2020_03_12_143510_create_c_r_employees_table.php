<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCREmployeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('c_r_employees', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->Text('ApproveKey')->nullable();
            $table->Text('EmployeeStatus')->nullable();

            $table->Text('EmployeeNo')->nullable();
            $table->Text('IDCard')->nullable();
            $table->Text('MotorcycleDrivingLicenseID')->nullable();
            $table->Text('CarDrivingLicenseID')->nullable();
            $table->Text('TruckDrivingLicenseID')->nullable();
            $table->Text('PrefixThai')->nullable();
            $table->Text('NameThai')->nullable();
            $table->Text('SurNameThai')->nullable();
            $table->Text('PrefixEng')->nullable();
            $table->Text('NameEng')->nullable();
            $table->Text('SurNameEng')->nullable();
            $table->Text('NickName')->nullable();

            $table->Integer('EmployeeTypeID')->nullable();
            $table->Integer('PositionID')->nullable();
            $table->Integer('SectionID')->nullable();
            $table->Integer('DepartmentID')->nullable();

            $table->Text('MobilePhone')->nullable();
            $table->Text('email')->nullable();
            $table->Datetime('BirthDate')->nullable();
            $table->Datetime('StartWorkingDate')->nullable();
            $table->Datetime('ResignDate')->nullable();
            $table->Text('SocialSecurityID')->nullable();
            $table->Text('password')->nullable();
            $table->Integer('CustomerIDRef')->nullable();
            $table->Text('Note')->nullable();
            $table->Datetime('IDCardExpireDate')->nullable();
            $table->Datetime('MortocycleDrivingLicenseExpireDate')->nullable();
            $table->Datetime('CarDrivingLicenseExpireDate')->nullable();
            $table->Datetime('TruckDrivingLicenseExpireDate')->nullable();
            $table->Text('MilitaryStatus')->nullable();
            $table->Text('MilitaryStatusExtension')->nullable();
            $table->Text('Nationality')->nullable();
            $table->Text('Origin')->nullable();
            $table->Text('Religious')->nullable();
            $table->Text('MaritalStatus')->nullable();
            $table->Text('NativeHabitat')->nullable();
            $table->Text('Address')->nullable();
            $table->Text('PostCode')->nullable();
            $table->Text('Country')->nullable();
            $table->Text('Province')->nullable();
            $table->Text('District')->nullable();
            $table->Text('SubDistrict')->nullable();
            $table->Text('FacebookID')->nullable();
            $table->Text('LineID')->nullable();
            $table->Text('Reference')->nullable();
            $table->Text('ReferecncePhone')->nullable();
            $table->Text('ReferenceRelationship')->nullable();
            $table->Text('FatherName')->nullable();
            $table->Text('FatherLastName')->nullable();
            $table->Text('MotherName')->nullable();
            $table->Text('MotherLastName')->nullable();
            $table->Text('BloodGroup')->nullable();
            $table->Text('MateName')->nullable();
            $table->Text('MateSurname')->nullable();
            $table->Text('MateIDCard')->nullable();
            $table->Text('MateBirthDate')->nullable();
            $table->Text('FirstEducationalBackground')->nullable();
            $table->Text('FirstEducationalBackgroundFaculty')->nullable();
            $table->Text('FirstEducationalPlace')->nullable();
            $table->Text('FirstGraduateYear')->nullable();
            $table->Text('CurrentEducationalBackground')->nullable();
            $table->Text('CurrentEducationalBackgroundFaculty')->nullable();
            $table->Text('CurrentEducationalPlace')->nullable();
            $table->Text('CurrentGraduateYear')->nullable();
            $table->Integer('ExternalExperienceYear')->nullable();
            $table->Integer('ExternalExperienceMonth')->nullable();
            $table->Text('UnderlyingDisease')->nullable();
            $table->Decimal('Height')->nullable();
            $table->Decimal('Weight')->nullable();
            $table->Text('BonusPerYear')->nullable();
            $table->Text('RatePerYear')->nullable();
            $table->Text('AdjustPerYear')->nullable();
            $table->Datetime('LastSalaryAdjust')->nullable();
            $table->Datetime('LastPotentialAdjust')->nullable();
            $table->integer('ExternalExp')->nullable();
            $table->Text('Bonus')->nullable();

            $table->Text('Yellow')->nullable();
            $table->Datetime('DateCreate')->nullable();
            $table->Integer('UserCreate')->nullable();
            $table->Datetime('DateLastUpdate')->nullable();
            $table->Integer('UserLastUpdate')->nullable();

            $table->Text('RoleCodeSkey')->nullable();
            $table->Text('StateRoles')->nullable();
            $table->Text('StateEmployee')->nullable();
            $table->Text('StateEmployeeChild')->nullable();
            $table->Text('StateEmployeeAttach')->nullable();
            $table->Text('StateCustomer')->nullable();
            $table->Text('StateCustomerContact')->nullable();
            $table->Text('StateCustomerLocation')->nullable();
            $table->Text('StateApprove')->nullable();
            $table->Text('StateApproveOfUser')->nullable();


            $table->Text('old_password')->nullable();
            $table->Text('remember_token')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('c_r_employees');
    }
}
