<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMaterialsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('materials', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->Text('MaterialCode')->nullable();
            $table->Text('MaterialName')->nullable();
            $table->Text('Specification')->nullable();
            $table->Integer('MaterialTypeID')->nullable();
            $table->Integer('MaterialGroupID')->nullable();
            $table->Text('CertificateNo')->nullable();
            $table->Text('CertificateStatus')->nullable();
            $table->Datetime('NextCertificateDate')->nullable();
            $table->Text('Location')->nullable();
            $table->Integer('UOMSkey')->nullable();
            $table->Text('MaterialStatus')->nullable();
            $table->Decimal('StandardCost')->nullable();
            $table->Decimal('AverageCost')->nullable();
            $table->Integer('SafetyStock')->nullable();
            $table->Decimal('Weight')->nullable();
            $table->Text('LotControlFlag')->nullable();
            $table->Text('SerialControlFlag')->nullable();
            $table->Datetime('DateCreate')->nullable();
            $table->Integer('UserCreate')->nullable();
            $table->Datetime('DateLastUpdate')->nullable();
            $table->Integer('UserLastUpdate')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('materials');
    }
}
