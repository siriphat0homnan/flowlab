<?php

use Illuminate\Database\Seeder;
use App\Providers\Model\Employee;
use \App\Providers\Model\Country;
use App\Providers\Model\Role;
use App\Providers\Model\Department;
use App\Providers\Model\Section;
use App\Providers\Model\Position;
use App\Providers\Model\EmployeeType;
use App\Providers\Model\RoleScreen;
use \App\Providers\Model\Customer;
use \App\Providers\Model\MaterialType;
use \App\Providers\Model\District;
use \App\Providers\Model\SubDistrict;
use \App\Providers\Model\Province;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */

    public function run()
    {
        DatabaseSeeder::employee();
//        DatabaseSeeder::role();
        DatabaseSeeder::department();
        DatabaseSeeder::position();
        DatabaseSeeder::section();
        DatabaseSeeder::roleScreen();
//        DatabaseSeeder::customer();
        DatabaseSeeder::employee_type();
        DatabaseSeeder::material_types();
//        DatabaseSeeder::provinces();
//        DatabaseSeeder::country();
//        DatabaseSeeder::district();
//        DatabaseSeeder::sub_district();



    }
    public function country(){
        $countries = array("Afghanistan", "Albania", "Algeria", "American Samoa", "Andorra", "Angola", "Anguilla", "Antarctica", "Antigua and Barbuda", "Argentina", "Armenia", "Aruba", "Australia", "Austria", "Azerbaijan", "Bahamas", "Bahrain", "Bangladesh", "Barbados", "Belarus", "Belgium", "Belize", "Benin", "Bermuda", "Bhutan", "Bolivia", "Bosnia and Herzegowina", "Botswana", "Bouvet Island", "Brazil", "British Indian Ocean Territory", "Brunei Darussalam", "Bulgaria", "Burkina Faso", "Burundi", "Cambodia", "Cameroon", "Canada", "Cape Verde", "Cayman Islands", "Central African Republic", "Chad", "Chile", "China", "Christmas Island", "Cocos (Keeling) Islands", "Colombia", "Comoros", "Congo", "Congo, the Democratic Republic of the", "Cook Islands", "Costa Rica", "Cote d'Ivoire", "Croatia (Hrvatska)", "Cuba", "Cyprus", "Czech Republic", "Denmark", "Djibouti", "Dominica", "Dominican Republic", "East Timor", "Ecuador", "Egypt", "El Salvador", "Equatorial Guinea", "Eritrea", "Estonia", "Ethiopia", "Falkland Islands (Malvinas)", "Faroe Islands", "Fiji", "Finland", "France", "France Metropolitan", "French Guiana", "French Polynesia", "French Southern Territories", "Gabon", "Gambia", "Georgia", "Germany", "Ghana", "Gibraltar", "Greece", "Greenland", "Grenada", "Guadeloupe", "Guam", "Guatemala", "Guinea", "Guinea-Bissau", "Guyana", "Haiti", "Heard and Mc Donald Islands", "Holy See (Vatican City State)", "Honduras", "Hong Kong", "Hungary", "Iceland", "India", "Indonesia", "Iran (Islamic Republic of)", "Iraq", "Ireland", "Israel", "Italy", "Jamaica", "Japan", "Jordan", "Kazakhstan", "Kenya", "Kiribati", "Korea, Democratic People's Republic of", "Korea, Republic of", "Kuwait", "Kyrgyzstan", "Lao, People's Democratic Republic", "Latvia", "Lebanon", "Lesotho", "Liberia", "Libyan Arab Jamahiriya", "Liechtenstein", "Lithuania", "Luxembourg", "Macau", "Macedonia, The Former Yugoslav Republic of", "Madagascar", "Malawi", "Malaysia", "Maldives", "Mali", "Malta", "Marshall Islands", "Martinique", "Mauritania", "Mauritius", "Mayotte", "Mexico", "Micronesia, Federated States of", "Moldova, Republic of", "Monaco", "Mongolia", "Montserrat", "Morocco", "Mozambique", "Myanmar", "Namibia", "Nauru", "Nepal", "Netherlands", "Netherlands Antilles", "New Caledonia", "New Zealand", "Nicaragua", "Niger", "Nigeria", "Niue", "Norfolk Island", "Northern Mariana Islands", "Norway", "Oman", "Pakistan", "Palau", "Panama", "Papua New Guinea", "Paraguay", "Peru", "Philippines", "Pitcairn", "Poland", "Portugal", "Puerto Rico", "Qatar", "Reunion", "Romania", "Russian Federation", "Rwanda", "Saint Kitts and Nevis", "Saint Lucia", "Saint Vincent and the Grenadines", "Samoa", "San Marino", "Sao Tome and Principe", "Saudi Arabia", "Senegal", "Seychelles", "Sierra Leone", "Singapore", "Slovakia (Slovak Republic)", "Slovenia", "Solomon Islands", "Somalia", "South Africa", "South Georgia and the South Sandwich Islands", "Spain", "Sri Lanka", "St. Helena", "St. Pierre and Miquelon", "Sudan", "Suriname", "Svalbard and Jan Mayen Islands", "Swaziland", "Sweden", "Switzerland", "Syrian Arab Republic", "Taiwan, Province of China", "Tajikistan", "Tanzania, United Republic of", "Thailand", "Togo", "Tokelau", "Tonga", "Trinidad and Tobago", "Tunisia", "Turkey", "Turkmenistan", "Turks and Caicos Islands", "Tuvalu", "Uganda", "Ukraine", "United Arab Emirates", "United Kingdom", "United States", "United States Minor Outlying Islands", "Uruguay", "Uzbekistan", "Vanuatu", "Venezuela", "Vietnam", "Virgin Islands (British)", "Virgin Islands (U.S.)", "Wallis and Futuna Islands", "Western Sahara", "Yemen", "Yugoslavia", "Zambia", "Zimbabwe");
        for($i=1; $i<=count($countries); $i++):
            DB::table('countries')
                ->insert([
                    'id' => $i,
                    'CountryCode' => $i,
                    'CountryNameThai' => '',
                    'CountryNameEng' => $countries[$i - 1],
                    'UserCreate' => 1,
                    'DateCreate' => now(),
                    ]);

        endfor;

    }
    public function district(){
        factory(District::class)->create([ //admin account
            'id' => 1,
            'ProvinceSkey' => 1,
            'DistrictCode' => 1,
            'DistrictNameThai' => 'สุดจัด',
            'DistrictNameEng' => 'Tim',
            'UserCreate' => 1,
            'DateCreate' => now(),
        ]);
        factory(District::class)->create([ //admin account
            'id' => 2,
            'ProvinceSkey' => 2,
            'DistrictCode' => 1,
            'DistrictNameThai' => 'สุดจัดดดด',
            'DistrictNameEng' => 'Timmmm',
            'UserCreate' => 1,
            'DateCreate' => now(),
        ]);
    }
    public function sub_district(){
        factory(SubDistrict::class)->create([ //admin account
            'id' => 1,
            'DistrictSkey' => 1,
            'SubDistrictCode' => 1,
            'SubDistrictNameThai' => 'สุดจัด',
            'SubDistrictNameEng' => 'Tim',
            'UserCreate' => 1,
            'DateCreate' => now(),
        ]);
        factory(SubDistrict::class)->create([ //admin account
            'id' => 2,
            'DistrictSkey' => 2,
            'SubDistrictCode' => 1,
            'SubDistrictNameThai' => 'สุดจัดดดด',
            'SubDistrictNameEng' => 'Timmmm',
            'UserCreate' => 1,
            'DateCreate' => now(),
        ]);
    }
    public function provinces(){
        $provinces = [
            ['id' => '1', '10', 'กรุงเทพมหานคร', 'Bangkok', '2'],
            ['id' => '2', '11', 'สมุทรปราการ', 'Samut Prakan', '2'],
            ['id' => '3', '12', 'นนทบุรี', 'Nonthaburi', '2'],
            ['id' => '4', '13', 'ปทุมธานี', 'Pathum Thani', '2'],
            ['id' => '5', '14', 'พระนครศรีอยุธยา', 'Phra Nakhon Si Ayutthaya', '2'],
            ['id' => '6', '15', 'อ่างทอง', 'Ang Thong', '2'],
            ['id' => '7', '16', 'ลพบุรี', 'Loburi', '2'],
            ['id' => '8', '17', 'สิงห์บุรี', 'Sing Buri', '2'],
            ['id' => '9', '18', 'ชัยนาท', 'Chai Nat', '2'],
            ['id' => '10', '19', 'สระบุรี', 'Saraburi', '2'],
            ['id' => '11', '20', 'ชลบุรี', 'Chon Buri', '5'],
            ['id' => '12', '21', 'ระยอง', 'Rayong', '5'],
            ['id' => '13', '22', 'จันทบุรี', 'Chanthaburi', '5'],
            ['id' => '14', '23', 'ตราด', 'Trat', '5'],
            ['id' => '15', '24', 'ฉะเชิงเทรา', 'Chachoengsao', '5'],
            ['id' => '16', '25', 'ปราจีนบุรี', 'Prachin Buri', '5'],
            ['id' => '17', '26', 'นครนายก', 'Nakhon Nayok', '2'],
            ['id' => '18', '27', 'สระแก้ว', 'Sa Kaeo', '5'],
            ['id' => '19', '30', 'นครราชสีมา', 'Nakhon Ratchasima', '3'],
            ['id' => '20', '31', 'บุรีรัมย์', 'Buri Ram', '3'],
            ['id' => '21', '32', 'สุรินทร์', 'Surin', '3'],
            ['id' => '22', '33', 'ศรีสะเกษ', 'Si Sa Ket', '3'],
            ['id' => '23', '34', 'อุบลราชธานี', 'Ubon Ratchathani', '3'],
            ['id' => '24', '35', 'ยโสธร', 'Yasothon', '3'],
            ['id' => '25', '36', 'ชัยภูมิ', 'Chaiyaphum', '3'],
            ['id' => '26', '37', 'อำนาจเจริญ', 'Amnat Charoen', '3'],
            ['id' => '27', '39', 'หนองบัวลำภู', 'Nong Bua Lam Phu', '3'],
            ['id' => '28', '40', 'ขอนแก่น', 'Khon Kaen', '3'],
            ['id' => '29', '41', 'อุดรธานี', 'Udon Thani', '3'],
            ['id' => '30', '42', 'เลย', 'Loei', '3'],
            ['id' => '31', '43', 'หนองคาย', 'Nong Khai', '3'],
            ['id' => '32', '44', 'มหาสารคาม', 'Maha Sarakham', '3'],
            ['id' => '33', '45', 'ร้อยเอ็ด', 'Roi Et', '3'],
            ['id' => '34', '46', 'กาฬสินธุ์', 'Kalasin', '3'],
            ['id' => '35', '47', 'สกลนคร', 'Sakon Nakhon', '3'],
            ['id' => '36', '48', 'นครพนม', 'Nakhon Phanom', '3'],
            ['id' => '37', '49', 'มุกดาหาร', 'Mukdahan', '3'],
            ['id' => '38', '50', 'เชียงใหม่', 'Chiang Mai', '1'],
            ['id' => '39', '51', 'ลำพูน', 'Lamphun', '1'],
            ['id' => '40', '52', 'ลำปาง', 'Lampang', '1'],
            ['id' => '41', '53', 'อุตรดิตถ์', 'Uttaradit', '1'],
            ['id' => '42', '54', 'แพร่', 'Phrae', '1'],
            ['id' => '43', '55', 'น่าน', 'Nan', '1'],
            ['id' => '44', '56', 'พะเยา', 'Phayao', '1'],
            ['id' => '45', '57', 'เชียงราย', 'Chiang Rai', '1'],
            ['id' => '46', '58', 'แม่ฮ่องสอน', 'Mae Hong Son', '1'],
            ['id' => '47', '60', 'นครสวรรค์', 'Nakhon Sawan', '2'],
            ['id' => '48', '61', 'อุทัยธานี', 'Uthai Thani', '2'],
            ['id' => '49', '62', 'กำแพงเพชร', 'Kamphaeng Phet', '2'],
            ['id' => '50', '63', 'ตาก', 'Tak', '4'],
            ['id' => '51', '64', 'สุโขทัย', 'Sukhothai', '2'],
            ['id' => '52', '65', 'พิษณุโลก', 'Phitsanulok', '2'],
            ['id' => '53', '66', 'พิจิตร', 'Phichit', '2'],
            ['id' => '54', '67', 'เพชรบูรณ์', 'Phetchabun', '2'],
            ['id' => '55', '70', 'ราชบุรี', 'Ratchaburi', '4'],
            ['id' => '56', '71', 'กาญจนบุรี', 'Kanchanaburi', '4'],
            ['id' => '57', '72', 'สุพรรณบุรี', 'Suphan Buri', '2'],
            ['id' => '58', '73', 'นครปฐม', 'Nakhon Pathom', '2'],
            ['id' => '59', '74', 'สมุทรสาคร', 'Samut Sakhon', '2'],
            ['id' => '60', '75', 'สมุทรสงคราม', 'Samut Songkhram', '2'],
            ['id' => '61', '76', 'เพชรบุรี', 'Phetchaburi', '4'],
            ['id' => '62', '77', 'ประจวบคีรีขันธ์', 'Prachuap Khiri Khan', '4'],
            ['id' => '63', '80', 'นครศรีธรรมราช', 'Nakhon Si Thammarat', '6'],
            ['id' => '64', '81', 'กระบี่', 'Krabi', '6'],
            ['id' => '65', '82', 'พังงา', 'Phangnga', '6'],
            ['id' => '66', '83', 'ภูเก็ต', 'Phuket', '6'],
            ['id' => '67', '84', 'สุราษฎร์ธานี', 'SuratThani', '6'],
            ['id' => '68', '85', 'ระนอง', 'Ranong', '6'],
            ['id' => '69', '86', 'ชุมพร', 'Chumphon', '6'],
            ['id' => '70', '90', 'สงขลา', 'Songkhla', '6'],
            ['id' => '71', '91', 'สตูล', 'Satun', '6'],
            ['id' => '72', '92', 'ตรัง', 'Trang', '6'],
            ['id' => '73', '93', 'พัทลุง', 'Phatthalung', '6'],
            ['id' => '74', '94', 'ปัตตานี', 'Pattani', '6'],
            ['id' => '75', '95', 'ยะลา', 'Yala', '6'],
            ['id' => '76', '96', 'นราธิวาส', 'Narathiwat', '6'],
            ['id' => '77', '97', 'บึงกาฬ', 'Bueng Kan', '3'],
        ];
        for($i=1; $i<=count($provinces); $i++):
            DB::table('provinces')
                ->insert([
                    'id' => $i,
                    'CountrySkey' => 211,
                    'ProvinceCode' => $i,
                    'ProvinceNameThai' => $provinces[$i - 1][1],
                    'ProvinceNameEng' => $provinces[$i - 1][2],
                    'UserCreate' => 1,
                    'DateCreate' => now(),
                ]);

        endfor;

//        factory(Province::class)->create([ //admin account
//            'id' => 1,
//            'CountrySkey' => 211,
//            'ProvinceCode' => 1,
//            'ProvinceNameThai' => 'สุดจัด',
//            'ProvinceNameEng' => 'Tim',
//            'UserCreate' => 1,
//            'DateCreate' => now(),
//        ]);
//        factory(Province::class)->create([ //admin account
//            'id' => 2,
//            'CountrySkey' => 2,
//            'ProvinceCode' => 1,
//            'ProvinceNameThai' => 'สุดจัดดดด',
//            'ProvinceNameEng' => 'Timmmm',
//            'UserCreate' => 1,
//            'DateCreate' => now(),
//        ]);
    }
    public function role(){
        factory(Role::class)->create([
            'id' => 1,
            'RoleCode' => 'R001',
            'RoleName' => 'Hr Admin',
            'RoleStatus' => 'Active',
            'DateCreate' => now(),
            'UserCreate' => 3,
        ]);
        factory(Role::class)->create([
            'id' => 2,
            'RoleCode' => 'R002',
            'RoleName' => 'Hr Manager',
            'RoleStatus' => 'Active',
            'DateCreate' => now(),
            'UserCreate' => 3,
        ]);
        factory(Role::class)->create([
            'id' => 3,
            'RoleCode' => 'R003',
            'RoleName' => 'Manager',
            'RoleStatus' => 'Active',
            'DateCreate' => now(),
            'UserCreate' => 3,
        ]);
        factory(Role::class)->create([
            'id' => 4,
            'RoleCode' => 'R004',
            'RoleName' => 'Admin',
            'RoleStatus' => 'Inactive',
            'DateCreate' => now(),
            'UserCreate' => 3,
        ]);
    }
    public function department(){
        factory(Department::class)->create([
            'id' => 1,
            'DepartmentCode' => 'OP',
            'DepartmentStatus' => 'Active',
            'DepartmentNameThai' => 'โอเปอร์เรชั่น',
            'DepartmentNameEng' => 'Operation',
            'DateCreate' => now(),
            'UserCreate' => 3,
        ]);
        factory(Department::class)->create([
            'id' => 2,
            'DepartmentCode' => 'TIM',
            'DepartmentStatus' => 'Active',
            'DepartmentNameThai' => 'ทิมเทพ',
            'DepartmentNameEng' => 'TIMLNW',
            'DateCreate' => now(),
            'UserCreate' => 3,
        ]);
    }
    public function position(){
        factory(Position::class)->create([
            'id' => 1,
            'PositionCode' => 'OS',
            'PositionStatus' => 'Active',
            'PositionNameThai' => 'ออนไซด์ เซอร์วิส',
            'PositionNameEng' => 'Onsite service',
            'DateCreate' => now(),
            'UserCreate' => 3,
        ]);
        factory(Position::class)->create([
            'id' => 2,
            'PositionCode' => 'TIM',
            'PositionStatus' => 'Active',
            'PositionNameThai' => 'ทิมเทพ',
            'PositionNameEng' => 'TIMLNW',
            'DateCreate' => now(),
            'UserCreate' => 3,
        ]);

    }
    public function section(){
        factory(Section::class)->create([
            'id' => 1,
            'SectionCode' => 'OS',
            'SectionStatus' => 'Active',
            'SectionNameThai' => 'ออนไซด์ เซอร์วิส',
            'SectionNameEng' => 'Onsite service',
            'DateCreate' => now(),
            'UserCreate' => 3,
        ]);
        factory(Section::class)->create([
            'id' => 2,
            'SectionCode' => 'TIM',
            'SectionStatus' => 'Active',
            'SectionNameThai' => 'ทิมเทพ',
            'SectionNameEng' => 'TIMLNW',
            'DateCreate' => now(),
            'UserCreate' => 3,
        ]);
    }
    public function employee_type(){
        factory(EmployeeType::class)->create([
            'id' => 1,
            'EmployeeTypeCode' => '1',
            'EmployeeTypeStatus' => 'Active',
            'EmployeeTypeNameThai' => 'สัญญาจ้าง',
            'EmployeeTypeNameEng' => 'Tempery',
            'DateCreate' => now(),
            'UserCreate' => 3,
        ]);
        factory(EmployeeType::class)->create([
            'id' => 2,
            'EmployeeTypeCode' => '2',
            'EmployeeTypeStatus' => 'Active',
            'EmployeeTypeNameThai' => 'ทิมเทพ',
            'EmployeeTypeNameEng' => 'TIMLNW',
            'DateCreate' => now(),
            'UserCreate' => 3,
        ]);

    }
    public function material_types(){

        factory(MaterialType::class)->create([
            'id' => 1,
            'MaterialTypeName' => 'Tim1',
            'MaterialTypeStatus' => 'Active',
            'DateCreate' => now(),
            'UserCreate' => 3,
        ]);
        factory(MaterialType::class)->create([
            'id' => 2,
            'MaterialTypeName' => 'Tim2',
            'MaterialTypeStatus' => 'Active',
            'DateCreate' => now(),
            'UserCreate' => 3,
        ]);
    }
    public function roleScreen(){
        factory(RoleScreen::class)->create([
            'id' => 1,
            'ScreenNo' => '1',
            'ScreenName' => 'Role Master',
            'ScreenStatus' => 'Active',
            'DateCreate' => now(),
            'UserCreate' => 3,

        ]);
        factory(RoleScreen::class)->create([
            'id' => 2,
            'ScreenNo' => '2',
            'ScreenName' => 'Material Master',
            'ScreenStatus' => 'Active',
            'DateCreate' => now(),
            'UserCreate' => 3,
        ]);
        factory(RoleScreen::class)->create([
            'id' => 3,
            'ScreenNo' => '3',
            'ScreenName' => 'Employee Master',
            'ScreenStatus' => 'Active',
            'DateCreate' => now(),
            'UserCreate' => 3,
        ]);
        factory(RoleScreen::class)->create([
            'id' => 4,
            'ScreenNo' => '4',
            'ScreenName' => 'Transport Master',
            'ScreenStatus' => 'Active',
            'DateCreate' => now(),
            'UserCreate' => 3,
        ]);
        factory(RoleScreen::class)->create([
            'id' => 5,
            'ScreenNo' => '5',
            'ScreenName' => 'Customer Master',
            'ScreenStatus' => 'Active',
            'DateCreate' => now(),
            'UserCreate' => 3,
        ]);
        factory(RoleScreen::class)->create([
            'id' => 6,
            'ScreenNo' => '6',
            'ScreenName' => 'Service Request Maintenance',
            'ScreenStatus' => 'Active',
            'DateCreate' => now(),
            'UserCreate' => 3,
        ]);
        factory(RoleScreen::class)->create([
            'id' => 7,
            'ScreenNo' => '7',
            'ScreenName' => 'Quotation',
            'ScreenStatus' => 'Active',
            'DateCreate' => now(),
            'UserCreate' => 3,
        ]);
        factory(RoleScreen::class)->create([
            'id' => 8,
            'ScreenNo' => '8',
            'ScreenName' => 'Planning',
            'ScreenStatus' => 'Active',
            'DateCreate' => now(),
            'UserCreate' => 3,
        ]);
        factory(RoleScreen::class)->create([
            'id' => 9,
            'ScreenNo' => '9',
            'ScreenName' => 'Approve',
            'ScreenStatus' => 'Active',
            'DateCreate' => now(),
            'UserCreate' => 3,
        ]);
        factory(RoleScreen::class)->create([
            'id' => 10,
            'ScreenNo' => '10',
            'ScreenName' => 'Employee Role',
            'ScreenStatus' => 'Active',
            'DateCreate' => now(),
            'UserCreate' => 3,
        ]);
    }
    public function customer(){
        $faker = \Faker\Factory::create();
        DB::table('customers')
            ->insert([
                'id' => 1,
                'CustomerCode' => 'CTM0001',
                'CustomerNameEng' => 'Tim',
                'CustomerStatus' => 'Active',
                'DateCreate' => now(),
                'UserCreate' => 3,
                'DateLastUpdate' => now(),
                'UserLastUpdate' => 3,

            ]);
        for($i=2; $i<=100; $i++):
            $last_customer = DB::table('customers')->orderBy('CustomerCode', 'desc')->select('CustomerCode')->get();
            $last = $last_customer[0]->CustomerCode;
            $last++;

            DB::table('customers')
                ->insert([
                    'id' => $i,
                    'CustomerCode' => $last,
                    'CustomerNameEng' => $faker->name,
                    'CustomerStatus' => 'Active',
                    'DateCreate' => now(),
                    'UserCreate' => 3,
                    'DateLastUpdate' => now(),
                    'UserLastUpdate' => 3,
                ]);
        endfor;


    }
    public function employee(){

        factory(Employee::class)->create([ //admin account
            'EmployeeNo' => 'F0001',
            'PrefixEng' => 'Mr',
            'NameEng' => 'Tim',
            'SurNameEng' => 'Tim2',
            'email' => 'admin@email.com',
            'password' => Hash::make('123456789'),
            'remember_token' => Str::random(10),
            'bearer_token' =>'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9sb2NhbGhvc3Q6ODAwMFwvYXBpXC9sb2dpbiIsImlhdCI6MTU4MDc5OTM4NiwiZXhwIjoxNTgwODAyOTg2LCJuYmYiOjE1ODA3OTkzODYsImp0aSI6Im1wVFhKazhITU9RN01iNDciLCJzdWIiOjMsInBydiI6IjcyY2NhMDYzNzc2YmI1MTY1ZmI4OGI2YzE1MjEyMGVkYmExNmE5MTgifQ.4PayIZu1sdjWJH-_UPHR7JD0_01dmBE7Q0_XKoEqOY4',
            'SectionID' => 1,
            'DepartmentID' => 1,
            'PositionID' => 1,
            'DateCreate' => now(),
            "StateRoles" => "RoleCode,RoleStatus,UserCreate",
            "StateEmployee" => "id,EmployeeNo,NameEng,SurNameEng,EmployeeStatus,email,MobilePhone",
            "StateEmployeeChild"=> "LineNo,ChildrenName,ChildrenLastName,Sex,Birthdate,IdCard",
            "StateEmployeeAttach"=> '',
            "StateCustomer" => "CustomerCode,CustomerName,CustomerStatus,DateCreate,UserCreate,LastUpdateDate,LastUpdateUser",
            "StateCustomerLocation" =>"LineNo,LocationNameThai,LocationNameEng,LocationInitialsName,Address,LocationStatus,Distance,Latitude,Longitude",
            "StateCustomerContact" =>"LineNo,PrefixThai,NameThai,SurnameThai,PrefixEng,NameEng,SurnameEng,Nickname,Telephone,Mobilephone",
            "StateApprove" =>"ApproveKey,Master,CRType,CRStatus",
            "StateApproveOfUser" => "ApproveKey,Master,CRType,CRStatus",


        ]);
        factory(Employee::class)->create([ //admin account
            'EmployeeNo' => 'F0002',
            'PrefixEng' => 'Mr',
            'NameEng' => 'Tim',
            'SurNameEng' => 'Tim2',
            'email' => 'login@email.com',
            'password' => Hash::make('123456789'),
            'remember_token' => Str::random(10),
            'SectionID' => 1,
            'DepartmentID' => 1,
            'PositionID' => 1,
            'DateCreate' => now(),
            "StateRoles" => "RoleCode,RoleStatus,UserCreate",
            "StateEmployee" => "id,EmployeeNo,NameEng,SurNameEng,EmployeeStatus,email,MobilePhone",
            "StateEmployeeChild"=> "LineNo,ChildrenName,ChildrenLastName,Sex,Birthdate,IdCard",
            "StateEmployeeAttach"=> '',
            "StateCustomer" => "CustomerCode,CustomerName,CustomerStatus,DateCreate,UserCreate,LastUpdateDate,LastUpdateUser",
            "StateCustomerLocation" =>"LineNo,LocationNameThai,LocationNameEng,LocationInitialsName,Address,LocationStatus,Distance,Latitude,Longitude",
            "StateCustomerContact" =>"LineNo,PrefixThai,NameThai,SurnameThai,PrefixEng,NameEng,SurnameEng,Nickname,Telephone,Mobilephone",
            "StateApprove" =>"ApproveKey,Master,CRType,CRStatus",
            "StateApproveOfUser" => "ApproveKey,Master,CRType,CRStatus",




        ]);

        $faker = \Faker\Factory::create();
        for($i=3; $i<=50; $i++):
            $first_name = array(
                'กฤต', 'กฤติพงศ์', 'กฤติเดช', 'กฤษฎา', 'กฤษฎิ์', 'กฤษณะ', 'กฤษณ์', 'กันตพงศ์', 'กันตภณ', 'กำจร', 'กิตติ', 'กิตติภณ', 'กิตติศักดิ์', 'ก้องภพ', 'คมกฤช', 'คมสันต์', 'ครรชิต', 'ครองฤทธิ์', 'คำรณ', 'คึกฤทธิ์', 'จตุภัทร', 'จรัล', 'จรูญ', 'จอมเดช', 'จาตุรนต์', 'จารุกิตติ์', 'จารุวัฒน์',
                'จารุวิทย์', 'จารุเดช', 'จิตรายุธ', 'จิรพัฒน์', 'จิรายุ', 'จิรายุทธ', 'จิรเดช', 'จิรโชติ', 'จีรยุทธ', 'จุมพล', 'ฉันทพล', 'ฉันทพัฒน์', 'ชนกันต์', 'ชนะศึก', 'ชัชชัย', 'ชัชวาลย์', 'ชัยณรงค์', 'ชัยนันท์', 'ชัยมงคล', 'ชัยวุฒิ', 'ชัยโรจน์', 'ชาญชัย', 'ชาญรบ', 'ชาตพล', 'ชานนท์', 'ชิษณุพงศ์', 'ชุติเดช', 'ฐิติพงศ์', 'ฐิติภัทร', 'ณรงค์',
                'ณรงค์กร', 'ณรงค์ชัย', 'ณรงค์ฤทธิ์', 'ณัฏฐกิตติ์', 'ณัฏฐชัย', 'ณัฏฐพล', 'ณัฐกฤต', 'ณัฐชนน', 'ณัฐพงศ์', 'ณัฐภูมิ', 'ณัฐฤกษ์', 'ณัฐวิโรจน์', 'ดนัย', 'ดนุเดช', 'ดลฤทธิ์', 'ดุลยศักดิ์', 'ตรีภพ', 'ติณณภพ', 'ต้นกล้า', 'ทรงกฤต', 'ทรงภพ',
                'ทรงวุฒิ', 'ทวีเดช', 'ทัดภูมิ', 'ทิฐิพล', 'ทินภัทร', 'ทีปกร', 'ธงชัย', 'ธนกร', 'ธนกฤต', 'ธนชาติ', 'ธนชิต', 'ธนภัทร', 'ธนภูมิ', 'ธนศักดิ์', 'ธนัชชัย', 'ธนาธิป', 'ธนเดช', 'ธนโชติ', 'ธรรมศักดิ์', 'ธวัชชัย', 'ธัญวิทย์', 'ธานินทร์', 'ธารินทร์',
                'ธิติ', 'ธีธัช', 'ธีรนัย', 'ธีรพล', 'ธเนศ', 'นพณัฐ', 'นพเดช', 'นรวุฒิ', 'นรสิงห์', 'นราธิป', 'นฤชิต', 'นันทภพ', 'บรรพต', 'บรรยงค์', 'บวรชัย', 'บวรพจน์', 'บวรวิทย์', 'บัณฑิต', 'บันลือศักดิ์', 'บัลลพ', 'บุญฤทธิ์', 'บุญส่ง', 'บุญโชค', 'บุรินทร์',
                'ปกรณ์', 'ปฏิพล', 'ปณต', 'ประชา', 'ประพจน์', 'ประเสริฐ', 'ปรัชญา', 'ปัญจพล', 'ปานศักดิ์', 'ปิติ', 'ปิติพงศ์', 'ปิยวัฒน์', 'ป้องเกียรติ', 'ผดุงศักดิ์', 'ผดุงเดช', 'พงศ์กฤต', 'พงศ์พัทธ์', 'พงศ์เดช', 'พลภัทร', 'พลวัต', 'พันธ์พงษ์',
                'พันเดช', 'พัลลภ', 'พาทิศ', 'พิพัฒน์', 'พิริยะศักดิ์', 'พิสิษฐ์', 'พิเชษฐ', 'พีรพล', 'พีรพัฒน์', 'พีรเทพ', 'พุฒิพงศ์', 'ภควัต', 'ภัคพล', 'ภัทร', 'ภัทรกร', 'ภากร', 'ภาคิน', 'ภาณุพงศ์', 'ภาณุภัทร', 'ภานุวัฒน์',
                'ภูมิ', 'ภูมิพัฒน์', 'ภูริช', 'ภูวดล', 'ภูวเดช', 'ภูสิทธิ', 'มงคล', 'มงคลชัย', 'มนภาส', 'มนุญศักดิ์', 'มานะ', 'ยุทธการ', 'ราเชน', 'วรพจน์', 'วรพล', 'วรวุฒิ', 'วรุตม์', 'วรเทพ', 'วรเมธ', 'วัชรพล', 'วัลลภ',
                'วิกรม', 'วิชาญ', 'วิทวัส', 'วิบูลย์', 'วิวัฒน์ชัย', 'วิโรจน์', 'วีรพล', 'วีรยุทธ', 'วีรเดช', 'วุฒิชัย', 'วุฒิชาติ', 'วุฒิเดช', 'ศักดา', 'ศุภกิจ', 'ศุภณัฐ', 'สมชาย', 'สมศักดิ์', 'สิทธินนท์', 'สิทธิเดช', 'สุกฤษฎิ์', 'สุทธินันท์',
                'สุทธิพงษ์', 'สุบรรณ', 'สุเมธ', 'อดิรุจ', 'อดิเทพ', 'อดุลย์', 'อติชาติ', 'อนันตชัย', 'อนุชิต', 'อนุทิศ', 'อนุพนธ์', 'อนุภัทร', 'อนุรักข์', 'อภิคม', 'อภิชาติ', 'อรรคพล', 'อรรณพ', 'อรรณวุฒิ', 'อัคคเดช', 'อัครชัย', 'อาชวิน', 'อาณัติ', 'อานนท์', 'อานันท์', 'อารักษ์', 'อิทธิชัย', 'อิทธิพัทธ์', 'เกริกไกร',
                'เจตน์', 'เจตพล', 'เจนศักดิ์', 'เจษฎา', 'เจิมศักดิ์', 'เชิดชาย', 'เด่นภูมิ', 'เตชภณ', 'เตชินท์', 'เป็นต่อ', 'เผชิญชัย', 'เผด็จ', 'เผ่าเทพ', 'เมธัส', 'เรวัติ', 'เรืองโรจน์', 'เสกข์', 'เหมรัชต์', 'เอกภพ', 'แสงโชติ', 'โกมุท', 'โกเมน', 'โกเมศ', 'โฆษิต', 'โพธิวัฒน์', 'โยธิต', 'โอภาส', 'โอฬาร', 'ไกรยุทธ์', 'ไชยวัฒน์', 'ไตรภพ', 'ไพศาล'
            ,
                'กชวรรณ', 'กนกกาญจน์', 'กนกทิพย์', 'กนกนุช', 'กนกพรรณ', 'กนกรัตน์', 'กนกวรรณ', 'กนกอร', 'กมลกานต์', 'กมลฉัตร', 'กมลชนก', 'กมลทิพย์', 'กมลรัตน์', 'กมลวรรณ', 'กมลา', 'กมลเนตร', 'กรกนก', 'กรกมล', 'กรรณิกา', 'กรรณิการ์', 'กรวรรณ', 'กรวิกา', 'กรวิภา', 'กรองกาญจน์', 'กรองแก้ว', 'กรุณา', 'กฤตยา', 'กวินทิพย์', 'กัลยรัตน์', 'กัลยา',
                'กัลยาณี', 'กัลยาณี', 'กานต์ธิดา', 'กิตติมา', 'กิรติกา', 'กิ่งกาญจน์', 'กิ่งแก้ว', 'กุสุมา', 'ขจีพรรณ', 'ขวัญจิรา', 'ขวัญตา', 'ขวัญทิพย์', 'ขวัญแก้ว', 'ครองพร', 'คะนึงจิต', 'คะนึงนิตย์', 'คำหยาด', 'จรรยา', 'จรัญพร', 'จรูญพรรณ', 'จันทนา', 'จันทนิภา',
                'จันทร์จิรา',  'จันทร์เพ็ญ',  'จารีรัตน์',  'จารุณี',  'จารุวรรณ',  'จิดาภา',  'จิตตนาถ',  'จิตติมา',  'จิตรลดา',  'จิตรานุช',  'จินดา',  'จินดาพรรณ',  'จินดารัตน์',  'จินตนา',  'จินตพร',  'จินตภา',  'จิรนาถ',  'จิรัชญา',  'จีรพรรณ',  'จุฑาทิพย์',
                'จุฑาภรณ์', 'จุฑามณี', 'จุฑารัตน์', 'จุไรรัตน์', 'จุไรวรรณ', 'ฉวีผ่อง', 'ฉัตราภรณ์', 'ชงโค', 'ชนกนันท์', 'ชนกนาถ', 'ชนนิภา', 'ชนิกานต์', 'ชนิดา', 'ชนิภา', 'ชมพูนุช', 'ชยาภา', 'ชลธิชา', 'ชลธิดา', 'ชลิตา', 'ชาลินี', 'ชิดชนก', 'ชุติกา', 'ชุติกาญจน์',
                'ชุติภา', 'ชุติมน', 'ชุติมา', 'ช่อทิพย์', 'ฐปนีย์', 'ฐาปนีย์', 'ฐิตาพร', 'ฐิตารีย์', 'ฐิติกร', 'ฐิติกาญจน์', 'ฐิติพร', 'ฐิติพรรณ', 'ฐิติภา', 'ฐิติมา', 'ณฐินี', 'ณฤดี', 'ณัชชา', 'ณัฏฐธิดา', 'ณัฏฐิกา', 'ณัฏฐินี', 'ณัฐกานต์', 'ณัฐชยา', 'ณัฐชา', 'ณัฐฐา', 'ณัฐวดี',
                'ณิชกานต์', 'ณิชมน', 'ณิชา', 'ณิชารีย์', 'ณีรนุช', 'ดลพร', 'ดวงแข', 'ดารณี', 'ดารินทร์', 'ดาวเรือง', 'ดุจดาว', 'ถวิกา', 'ถิรดา', 'ทองปลิว', 'ทัดดาว', 'ทิพนาถ', 'ทิพปภา', 'ทิพย์รัตน์', 'ธนพร', 'ธนัญญา', 'ธนิดา', 'ธัญชนก', 'ธัญญรัตน์', 'ธัญญา', 'ธัญทิพย์',
                'ธัญพร', 'ธัญพิมล', 'ธัญรัตน์', 'ธันยชนก', 'ธันยพร', 'ธารทิพย์', 'ธาริณี', 'ธิดารัตน์', 'ธิดาวรรณ', 'นงนุช', 'นภา', 'นภาพร', 'นภาพรรณ', 'นภาวรรณ', 'นรีรัตน์', 'นฤมล', 'นลิน', 'นันทฉัตร', 'นันทนา', 'นันทิกานต์', 'นันทิชา', 'นันทิตา', 'นันทินี', 'นาถสุดา', 'นิชา',
                'นิชาภา', 'นิรชา', 'บุณยนุช', 'บุณยพร', 'บุปผา', 'บุษบา', 'บุหลัน', 'ปฐมา', 'ปณิดา', 'ปนัดดา', 'ประกายแก้ว', 'ประภัสสร', 'ประภารัตน์', 'ประไพพรรณ', 'ปรางทิพย์', 'ปราญชลี', 'ปรียา', 'ปรียาดา', 'ปรียานุช', 'ปวันรัตน์', 'ปวีณอร', 'ปัญญารัตน์',
                'ปัทมา', 'ปัทมาพร', 'ปานชนก', 'ปาริฉัตร', 'ปิยธิดา', 'ปิยนุช', 'ปิยะพรรณ', 'ปิยะวรรณ', 'ปุณยภา', 'ผกาพรรณ', 'ผดุงพร', 'พรทิพา', 'พรนภา', 'พรประภา', 'พรพรรณ', 'พรพิมล', 'พรรณทิพย์', 'พรรนิภา', 'พรไพลิน', 'พวงผกา', 'พัชรมัย', 'พัชรีวรรณ', 'พิมพ์ผกา', 'พิมพ์พรรณ', 'พิมพ์อัปสร', 'พิมลภา', 'พิมลรัตน์', 'พิศมัย',
                'ภัทรธิดา', 'ภัทรลดา', 'ภัทรวรรณ', 'ภารวี', 'ภาวินี', 'มณีรัตน์', 'มัลลิกา', 'มาลินี', 'มินตรา', 'ยุพา', 'รติมา', 'รัชนี', 'รัตนประภา', 'รุจยา', 'รุจิราพร', 'ลดามณี', 'ลำดวน', 'วรกานต์', 'วรรณพร', 'วราภรณ์', 'วลัยพร', 'วลัยพรรณ', 'วลัยสมร', 'วลีรัตน์', 'วัชราภรณ์',
                'วันทนีย์', 'วิภาดา', 'วิมลรัตน์', 'วิไลรัตน์', 'วิไลวรรณ', 'วีณา', 'ศันสนีย์', 'สมพร', 'สมศรี', 'สมหญิง', 'สาวิตรี', 'สุนิสา', 'อภิรดี', 'อรจิรา', 'อรชร', 'อรณิชา', 'อรนุช', 'อรพรรณ', 'อรพิมพ์', 'อรพิมล', 'อลิน', 'อังคณา', 'อัจฉรา', 'อัจฉราพร', 'อัญชลิกา', 'อาภรณ์', 'อินตรา', 'อุไรวรรณ', 'เกตุมณี',
                'เกวลิน', 'เขมจิรา', 'เขมิกา', 'เครือวัลย์', 'เจนจิรา', 'เบญจพร', 'เบญจภรณ์', 'เบญจรัตน์', 'เบญจวรรณ', 'เพ็ญประภา', 'เพ็ญแข', 'เภตรา', 'เมธินี', 'เลอลักษณ์', 'เสาวรส', 'แก้วทิพย์', 'แสงจันทร์', 'โชติกา', 'ไฉววงศ์', 'ไปรยา', 'ไลลา',
                'ไอริณ',
            );
            $lastName = array(
                'กติยา', 'กมลานันท์', 'กล้านอนหงาย', 'กองการ', 'กางมุ้งคอย', 'กาญจนวิภู', 'กำธรเจริญ', 'กิจจานุรักษ์', 'กิตติธร', 'ครองจินดา', 'งามระลึก', 'จันทร์ประดับ', 'จิตประสงค์', 'จิตมานะ', 'จิตสะอาด', 'จิรามณี', 'จุฑาเทพ ณ อยุธยา', 'ชมชอบ', 'ชอบนอนหงาย', 'ดำรงลาวรรณ', 'ดิเรกวิทยา', 'ตะลุมพุก', 'ทวิพักตร์', 'ทองดี', 'ทองม่วง', 'นทีพิทักษ์', 'นิมิตนาม',
                'บาดตาสาว', 'บินทะลุบ้าน', 'บุญมั่น', 'ปรปักษ์เป็นจุล', 'ประจัญบาน', 'ประภาพร', 'ปืนครก', 'พงศ์พิทักษ์', 'พจน์จำเนียร', 'พรรณาราย', 'พิชัยรณรงค์', 'พิทักษ์ไทย', 'พึ่งสุข', 'พุ่มฉัตร', 'มณีน้อย', 'มนูญศักดิ์', 'มหานิยม', 'ยอดยาใจ', 'รอดคงรวย', 'รักธรรม', 'รักสำราญ', 'รัตนามิรา', 'รัตนเดชากร', 'รุจิอาภรณ์', 'วงศ์บันเทิง', 'วงศ์สินวิเศษ',
                'วรรณดำรง',  'กติยา', 'กมลานันท์', 'กล้านอนหงาย', 'กองการ', 'กางมุ้งคอย', 'กาญจนวิภู', 'กำธรเจริญ', 'กิจจานุรักษ์', 'กิตติธร', 'ครองจินดา', 'งามระลึก', 'จันทร์ประดับ', 'จิตประสงค์', 'จิตมานะ', 'จิตสะอาด', 'จิรามณี', 'จุฑาเทพ ณ อยุธยา', 'ชมชอบ', 'ชอบนอนหงาย', 'ดำรงลาวรรณ', 'ดิเรกวิทยา', 'ตะลุมพุก', 'ทวิพักตร์', 'ทองดี', 'ทองม่วง', 'นทีพิทักษ์', 'นิมิตนาม',
                'บาดตาสาว', 'บินทะลุบ้าน', 'บุญมั่น', 'ปรปักษ์เป็นจุล', 'ประจัญบาน', 'ประภาพร', 'ปืนครก', 'พงศ์พิทักษ์', 'พจน์จำเนียร', 'พรรณาราย', 'พิชัยรณรงค์', 'พิทักษ์ไทย', 'พึ่งสุข', 'พุ่มฉัตร', 'มณีน้อย', 'มนูญศักดิ์', 'มหานิยม', 'ยอดยาใจ', 'รอดคงรวย', 'รักธรรม', 'รักสำราญ', 'รัตนามิรา', 'รัตนเดชากร', 'รุจิอาภรณ์', 'วงศ์บันเทิง', 'วงศ์สินวิเศษ',
                'วรรณดำรง', 'วรเลิศรัตน์', 'วังชัยศรี', 'วาทะศรัทธา', 'วิทยเขตปภา', 'วิไลสักดิ์', 'ศรีเจริญ', 'ศิลามหาฤกษ์', 'ศิวาวงศ์', 'สมคำนึง', 'สมุทรเทวา', 'สันตติวงศ์', 'สันต์สิริศักดิ์', 'สัมพันธ์พงษ์', 'สำราญฤทธิ์', 'สุรบดินทร์', 'สูญสิ้นภัย', 'หงส์แก้ว', 'หนึ่งในยุทธจักร', 'หวังกระแทกคาง', 'อดุสาระดี', 'อัศวรัช', 'อัศวเรืองฤทธิ์', 'อาจฤทธิ์', 'เกตุอารี',
                'เกิดความสุข', 'เกิดงามพริ้ง', 'เก่งระดมยิง', 'เจนพานิชย์สกุล', 'เจริญเดช', 'เทพทัต', 'เทพสุวรรณ', 'เที่ยงธรรม', 'เบี้ยวสกุล', 'เหนือกาล', 'แก้วมณีงาม', 'แสนชล', 'โชคช่วย', 'ไชยวัฒนา'
            ,'วรเลิศรัตน์', 'วังชัยศรี', 'วาทะศรัทธา', 'วิทยเขตปภา', 'วิไลสักดิ์', 'ศรีเจริญ', 'ศิลามหาฤกษ์', 'ศิวาวงศ์', 'สมคำนึง', 'สมุทรเทวา', 'สันตติวงศ์', 'สันต์สิริศักดิ์', 'สัมพันธ์พงษ์', 'สำราญฤทธิ์', 'สุรบดินทร์', 'สูญสิ้นภัย', 'หงส์แก้ว', 'หนึ่งในยุทธจักร', 'หวังกระแทกคาง', 'อดุสาระดี', 'อัศวรัช', 'อัศวเรืองฤทธิ์', 'อาจฤทธิ์', 'เกตุอารี',
                'เกิดความสุข', 'เกิดงามพริ้ง', 'เก่งระดมยิง', 'เจนพานิชย์สกุล', 'เจริญเดช', 'เทพทัต', 'เทพสุวรรณ', 'เที่ยงธรรม', 'เบี้ยวสกุล', 'เหนือกาล', 'แก้วมณีงาม', 'แสนชล', 'โชคช่วย', 'ไชยวัฒนา'
            );
            $last_emp = DB::table('employees')->orderBy('EmployeeNo', 'desc')->select('EmployeeNo')->get();
            $last = $last_emp[0]->EmployeeNo;
            $last++;
            $num = [1, 2];
            $status =  ['Active', 'Inactive'];

            $prefix_thai =  ['นาย', 'นาง', 'นางสาว'];
            $prefix_eng =  ['Mr', 'Miss', 'Ms'];

//            DB::table('employees')
//                ->insert([
//                    'id' => $i,
//                    'EmployeeNo' => $last,
//                    'IDCard' => $faker->uuid,
//                    'MotorcycleDrivingLicenseID' => $faker->uuid,
//                    'CarDrivingLicenseID' => $faker->uuid,
//                    'TruckDrivingLicenseID' => $faker->uuid,
//                    'PrefixThai' => $prefix_thai[$i % 3],
//                    'NameThai' => array_rand($first_name,1),
//                    'SurNameThai' =>array_rand($lastName,1),
//                    'PrefixEng' => $prefix_eng[$i % 3],
//                    'NameEng' => $faker->firstName,
//                    'SurNameEng' =>$faker->lastName,
//                    'NickName' => $faker->name,
//                    'EmployeeTypeID' => $num[$i % 2],
//                    'PositionID' => $num[$i % 2],
//                    'SectionID' => $num[$i % 2],
//                    'DepartmentID' => $num[$i % 2],
//                    'MobilePhone' => $faker->phoneNumber,
//                    'email' => $faker->email,
//                    'BirthDate' => $faker->date(),
//                    'StartWorkingDate' => $faker->date(),
//                    'ResignDate' => $faker->date(),
//                    'SocialSecurityID' => $faker->uuid,
//                    'EmployeeStatus' => $status[$i % 2],
//                    'CustomerIDRef' => 1,
//                    'Note' => $faker->randomDigit,
//                    'IDCardExpireDate' => $faker->date(),
//                    'MortocycleDrivingLicenseExpireDate' => $faker->date(),
//                    'CarDrivingLicenseExpireDate' => $faker->date(),
//                    'TruckDrivingLicenseExpireDate' => $faker->date(),
//                    'DateCreate' => now(),
//                    'UserCreate' => 1,
//                    'MilitaryStatus' => $status[$i % 2],
//                    'MilitaryStatusExtension' => $faker->text,
//                    'Nationality' => $faker->text,
//                    'Origin' => $faker->text,
//                    'Religious' => $faker->text,
//                    'MaritalStatus' => $status[$i % 2],
//                    'NativeHabitat' => $faker->text,
//                    'Address' => $faker->address,
//                    'PostCode' => $faker->postcode,
//
////                    'CountrySkey' => $num[$i % 2],
////                    'ProvinceSkey'=> $num[$i % 2],
////                    'DistrictSkey'=> $num[$i % 2],
////                    'SubDistrictSkey'=> $num[$i % 2],
//
//                    'FacebookID' => $faker->uuid,
//                    'LineID' => $faker->uuid,
//                    'Reference' => $faker->text,
//                    'ReferecncePhone' => $faker->phoneNumber,
//                    'ReferenceRelationship' => 1,
//                    'FatherName' => $faker->firstName,
//                    'FatherLastName' => $faker->lastName,
//                    'MotherName' => $faker->firstName,
//                    'MotherLastName' => $faker->lastName,
//                    'BloodGroup' => 'O',
//                    'MateName' => $faker->firstName,
//                    'MateSurname' => $faker->lastName,
//                    'MateIDCard' => $faker->uuid,
//                    'MateBirthDate' => $faker->date(),
//                    "StateRoles" => "RoleCode,RoleStatus,UserCreate",
//                    "StateEmployee" => "id,EmployeeNo,NameEng,SurNameEng,EmployeeStatus,email,MobilePhone",
//                    "StateEmployeeChild"=> "LineNo,ChildrenName,ChildrenLastName,Sex,Birthdate,IdCard",
//                    "StateEmployeeAttach"=> '',
//                    "StateCustomer" => "CustomerCode,CustomerName,CustomerStatus,DateCreate,UserCreate,LastUpdateDate,LastUpdateUser",
//                    "StateCustomerLocation" =>"LineNo,LocationNameThai,LocationNameEng,LocationInitialsName,Address,LocationStatus,Distance,Latitude,Longitude",
//                    "StateCustomerContact" =>"LineNo,PrefixThai,NameThai,SurnameThai,PrefixEng,NameEng,SurnameEng,Nickname,Telephone,Mobilephone",
//                    "StateApprove" =>"ApproveKey,Master,CRType,CRStatus",
//                    "StateApproveOfUser" => "ApproveKey,Master,CRType,CRStatus",
//
//
//
//
//            ]);


        endfor;

    }


}

